\index{system!open|(textbf}
\section{Why Use an Open System?}
\index{system!closed, usefulness of}\index{system!open, usefulness of}\index{closed vs. open system}

	In many machines, the fluid used to transfer heat and work circulates continuously. It can then be difficult to identify a fixed amount of mass to make it a closed system and quantify energy transfers within it. For example, in a turbojet nozzle, air expands and accelerates continuously: at any given moment, there is no identifiable volume that would have \emph{one} specific speed or \emph{one} particular pressure.

	Using an open system is very useful to account for energy in flows. Rather than separating stages in time (for example before and after compression), we quantify work and heat transfers by separating stages in space (for example upstream and downstream of the compressor).


\section{Accounting Conventions}
\label{ch_accounting_conventions_os}


	\subsection{The open system}
	\label{ch_convention_signe_so}
	\index{system!closed, sign conventions}

		\thermoquotebegin{O}
			% Original quote in German:
			% Die konstruktiven Schwierigkeiten, die der Großgasmotor zu überwinden hat, die aus den gewaltigen Kolbendrücken und der Wärmeausdehnung der komplizierten Zylinderköpfe (zahlreiche Brüche!) entspringen, sind allgemein bekannt. Eine betriebssichere Gasturbine würde in dieser Beziehung einen Fortschritt bedeuten.
			% Original translation in English:
			The constructive difficulties that have to be overcome in a large gas motor because of the immense piston pressures and heat expansion of the complicated cylinder heads (cracks galore!) are well known. A safe gas turbine would in this respect be an improvement. 
		\thermoquoteend{Aurel Stodola, 1904}{\textit{Die Dampfturbinen}~\cite{stodola1904, stodola1905}\vspace{3em}\index{Stodola!Aurel}}%handmade vspace
		We call an \vocab[system!open, definition]{open system} an arbitrary subject of study whose boundaries are permeable to mass (\cref{fig_open_system}). In general, its volume can change, and it can have multiple intakes and outlets, each with a different flow rate and pressure.

		\begin{figure}[ht]%handmade pour poser la figure
			\begin{center}
				\includegraphics[width=8cm]{convention_systeme_ouvert}
			\end{center}
			\supercaption{Sign conventions for an open system. Inflows are positive, outflows are negative; they are all represented with inward arrows.}{\wcfile{Open thermodynamic system.svg}{Diagram} \cczero \oc}
			\label{fig_open_system}
		\end{figure}

		In our study of thermodynamics, we will only use open systems:
		\begin{itemize}
			\item with fixed volume;
			\item having only one inlet and one outlet;
			\item being crossed by a constant mass flow rate $\dot m$ (positive by convention).
		\end{itemize}
		These systems are said to be in \vocab[]{steady state} (sometimes called \vocab[]{steady flow} or \vocab[]{stationary flow} regime).\index{steady state}\index{steady,state}.

	\dontbreakpage%handmade, pour éviter un drôle de saut de page
	\subsection{Sign conventions}
	\index{system!closed, sign conventions}

		Just like for closed systems, we will take the open system's point of view to quantify transfers:
			\begin{itemize}
				\item The reception of work, heat, or mass results in a \emph{positive} transfer;
				\item The loss of work, heat, or mass results in a \emph{negative} transfer.
			\end{itemize}

		Thus, we add up all transfers as on a bank statement.


\section{The First Law in an Open System}
\index{system!open, first principle}\index{thermodynamic principles!first, open system}

	We have seen that in a closed system, the law of conservation of energy is expressed by the equation $q + w = \Delta u$ (\ref{eq_premier_principe_sf_min}). In an open system, the situation is a little different and we must consider additional forms of energy.


	\subsection{Entering and exiting the system: flow work}
	\index{work!flow}\index{work!insertion}\index{work!extraction}

		Let's imagine an open system in steady flow, containing a small water pump. To insert water into the pump at a given pressure, energy must be supplied to the system. Conversely, to push the water outside (at a higher pressure), the system must supply energy. How can we quantify this energy?

		Consider the case of a \vocab[fluid element]{fluid element}\index{fluid, element of} (i.e., a small quantity of fluid in transit, with volume $V_\text{element}$) entering our system at pressure $p_1$ (\cref{fig_flow_work}).

		\begin{figure}
			\begin{center}
				\includegraphics[width=8cm]{travail_insertion-en}
			\end{center}
			\supercaption{A fluid element of volume $V_\text{element}$ entering at pressure $p$ into the open system.}{\wcfile{Conceptual experiment flow work.svg}{Diagram} \cczero \oc}
			\label{fig_flow_work}
		\end{figure}

		The work $W_\text{insertion}$ received by the system when the element is pushed through the insertion is:
		\begin{IEEEeqnarray}{rCl}
			W_\text{insertion} 	& = & p_1 \ V_\text{element}
		\end{IEEEeqnarray}
		\begin{equationterms}
			\item where \tab $W_\text{insertion}$ 	\tab is the insertion work (\si{\joule}),
			\item and \tab $V_\text{element}$ 	\tab\tab is the volume of the fluid element (\si{\cubic\meter}).
		\end{equationterms}

		If such a volume of fluid enters the system every second, then the system receives power in the form of work, which we call \vocab[power!insertion]{insertion power}, $\dot{W}_\text{insertion}$. We sometimes express it in specific form~(\S\ref{ch_valeurs_specifiques}):
		\begin{IEEEeqnarray}{rCl}
			\dot{W}_\text{insertion} 	& = & p_1 \ \dot{V}_1 = \dot m_1 \ p_1 \ v_1 = \dot m \ p_1 \ v_1\\
			w_\text{insertion} 			& = & p_1 \ v_1
			\label{eq_puissance_spe_insertion}
		\end{IEEEeqnarray}
		\begin{equationterms}
			\item where \tab $\dot{W}_\text{insertion}$ 	\tab is the insertion power (\si{\watt}),
			\item 	\tab $w_\text{insertion}$ 			\tab is the specific insertion power (\si{\joule\per\kilogram}),
			\item		\tab $\dot m_1$ 	is the net mass flow rate at 1 (\si{\kilogram\per\second}),
			\item		\tab $\dot m$ 		\tab\tab is the mass flow rate crossing the system (always positive, \si{\kilogram\per\second}),
			\item 	\tab $\dot{V}_1$ 	\tab is the volumetric flow rate of fluid (\si{\cubic\meter\per\second}),
			\item and \tab $v_1$ 				\tab is the specific volume of the fluid at the inlet (\si{\cubic\meter\per\kilogram}).
		\end{equationterms}

		Similarly, for the fluid to exit the system at the other end, the system must continuously provide a power called \vocab[power!extraction]{extraction power}:
		\begin{IEEEeqnarray}{rCl}
			\dot{W}_\text{extraction} 	& = & - p_2 \ \dot{V_2} = \dot{m}_2 \ p_2 \ v_2 = - \dot{m} \ p_2 \ v_2 \\
			w_\text{extraction} 			& = & - p_2 \ v_2
		\end{IEEEeqnarray}
		\begin{equationterms}
			\item where the outgoing mass flow rate $\dot m_2$ (negative) is expressed in terms of the mass flow rate $\dot m$ crossing the system (always positive, \si{\kilogram\per\second}).
		\end{equationterms}

		The net sum of these two powers at the boundaries is called \vocab[flow power]{flow power}, $\dot{W}_\text{flow} \equiv \dot{W}_\text{insertion} + \dot{W}_\text{extraction}$. Its sign depends on the operating conditions~-- students are encouraged to visualize and formulate the conditions under which the flow power can be negative, zero, or positive.


	\subsection{Energy balance}

		Let's try to design an open system in steady flow in the most general way possible, as represented in \cref{fig_open_system}. We will now account for all energy transfers within it.

		\begin{figure}
			\begin{center}
				\includegraphics[width=\textwidth]{sfee-en}
			\end{center}
			\supercaption{Arbitrary open system. The system (whose boundaries are dashed lines, in red) is crossed from left to right by the fluid flowing with a constant mass flow rate $\dot{m}$. It receives power $\dot{W}_{1 \to 2}$ in the form of work and power $\dot{Q}_{1 \to 2}$ in the form of heat.}{\wcfile{Arbitrary continuous flow open system.svg}{Diagram} \cczero \oc}
			\label{fig_open_system}
		\end{figure}

		When entering the system, the fluid already has an internal energy $u_1$; therefore, the system sees its internal energy increase with power $\dot{U}_{1}$:
		\begin{equation}
			\dot{U}_{1} = \dot{m} \ u_1
			\label{eq_w_int1}
		\end{equation}

		Similarly, the fluid has a specific mechanical energy $e_\text{mech1}$ (\ref{def_energie_mecanique_specifique}), and the system also receives power $\dot{E}_\text{mech1}$:
		\begin{equation}
			\dot{E}_\text{mech1} = \dot m \ e_\text{mech1} = \dot{m} \ \left(\frac{1}{2} \ C_1^2 + g \ z_1\right)
			\label{eq_mech_energy1}
		\end{equation}

		These expressions~\ref{eq_w_int1} and~\ref{eq_mech_energy1} have the opposite sign at the system's outlet, where we assign them the index 2.

		At this point, we have covered all of the energy forms that can be observed crossing the boundaries of an open system together with the fluid: flow work, internal energy, and mechanical energy. Since the first law states that energy is indestructible (\S\ref{ch_premier_principe}), the addition of power $\dot Q$ in the form of heat or $\dot W$ in the form of work can only vary these three forms. This results in the equation:
		\begin{IEEEeqnarray}{rCl}
			\dot{Q}_{1 \to 2} + \dot{W}_{1 \to 2} + \left(\dot{W}_\text{insertion} + \dot{U}_{1} + \dot{E}_\text{mech1}\right) + \left(\dot{W}_\text{extraction} + \dot{U}_{2} + \dot{E}_\text{mech2}\right) = 0	\nonumber\\
			\label{eq_somme_puissances}
		\end{IEEEeqnarray}
		\begin{equationterms}
			\item where all terms are expressed in \si{watts}.
		\end{equationterms}

		%TODO check/fix equation overflow into margin
		We can re-express equation~\ref{eq_somme_puissances} in terms of directly measurable quantities:
		\begin{IEEEeqnarray}{rCl}
			\dot{Q}_{1 \to 2} + \dot{W}_{1 \to 2} + \dot{m} \ \left(p_1 \ v_1 + u_1 + \frac{1}{2} C_1^2 + g \ z_1\right) &=& \dot{m} \ \left(p_2 \ v_2 + u_2 + \frac{1}{2} C_2^2 + g \ z_2\right)\nonumber\\
			\label{eq_grande_sfee}
		\end{IEEEeqnarray}
		or:
		\begin{IEEEeqnarray}{rCl}
			\dot{Q}_{1 \to 2} + \dot{W}_{1 \to 2} 	& = & \dot{m} \left[ \Delta u + \Delta (p v) + \frac{1}{2} \Delta \left(C^2\right) + g \ \Delta z \right] 	\label{eq_grande_sfee_deltas} \\
			q_{1 \to 2} + w_{1 \to 2} 		& = & \Delta u + \Delta (p v) + \Delta e_\text{mech.} \label{eq_petite_sfee_deltas}
		\end{IEEEeqnarray}
		\begin{equationterms}
			\item where the symbols $\Delta$ indicate the variation of properties between points 1 and 2 within the system.
		\end{equationterms}

		Equations~\ref{eq_grande_sfee} and~\ref{eq_petite_sfee_deltas} are extremely useful in thermodynamics, as they allow us to quantify by deduction the powers involved in flows. They allow us, in particular, to predict the properties of the fluid at the outlet of a device for which we know the mechanical power and heat emissions. For example, we can determine the remaining energy in the air at the outlet of a turbine for which we know the power.

		\begin{anexample}
			A turbojet compressor admits \SI{1.5}{\kilogram\per\second} of air at a pressure of \SI{0.8}{\bar}, internal energy of \SI{192.5}{\kilo\joule\per\kilogram}, and specific volume of \SI{0.96}{\cubic\meter\per\kilogram}. It compresses the air to \SI{30}{\bar}, releasing it with an internal energy of \SI{643.1}{\kilo\joule\per\kilogram} and a specific volume of \SI{8.57e-2}{\cubic\meter\per\kilogram}. The velocity and altitude of the air remain unchanged.

			What is the power of the compressor if its heat transfers are negligible?
				\begin{answer}
					We apply equation~\ref{eq_grande_sfee_deltas} to obtain: $\dot{W}_{1 \to 2}
					= -\dot{Q}_{1 \to 2} +\allowbreak \dot{m} \left[ \Delta u + \Delta (p v) + \frac{1}{2} \Delta \left(C^2\right) + g \ \Delta z \right]
					= 0 + \dot{m} \left[ \Delta u + \Delta (p v) + 0 + 0 \right]
					= \num{1,5} \left[ (\num{643,1e3} - \num{192,5e3}) + (\num{30e5}\times\num{8,57e-2} - \num{0,8e5}\times\num{0,96})\right]
					= \SI{+9,464e5}{\watt} = \SI{+946,4}{\kilo\watt}$.
					\begin{remark}The only difficulty in applying this equation concerns the proper conversion of units. Pressure and energy should always be converted from their usual units to \textsc{si} units.\end{remark}
					\begin{remark}The power is positive, as expected since the air \emph{receives} the work. In a turbine, the work would be negative.\end{remark}%
					\begin{comment}
						Construction of the example: compression from 0.8 to 30 bar according to a polytropic pv^1.5 = k.
						p1 = 0.8 bar 	T1 = -5°C = 268.15K	==> v1 = 0.962m3/kg & u1 = 192.53 kJ/kg
						p2 = 30 bar	==> v2 = 8.569E-2 m3/kg 	T2 = 895.7K (622.6°C)	u2 = 643.1 kJ/kg
					\end{comment}
				\end{answer}
		\end{anexample}


	\subsection{Enthalpy}

		In many cases, the terms $u$ and $p v$ vary in the same way with the state of the fluid (we will see in the next chapter that in the case of an ideal gas, they are both proportional to the temperature). To simplify their use in calculations, they are often combined into a single term.

		We call the sum of the terms $u$ and $p v$ the \vocab[enthalpy!specific]{specific enthalpy}\index{specific!enthalpy}, and assign it the symbol $h$:
		\begin{equation}
			h \equiv u + p \ v
			\label{def_enthalpie}
		\end{equation}
		\begin{equationterms}
			\item where the terms are expressed in \si{\joule\per\kilogram}.
		\end{equationterms}

		Of course, the \vocab[enthalpy!definition|textbf]{enthalpy} $H$ is simply defined as:
		\begin{equation}
			H \equiv m \ h
			\label{eq_enthalpie_H}
		\end{equation}
		\begin{equationterms}
		   \item where \tab $H$ \tab is measured in \si{joules} (\si{\joule}).
		\end{equationterms}

		\thermoquotebegin{O}
			% Original quote in German:
			% Die Abnahme des Wärmeinhaltes ist dem Betrage nach gleich dem Wärmewert der gewonnenen „Nutzarbeit“, zuzüglich der nach außen abgeleiteten Wärme, zuzüglich der Zunahme der kinetischen Energie pro kg Dampf.
			% Original translation in English:
			The decrease of the heat contents is equal to the heat value of the gained "useful work" plus the heat carried away to the outside plus the increase of kinetic energy per pound (or kilogram) of steam.
		\thermoquoteend{Aurel Stodola, 1904\\{\tiny(where the "heat contents" $\lambda$ is not yet named \textit{enthalpy})}}{\textit{Die Dampfturbinen}~\cite{stodola1904, stodola1905}\vspace{1em}\index{Stodola!Aurel}}%handmade vspace
		In practice, the term \textit{enthalpy} is often used even if it refers to specific enthalpy; the symbol and context help determine which variable is being referred to.

		By using the concept of enthalpy, equations~\ref{eq_grande_sfee} and~\ref{eq_petite_sfee_deltas} are simplified to become:
		\begin{IEEEeqnarray}{rCl}
			\dot{Q}_{1 \to 2} + \dot{W}_{1 \to 2} 	& = & \dot{m} \left( \Delta h + \Delta e_\text{mech.} \right) \label{eq_grande_sfee_deltas_h} \\
			q_{1 \to 2} + w_{1 \to 2} 		& = & \Delta h + \Delta e_\text{mech.}	\label{eq_petite_sfee_deltas_h}
		\end{IEEEeqnarray}

		Thus, in an open system, we see that heat and work transfers change the \emph{enthalpy} of the fluid, and not only its internal energy as in a closed system.

		\begin{anexample}
			In a nozzle, air expands without any work or heat transfer. It enters with a specific enthalpy of \SI{776}{\kilo\joule\per\kilogram} and a velocity of \SI[per-mode=symbol]{30}{\kilo\meter\per\hour} and exits at the same altitude, with an enthalpy of \SI{754}{\kilo\joule\per\kilogram}.
			
			What is the air ejection velocity?
				\begin{answer}
					We start from equation~\ref{eq_petite_sfee_deltas_h}:
						\begin{IEEEeqnarray*}{rCl}
							q_{1 \to 2} + w_{1 \to 2} 		& = & \Delta h + \Delta e_\text{mech.}\\
							\Delta e_\text{mech.} 			& = & -\Delta h + 0 + 0 \\
							\frac{1}{2}\left(C_2^2 - C_1^2\right) & = & - \Delta h\\
							C_2 &=& \left[ -2 \ \Delta h + C_1^2\right]^{\frac{1}{2}}
						\end{IEEEeqnarray*}
					So $C_2 = \left[-2\times(\num{754e3} - \num{776e3}) + \left(\frac{30}{\num{3,6}}\right)^2 \right]^{\frac{1}{2}}
						= \SI{209,9}{\metre\per\second} = \SI{755,7}{\kilo\metre\per\hour}$.
							\begin{remark}Care must be used with conversions: in the equations, velocities and energies are always in \textsc{si} units.\end{remark}
				\end{answer}
		\end{anexample}


\section{Quantifying Work with an Open System}


	\subsection{Work of a fluid in slow evolution}
	\index{work!in an open system, in slow evolution}\index{system!open system, work in slow evolution}

		We have seen that when the fluid evolves slowly, the work done by a closed system can be quantified by performing the integral $-\int p \diff v$ (\ref{eq_travail_pdv}). With an open system, the expression is slightly different. To develop it, we propose to study the continuous compression of a fluid passing through a compressor.

		To do this, let us first observe the transformation of a fixed mass quantity $m_A$ circulating in the compressor (\cref{fig_travail_so_vu_de_sf}). As it passes between the moving blades, its pressure varies by $\tdiff p$ and its volume by $\tdiff v$. This is simply a moving closed system: since the fluid evolves very slowly (reversible evolution), the work $\tdiffi w_{m_\A}$ received by the system will be:
		\begin{equation}
			\diffi w_{m_\A} = -p \diff v
		\end{equation}
		\begin{equationterms}
			\item during a reversible evolution,
			\item and where the notation $\tdiffi$ is used to denote the infinitesimal variation of work (work being a path quantity, see annex \ref{ch_grandeurs_etat_chemin} \pagerefp{ch_grandeurs_etat_chemin}).
		\end{equationterms}

		\begin{figure}
			\begin{center}
				\includegraphics[width=\textwidth]{travail_sf_so1}
			\end{center}
			\supercaption{A fixed mass quantity $m_A$ flows from left to right through a compressor. It is compressed: its properties change from $p$ and $v$ to $p + \diff p$ and $v + \diff v$. If we consider the point of view of a closed system in transit, the work transfer is $\tdiffi w_{m_\A} = - p \diff v$.}{\vspace{-1em}\wcfile{Travail d\%E2\%80\%99un système ouvert.svg}{Diagram} \cczero \oc}%handmade vspace
			\label{fig_travail_so_vu_de_sf}
		\end{figure}

		Now, let's observe the course of this \emph{same} phenomenon from the point of view of an open system (\cref{fig_travail_so_vu_de_so}). What specific power $\tdiffi w_\text{O.S.}$ must be supplied to the compressor so that each fluid particle receives work $\tdiffi w_{m_\A}$?

		\begin{figure}
			\begin{center}
				\includegraphics[width=\textwidth]{travail_sf_so2-en}
			\end{center}
			\supercaption{The same flow as in \cref{fig_travail_so_vu_de_sf}, now observed from the viewpoint of a stationary open system crossed from left to right by a continuous flux. We seek to quantify the work $\tdiffi w_\text{O.S.}$ to be supplied to the system so that each mass quantity $m_\A$ receives work $\tdiffi w_{m_\A}$.}{\wcfile{Travail d\%E2\%80\%99un système ouvert.svg}{Diagram} \cczero \oc}
			\label{fig_travail_so_vu_de_so}
		\end{figure}

		\clearfloats
		The open system has four work transfer forms:
	
		\begin{description}
			\item [The specific insertion power] $w_\text{insertion}$ (\ref{eq_puissance_spe_insertion}) is due to the permanent arrival of the fluid at the system's inlet. From the viewpoint of the open system, we have:
				\begin{equation}
					w_\text{insertion} = +p \ v
				\end{equation}

			\item [The specific compression power] $-\tdiffi w_{m_\A}$ is the specific work that the open system must transfer to each mass quantity $m_A$ to effectively compress it:
				\begin{equation}
					-\tdiffi w_{m_\A} = -(-p \diff v)
					\label{eq_travail_so_wma}
				\end{equation}
		
			\item [The specific extraction power] $w_\text{extraction}$ is spent by the open system to continuously remove the fluid.
		
				At the exit, the fluid properties have become $p + \tdiff p$ for pressure, and $v + \tdiff v$ for volume. Thus, we have:
				\begin{equation}
					w_\text{extraction} = -(p + \tdiff p) (v + \tdiff v)
				\end{equation}
				
			\item [The specific power received from the outside] $\tdiffi w_\text{O.S.}$ is the power that feeds the compression: this is the quantity we aim to quantify.
		\end{description}

		These four powers cancel each other out, since the total work transfer involved in the flow does not depend on the adopted viewpoint:
		\begin{equation}
			\diffi w_\text{O.S.} + w_\text{insertion} + (-\tdiffi w_{m_\A}) + w_\text{extraction} = 0
		\end{equation}

		Therefore, we can quantify the specific power $\diffi w_\text{O.S.}$ that must be supplied to the compressor:
		\begin{IEEEeqnarray*}{rCl}
			\diffi w_\text{O.S.} 	& = & - w_\text{insertion} + \tdiffi w_{m_\A} - w_\text{extraction} \\
			\diffi w_\text{O.S.} 	& = & -p \ v + (-p \tdiff v) + (p + \tdiff p) (v + \tdiff v) \\
					& = & -p \ v - p \diff v + p \ v + p \diff v + \diff p \ v + \diff p \ \diff v \\
					& = & \tdiff p \ v + \diff p \ \diff v
		\end{IEEEeqnarray*}

		And since the product $\tdiff p \times \tdiff v$ tends to zero when using infinitesimal quantities, we obtain the surprising expression:
		\begin{equation}
			\diffi w_\text{O.S.} = v \diff p
			\label{eq_travail_vdp}
		\end{equation}

		The terms $\tdiff p$ and $\tdiff v$ in our study are not necessarily positive: this expression applies equally to expansions and compressions, as long as they are reversible.
	
		By integrating this expression \ref{eq_travail_vdp} to apply it to the general case in continuous flow, we obtain:
		\begin{IEEEeqnarray}{rCl}
			w_\text{O.S.} 			& = & \int v \diff p 					\label{eq_travail_w_rév_so} \\
			\dot{W}_\fromatob 	& = & \dot{m} \int_\A^\B v \diff p	\label{eq_travail_W_rév_so}
		\end{IEEEeqnarray}
		\begin{equationterms}
			\item when the flow is continuous,
			\item when the evolution is reversible,
			\item and regardless of the heat input.
		\end{equationterms}

		Thus, when we want to quantify reversible work in an open system, it is the integral $+\int v \diff p$ that needs to be calculated, and not $-\int p \diff v$.
		
		On a pressure-volume diagram, we can visualize this work by adding the insertion work and extraction work to the compression work, as shown in \cref{p-v_travail_so_construction}.

		\begin{figure}
			\begin{center}
				\includegraphics[width=8cm]{pv_systeme_ouvert_construction}
			\end{center}
			\supercaption{Work received by an open system crossed by a fluid, during a slow evolution.\\
			The system first receives the insertion work ($p_\text{ini.} v_\text{ini.}$, in orange, positive) to enter the system,
			then it spends compression work (hatched area, negative),
			and finally, it spends extraction work ($p_\text{fin} v_\text{fin.}$, in blue, negative).\\
			The net sum of these three areas is the specific power to be supplied to the open system.}{\wcfile{P-v diagram specific work open system construction.svg}{Diagram} \cczero \oc}
			\label{p-v_travail_so_construction}
		\end{figure}

		\begin{figure}
			\begin{center}
				\includegraphics[width=\textwidth]{pv_systeme_ouvert}
			\end{center}
			\supercaption{Work measured in an open system, during a reversible evolution.\\
			The integral of $v \diff p$ is visualized by the area to the left of the curve. \\
			If the fluid returns to its initial state (having completed a \vocab[cycle!thermodynamique]{thermodynamic cycle}), the developed work is visualized by the area enclosed within the curve. In this case, the quantification is the same for closed and open systems.}{\wcfile{P-v diagram specific work open system.svg}{Diagram} \cczero \oc}
			\label{p-v_travail_so}
		\end{figure}

		The reversible work done in steady, reversible flow is thus visualized by the area enclosed \textit{to the left} of the curve, as shown in \cref{p-v_travail_so}.

		\clearpage %handmade
		\begin{anexample}\index{pump}
			A liquid pump slowly compresses a water flow of~\SI{2}{\kilogram\per\second} from \SI{1}{\bar} to \SI{20}{\bar}. During compression, the specific volume of water remains constant at $v_L = \SI{e-3}{\metre\cubed\per\kilogram}$. What is the power required in the form of work?
				\begin{answer}
					On a pressure-volume diagram and qualitatively (i.e., without representing numerical values), the evolution can be depicted like this:
							\begin{center}
								\includegraphics[width=3cm]{exe_pv_isochore_so}
							\end{center}
					We use equation~\ref{eq_travail_W_rév_so}, being cautious with the units. Since $v$ is independent of $p$, integration is straightforward: $\dot{W}_\fromatob 	= \dot{m} \int_\A^\B v \diff p = \dot{m} \ v_L \int_\A^\B \diff p = \dot{m} \ v_L \left[p\right]_{p_\A}^{p_\B} = 2 \times \num{e-3} \left(\num{20e5} - \num{1e5}\right) = \SI{+3,8e3}{\watt} = \SI{+3,8}{\kilo\watt}$.
				\end{answer}
					\begin{remark}This power is indeed positive, wince the fluid in the system is receiving the work.\end{remark}
					\begin{remark}Here the specific volume $v_L$ is constant (as always with liquid water). If it were the pressure that was constant, then the work would be zero even if $v$ were to vary.\end{remark}
		\end{anexample}

		\begin{anexample}
			A compressor slowly compresses an air flow of~\SI{2}{\kilogram\per\second} from \SI{1}{\bar} to \SI{20}{\bar}. During compression, the specific volume and pressure of the air are related by the expression $p \ v^{\num{1.35}} = k$. At the inlet, the specific volume of the air is $v_\A = \SI{0.8}{\metre\cubed\per\kilogram}$.\\
			What is the power required in the form of work?
				\begin{answer}
				On a pressure-volume diagram and qualitatively, the evolution can be depicted like this:
							\begin{center}
								\includegraphics[width=3cm]{exe_pv_exp_so}
							\end{center}
					Here the specific volume is a function of pressure: we have $v = \left(\frac{k}{p}\right)^{\frac{1}{\num{1,35}}} = k^{\frac{1}{\num{1,35}}} p^{-\frac{1}{\num{1,35}}}$. We start from equation~\ref{eq_travail_W_rév_so} : \\
					$\dot{W}_\fromatob
					= \dot{m} \int_\A^\B v \diff p
					= \dot{m} \ k^{\frac{1}{\num{1,35}}} \int_\A^\B p^{-\frac{1}{\num{1,35}}} \diff p
					= \dot{m} \ k^{\frac{1}{\num{1,35}}} \left[\frac{1}{-\frac{1}{\num{1,35}} +1} p^{-\frac{1}{\num{1,35}} +1}\right]_{p_\A}^{p_\B}\\
					= \dot{m} \ \left(p_\A v_\A^{\num{1,35}}\right)^{\frac{1}{\num{1,35}}} \frac{1}{\num{0,25926}} \left[p^{\num{0,25926}}\right]_{p_\A}^{p_\B}\\
					= 2 \left(\num{1e5} \times \num{0,8}^{\num{1,35}}\right)^{\frac{1}{\num{1,35}}} \frac{1}{\num{0,25926}} \left[\left(\num{20e5}\right)^{\num{0,25926}} - \left(\num{1e5}\right)^{\num{0,25926}}\right]\\
					= \SI{+7,247e5}{\watt} = \SI{+724,7}{\kilo\watt}$.
				\end{answer}
					\begin{remark}Here the key is to describe the function $v_{(p)}$ accurately before proceeding with the integration.\end{remark}
					\begin{remark}The power of the compressor is \num{190} times larger than that of the pump in the previous example. Additionally, the specific volume of the air at the inlet is~\num{800} times larger: a much larger machine will be required (it must handle a volumetric flow rate $\dot{V}_\A = \dot{m} \ v_\A = \SI{1.6}{\metre\cubed\per\second} = \SI{1600}{\liter\per\second}$ at the inlet).\index{pump}\end{remark}
		\end{anexample}


	\subsection{Work of a fluid in rapid evolution}
	\index{work!in an open system, in rapid evolution}\index{work!irreversible transfers}\index{system!open system, work in rapid evolution}
	
		When the fluid evolves rapidly (as is always the case in practice), we encounter the phenomena described in the previous chapter (\S\ref{ch_evolutions_irr_sf}): the pressure exerted on the moving walls no longer corresponds to the "average" pressure inside the fluid. The work required in compressions is greater and the work received during expansions is less than during slow evolutions.
		
		Using an open system to account for energy transfers does not change the problem, of course. We do not have the means to predict analytically the work required for compression at a given speed. The problem --calculating the spatial distribution of pressure inside the fluid over time-- falls within the scope of fluid mechanics, and will be solved on a case-by-case basis.
		
		On our pressure-volume diagrams, we represent irreversible evolutions with a dashed line, to clearly differentiate them from reversible evolutions (\cref{fig_pv_evolution_irr_so}).

		\begin{figure}
			\begin{center}
				\includegraphics[width=8cm]{pv_compression_irreversible_so}
			\end{center}
			\supercaption{Reversible (solid line) and irreversible (dashed line) compressions represented on a pressure-volume diagram. In an open system, work transfers can be visualized with the area to the left of the curve, but only when the evolutions are reversible.}{\wcfile{P-v diagram reversible vs irreversible evolution open system.svg}{Diagram} \cczero \oc}
			\label{fig_pv_evolution_irr_so}
		\end{figure}
	
		\clearfloats%handmade
		\begin{anexample}\index{adiabatic!irreversible evolutions}\index{heat!transformation to work}\index{work!transformation to heat}
			Air is continuously compressed from 1 to \SI{20}{\bar} in a compressor. Just after leaving the compressor, the air enters a turbine that expands it from 20 to \SI{1}{\bar}. After leaving the turbine, the air is again inserted into the compressor.
			
			What will be the shape of the evolutions on a pressure-volume diagram?
					\begin{answer}
						If the evolutions occur infinitely slowly, the pressure and specific volume always pass through the same values during the back-and-forth movements:
							\begin{center}
								\includegraphics[height=4cm]{exe_pv_rev_so}
							\end{center}
						However, if the evolutions are carried out with realistic speed, at each trip the final specific volume is larger than it would have been during a slow trip:
							\begin{center}
								\includegraphics[height=4cm]{exe_pv_irr_so}
							\end{center}
						
						Thus, the properties gradually shift on the pressure-volume diagram. Unless the evolutions are infinitely slow, the compressor demands more energy than the turbine is able to extract upon return. This excess energy is absorbed by the air, increasing its internal energy and temperature.
					\end{answer}
			\end{anexample}


\section{Quantifying Heat with an Open System}
\index{heat!calculation in an open system}\index{system!open system, heat calculation}

With an open system, we will use the same method as with a closed system: since we cannot quantify heat transfers directly, we will always proceed by deduction.
Mathematically, we simply reuse equation~\ref{eq_grande_sfee_deltas_h} to obtain:
\begin{IEEEeqnarray}{rCl}
	\dot{Q}_{1 \to 2} & = & \dot{m} \left( \Delta h + \Delta e_\text{mech.}\right) - \dot{W}_{1 \to 2} \\
		q_{1 \to 2} 	& = & \Delta h + \Delta e_\text{mech.} - w_{1 \to 2}
\end{IEEEeqnarray}
\begin{equationterms}
	\item for an open system.
\end{equationterms}
	
	Once again, the main challenge in quantifying a heat transfer is predicting and quantifying the change in enthalpy, $\Delta h$. For gases, $h$ is almost proportional to temperature; for liquids and vapors, the relationship is more complex. We will learn how to quantify enthalpy in fluids in \chapterfour and \chapterfive.
\index{system!open system|)}

