\begin{boiboiboite}
	\propair
	\propgaz
	\isentropics
\end{boiboiboite}

\subsubsection{Knowledge Recap Questions}
	
	\begin{enumerate}
		\item Why do piston engines allow for higher combustion temperatures than gas turbines?
		\item What advantage does the Diesel cycle have over the Otto cycle?
		\item Represent the cycle followed by the air in a single-turbine turbojet on a pressure-volume diagram and on a temperature-entropy diagram, in a qualitative way.
		\item Why are two concentric spools (two shafts connecting a compressor and a turbine) used in some gas turbines?
		\item Represent the cycle followed by the air in an intercooled turboshaft engine with an economizer heat exchanger (\cref{fig_intercooler_echangeur}) on a temperature-entropy diagram, in a qualitative way.
	\end{enumerate}
\exerciseend

\subsubsection{Gasoline engine}
\label{exo_cycle_moteur_essence_dix}\index{Otto cycle}\index{cycle!d’Otto}

	We propose to study the basic operation of the piston/cylinder engine of a private plane (\cref{fig_photos_moteur_essence}). The engine is referred to as a "gasoline engine" and is based on the theoretical Otto cycle.

\begin{figure}
	\begin{center}
		\includegraphics[width=0.8\columnwidth]{photo_continental_io-550}
		\includegraphics[width=0.8\columnwidth]{photo_sr22}
	\end{center}
	\supercaption{The six-cylinder gasoline fuel injection \wed{Continental IO-550}{\textit{Continental} \textsc{io-550}} of~\SI{300}{hp}, in production since 1983. It equips among others the \wed{Cirrus SR22}{\textit{Cirrus} \textsc{sr22}} aircraft.\\
		This photo shows the turbocharged version of the engine, with the intercooler visible in the top left corner.}{\wcfile{TSIOF-550-D}{Engine photo} \ccbysa by \wcu{FlugKerl2};\\ \wcfile{IAcirrus.JPG}{Aircraft photo} \ccbysa by \wcu{Airman7474}.}
	\label{fig_photos_moteur_essence}
\end{figure}

	\begin{itemize}
		\item At the beginning of the cycle, the air is at~\SI{21}{\degreeCelsius} and~\SI{1}{\bar};
		\item The specific heat supplied during each cycle in cruise flight is~\SI{500}{\kilo\joule\per\kilogram};
		\item The compression ratio $\epsilon \equiv \frac{V_\max}{V_\min}$ is~\num{7}.
	\end{itemize}

In this study, we consider that compression and expansion are isentropic and that heat addition and rejection occur at constant volume.

	\begin{enumerate}
		\item Draw the cycle followed on a pressure-volume or temperature-entropy diagram, in a qualitative way and representing all heat and work transfers.
		\item What are the air temperatures at the beginning and at the end of combustion?
		\item What is the amount of heat rejected during cooling?
		\item What is the efficiency of this theoretical engine cycle?
		\item In practice, the evolution of air on the pressure-volume diagram is very different from the cycle described by Otto. Propose two reasons explaining this.
		\item It is observed that when the aircraft gains altitude, the power that the engine can provide decreases significantly. What modification can be made to the engine to compensate for this?
	\end{enumerate}
\exerciseend

\subsubsection{Diesel engine}
\label{exo_cycle_moteur_diesel}\index{Diesel engine}\index{Diesel cycle}\index{turbocharging}

	A piston-cylinder engine used to propel a ship (\cref{fig_photos_moteur_diesel}) is turbocharged by a turbo that increases the pressure and temperature of the intake air using energy extracted from the exhaust gases (the turbocharger is a component that does not require any external input of energy in the form of work or heat, see \S\ref{ch_turbo} \pagerefp{ch_turbo}). The engine has the following operating characteristics:
	\begin{itemize}
		\item the air admitted to the cylinders is at~\SI{115}{\degreeCelsius} and~\SI{3}{\bar};
		\item the specific heat supplied each cycle is~\SI{1250}{\kilo\joule\per\kilogram};
		\item the compression ratio $\epsilon \equiv \frac{V_\max}{V_\min}$ is~\num{17}.
	\end{itemize}

\begin{figure}
	\begin{center}
		\includegraphics[width=0.9\columnwidth]{photo_diesel_generateur}
		\includegraphics[width=0.9\columnwidth]{photo_diesel_propulsion}
	\end{center}
	\supercaption{The two Diesel engines of a~\SI{290,000}{\tonne} oil tanker: a six-cylinder~\SI{1100}{\kilo\watt} generator (top) and a seven-cylinder~\SI{25}{\mega\watt} propulsion engine (bottom).}{\wcfile{Diesel generator on an oil tanker}{Photos 1} and \wcfile{Main engine of a VLCC tanker 3}{2} \ccbysa by Hervé Cozanet}
	\label{fig_photos_moteur_diesel}
\end{figure}

We consider the optimal operating case, that is, following the Diesel cycle, according to the following characteristics:
	\begin{itemize}
		\item isentropic compression and expansion;
		\item combustion at constant pressure;
		\item heat rejection at constant volume.
	\end{itemize}

	\begin{enumerate}
		\item Draw the thermodynamic cycle followed on a pressure-volume or temperature-entropy diagram, in a qualitative way, indicating all heat and work transfers.
		\item What is the air temperature at the end of the compression?
		\item What is the gas temperature at the end of the combustion?
		\item What is the maximum pressure reached in the engine?
		\item What is the temperature at the end of the expansion?
		\item What is the engine cycle efficiency?
		\item It is easy to show that at the same compression ratio, a Diesel cycle is less efficient than a so-called "gasoline" cycle (Otto cycle). Why is it used nevertheless?
	\end{enumerate}
\exerciseend

\subsubsection{Turboprop engine}
\label{exo_cycle_turbopropulseur}\index{turboprop}

	A regional airliner is powered by two turboprop engines (\cref{fig_turboprop_photos}). In each of them, a single turbine drives an axial compressor, as well as the propeller through a gearbox (\cref{fig_turboprop_circuit}).

\begin{figure}
	\begin{center}
		\includegraphics[width=11cm]{circuit_turboprop}
	\end{center}
	\supercaption{Internal arrangement principle of a turbopropeller.}{\wcfile{Overall layout of a turboprop engine and a turboshaft engine.svg}{Diagram} \ccbysa by \olivier}
	\label{fig_turboprop_circuit}
\end{figure}

During cruise, the air flow within the engine is~\SI{4.9}{\kilogram\per\second}, and the circuit is as follows:
	\begin{itemize}
		\item Air at ambient pressure and temperature (\SI{0.55}{\bar} and \SI{-5}{\degreeCelsius}) is admitted into the compressor;
		\item The compressor raises the air pressure to~\SI{7.6}{\bar} with an isentropic efficiency of~\SI{80}{\percent};
		\item The air is then heated in the combustion chamber to~\SI{1315}{\degreeCelsius};
		\item The combustion gases are then expanded in the turbine and ejected into the atmosphere; the turbine has an isentropic efficiency of~\SI{80}{\percent}.
	\end{itemize}

The turbine drives the compressor (through a shaft with negligible friction) and the propeller (through a transmission box with an efficiency of~\SI{83}{\percent}).

We want to quantify the power actually received by the propeller during flight.

	\begin{enumerate}
		\item Draw the cycle followed by the air on a temperature-entropy diagram, in a qualitative way.
		\item What is the air temperature at the compressor outlet?
		\item What is the gas temperature at the turbine outlet?
		\item What is the power supplied to the propeller?
	\end{enumerate}

	\index{extraction!in compressor}\index{compressor!extraction}
	In order to de-ice the wings, a small gas bleed is carried out in the compressor. The extraction flow rate is~\SI{0.1}{\kilogram\per\second}, and the air temperature is~\SI{200}{\degreeCelsius}.

	\begin{enumerate}
		\shift{4}
		\item Propose and quantify a modification to the engine operation so that it may supply the propeller with the same power.
	\end{enumerate}

	\begin{figure}[h!]
		\begin{center}
			\includegraphics[width=0.9\columnwidth]{photo_pwc_pw123}
			\includegraphics[width=0.9\columnwidth]{photo_dash8}
		\end{center}
		\supercaption{A \textit{Pratt \& Whitney Canada} \textsc{pwc123} turboprop engine powering a \textit{Bombardier} Dash~8. The \textsc{pwc123} is configured with three concentric rotating assemblies, with the engine shaft powered by a free turbine, but its operation principle remains similar to that described in \cref{fig_turboprop_circuit}.}{\wcfile{Pratt & Whitney Canada PW123 retouched}{Engine photo} derived from \wcfile{Pratt & Whitney Canada PW123}{a photo} \ccby by \flickrcodename{28567825@N03}{cliff1066}; \wcfile{Dash 8 front view}{Aircraft photo} \ccbysa by \flickrcodename{24056116@N07}{Björn}}
		\label{fig_turboprop_photos}
	\end{figure}
\exerciseend

\subsubsection{Modification of a Turbojet}
\label{exo_cycle_turboreacteur}\index{single-flow turbojet}\index{multiple shaft (turbomachinery)}\index{turbine!multiple}

	A single-flow turbojet operates with a single engine spool (single compressor and single turbine). Its operating characteristics are as follows:
	\begin{itemize}
		\TabPositions{0.55\columnwidth}%handmade, to make the layout fit
		\item Air flow rate:				\tab \SI{4}{\kilogram\per\second}
		\item Atmospheric conditions: 		\tab \SI{283}{\kelvin} \& \SI{0.95}{\bar}
		\item Pressure ratio $\frac{p_\maxd}{p_\mind}$: 	\tab \num{25}
		\item Maximum temperature: 							\tab \SI{1300}{\kelvin}
		\item Isentropic efficiency of the compressor and turbine:\\ 	\tab \SI{85}{\percent}
	\end{itemize}

	We are looking to quantify its performance before it is modified.
	\begin{enumerate}
		\item Represent the components of the turbojet and the thermodynamic cycle followed by the air on a temperature-entropy or pressure-volume diagram.
		\item What is the pressure available at the turbine outlet?
		\item What speed would the gases reach at the nozzle outlet if the expansion were isentropic?
	\end{enumerate}

	\begin{figure}[ht!]
		\begin{center}
			\includegraphics[width=\columnwidth]{photo_pw_j52_1}
			%\includegraphics[width=\columnwidth]{photo_pw_j52_2}
		\end{center}
		\supercaption{A single-flow turbojet with twin spools \wed{Pratt \& Whitney J52}{\textit{Pratt \& Whitney} \textsc{j52}} (or \textsc{jt8a}), built in \num{4500} units. It still equips the \wed{Northrop Grumman EA-6B Prowler}{\textsc{ea-6b} \textit{Prowler}}.}%
		{\wcfile{Pratt & Whitney J52 retouched}{Photo 1} derived from a photo \ccby by \flickrname{37467370@N08}{Greg Goebel}\\
			\wcfile{J52 engine maintenance USS America (CV-66) 1993.JPEG}{Photo 2} by Sgt. G. Robinson, U.S.~Navy (\pd, cropped)}
		\label{fig_exo_turbojet_twin_spool}
	\end{figure}

	The team of engineers in charge of designing the components proposes to modify the engine by using two spools instead of one (\cref{fig_exo_turbojet_twin_spool}). The rotating assembly closest to the center of the engine can operate at higher speeds, increasing the isentropic efficiency of the components:
	\begin{itemize}
		\item Isentropic efficiency of the low-pressure compressor and turbine (\textsc{lp} spool): \SI{85}{\percent}\\
			(pressure ratio: \num{2})
		\item Isentropic efficiency of the high-pressure compressor and turbine (\textsc{hp} spool): \SI{90}{\percent}\\
			(pressure ratio: \num{12.5})
	\end{itemize}

	All other operating characteristics of the engine remain unchanged.
	\begin{enumerate}
	\shift{3}
		\item What is the new available pressure at the turbine outlet?
		\item What is the new theoretical exhaust gas velocity?
	\end{enumerate}
\exerciseend

\subsubsection{Intercooled Turboshaft}
\label{exo_cf6_generateur_intercooling}\index{turboprop}\index{intercooling}\index{multiple shaft (turbomachinery)}\index{turbine!multiple}\index{intermediate cooling}

	You are tasked by a small company to develop an engine that will be used to generate electricity in a factory. It is decided to base the engine on a turbofan jet engine from a retired commercial aircraft: it is a venerable \textit{General Electric} \textsc{cf6} (figures~\ref{fig_cf6_1} and~\ref{fig_cf6_2}).
	\begin{figure}[ht!]
		\begin{center}
		\includegraphics[width=10cm]{cf6_cutaway}
		\end{center}
		\supercaption{Cutaway diagram of a \wed{General Electric CF6}{\textit{General Electric} \textsc{cf6-6}}. The engine propelled all major long-haul aircraft families of the 1970s and 1980s.}{\wcfile{CF6-6 engine cutaway}{Diagram} \pd U.S. FAA}
		\label{fig_cf6_1}
	\end{figure}

	\begin{figure}[ht!]
		\begin{center}
		\vspace{-0.5cm}%handmade
		\includegraphics[width=10cm]{circuit_turbofan_twin_spool}
		\end{center}
		\supercaption{Schematic diagram of the arrangement of the \textit{General Electric} \textsc{cf6}.}{\wcfile{Overall layout of General Electric CF6.svg}{Diagram} \ccbysa \olivier}
		\label{fig_cf6_2}
	\end{figure}

	The turbofan engine has two concentric spools:

	\begin{itemize}
		\item The low-pressure spool connects the fan, a compressor section called the \textit{booster}, and the low-pressure turbine;
		\item The high-pressure spool connects the rest of the compressor to the high-pressure turbine.
	\end{itemize}

	The turbofan engine has the following properties:
	\begin{description}
		\TabPositions{0.75\columnwidth}%handmade, to make data fit
		\item Maximum pressure ratio: 					\tab \num{29.3}
		\item Booster pressure ratio: 					\tab \num{1.2}
		\item Fan pressure ratio: 						\tab \num{1.2}
		\item Maximum temperature: 						\tab \SI{1300}{\degreeCelsius}
		\item Isentropic efficiency of compressors: 	\tab \SI{85}{\percent}
		\item Isentropic efficiency of turbines: 		\tab \SI{85}{\percent}
		\item Exhaust gas pressure into the atmosphere: 	\SI{1.1}{\bar}
	\end{description}

	In order to convert the turbofan into a turboshaft engine, you have the nacelle and the fan removed, and mechanically connect the low-pressure spool to the generator (\cref{fig_turboprops_exercise}). The turboshaft engine is started at atmospheric conditions of \SI{1}{\bar} and \SI{18}{\degreeCelsius}. At full throttle, it uses an air flow rate of \SI{80}{\kilogram\per\second}.
	\begin{enumerate}
		\item Represent the thermodynamic cycle followed by the air on a pressure-volume diagram, qualitatively.
		\item What is the mechanical power developed by the machine?
		\item What is its work ratio?
		\item What is its efficiency?
	\end{enumerate}

	The client company receives your engine but wishes to increase its power. Since the engine is already operating at full capacity, you are unable to increase either the air mass flow rate or the combustion temperature.

	In order to increase the power, you install an intercooling system (figure~\ref{fig_turboprops_exercise}). The air compression is interrupted at a pressure of \SI{7}{\bar}; the air is led into a large heat exchanger where it is cooled at constant pressure. Once its temperature has dropped back to \SI{40}{\celsius}, compression resumes in the compressor, which has not been modified.
	\begin{enumerate}
	\shift{4}
		\item Represent qualitatively the new thermodynamic cycle on the pressure-volume diagram above.
		\item What is the increase in mechanical power?
		\item What is the new work ratio?
		\item What is the new efficiency?
	\end{enumerate}

	\begin{figure}[ht!]%handmade
		\begin{center}
		\includegraphics[width=12cm]{circuit_turboshaft_exercice}
		\end{center}
		\supercaption{Top: schematic diagram of a turboshaft based on the \textsc{cf6} from which the fan has been removed.\\
					Bottom: the same turboshaft engine modified by the addition of an intercooling system.}{\wcfile{Overall layout of a twin-spool turboshaft engine.svg}{Diagrams} \ccbysa \olivier}
		\label{fig_turboprops_exercise}
	\end{figure}
\exerciseend

\exercisesolutionpage
\titreresultats

	\begin{description}[itemsep=2em]
		\item [\ref{exo_cycle_moteur_essence_dix}]
			\tab 1) See \cref{fig_cycle_otto} \pagerefp{fig_cycle_otto};
			\tab 2) $T_\B = T_\A \left(\frac{v_\A}{v_\B}\right)^{\gamma_\text{air} - 1} = \SI{640,6}{\kelvin} = \SI{367,5}{\degreeCelsius}$ (\ref{eq_isentropique_horrible1}) and $T_\C = \frac{q_\text{combustion}}{c_{v\text{ (gaz)}}} + \frac{c_{v\text{ (air)}}}{c_{v\text{ (gaz)}}} T_\B = \SI{1166,3}{\kelvin} = \SI{893,3}{\degreeCelsius}$;
			\tab 3) $T_\D = \SI{610,16}{\kelvin} = \SI{337}{\degreeCelsius}$ (\ref{eq_isentropique_horrible1}) so $q_\fromdtoa = c_{v\text{ (gaz)}} (T_\D - T_\A) = \SI{-260,1}{\kilo\joule\per\kilogram}$;
			\tab 4) $\eta_\text{engine} = \frac{q_\net}{q_\inn} = \SI{47.99}{\percent}$ (purely theoretical value, since compressions and expansions are reversible: in practice, experct around \SI{35}{\percent});
			\tab 5) See \S\ref{ch_cycles_pistons_reels} \pagerefp{ch_cycles_pistons_reels};
			\tab 6) Power decreases since the air density of the atmosphere decreases with altitude. In order to increase $\dot m_\text{air}$, one can, for example, install a turbocharging system (cf.\ \S\ref{ch_turbo}) as shown in \cref{fig_photos_moteur_essence}.
		\item [\ref{exo_cycle_moteur_diesel}]
			\includegraphics[width=\solutiondiagramwidth]{exo_sol_ts_diesel}
			\tab 2) $T_\B = \SI{1205.5}{\kelvin} = \SI{932.4}{\degreeCelsius}$ (\ref{eq_isentropique_horrible1});
			\tab 3) $T_\C = \frac{q_\text{combustion}}{c_{p \text{(gases)}}} + \frac{c_{p \text{(air)}}}{c_{p \text{(gases)}}} T_\B = \SI{2140.5}{\kelvin} = \SI{1867.3}{\degreeCelsius}$;
			\tab 4) $p_\C = p_\B = \SI{158.4}{\bar}$;
			\tab 5) Using equation \ref{eq_isentropique_horrible1}, $\frac{T_\D}{T_\C}
					= \left(\frac{v_\D}{v_\C}\right)^{\gamma_\gases - 1}
					= \left(\frac{v_\A}{v_\B} \frac{v_\B}{v_\C}\right)^{\gamma_\gases - 1}
					= \left[\epsilon \frac{R_\text{air}}{R_\gases} \frac{T_\B}{T_\C}\right]^{\gamma_\gases - 1}$
					Therefore $T_\D = \SI{1053.6}{\kelvin} = \SI{780.4}{\degreeCelsius}$ (note that these gases will still need to power the turbocharger's turbine before being ejected into the atmosphere);
			\tab 6) $\eta_\text{engine} = \frac{q_\net}{q_\inn} = \SI{56.19}{\percent}$ (close to reality since these engines are very slow);
			\tab 7) See sections \S\ref{ch_cycle_diesel} \pagerefp{ch_cycle_diesel} and \S\ref{ch_cycles_pistons_reels} \pagerefp{ch_cycles_pistons_reels}.
		\item [\ref{exo_cycle_turbopropulseur}]
			\tab 1) See \cref{fig_turboprop} \pagerefp{fig_turboprop};
			\tab 2) $T_\B = \SI{6442.8}{\kelvin} = \SI{369.6}{\degreeCelsius}$ (\ref{eq_isentropique_horrible2} \& \ref{eq_puissance_compresseur});
			\tab 3) $T_\D = \SI{976.9}{\kelvin} = \SI{703.8}{\degreeCelsius}$ (\ref{eq_isentropique_horrible2} \& \ref{eq_puissance_turbine_gaz});
			\tab 4) $\dot W_\text{propellers} = - \dot m \ \eta_\text{transmission} \ (w_\text{turbine} + w_\text{compressor}) = \SI{+1.517}{\mega\watt}$ (with a thermal efficiency before transmission of~\SI{27.6}{\percent}, a slightly lower value than reality);
			\tab 5) One possibility: increase $\dot m_\text{engine-air}$ without modifying the temperatures. Then, $\dot m_\text{input~engine~2} = \SI{5.774}{\kilogram\per\second}$ (\SI{+0.174}{\kilogram\per\second}).
		\item [\ref{exo_cycle_turboreacteur}]
			\tab 1) See \cref{fig_turboreacteur} \pagerefp{fig_turboreacteur};
			\tab 2) $T_\B = \SI{785.2}{\kelvin}$, thus $T_\D = \SI{861.1}{\kelvin}$ and $T_{\D'} = \SI{783.6}{\kelvin}$: $p_\D = \SI{3.13}{\bar}$;
			\tab 3) Neglecting $C_\D$, and with complete and reversible expansion, $C_\E = \SI{714.3}{\meter\per\second}$ (the same remarks as in example \ref{exemple_tuyere} \pagerefp{exemple_tuyere} apply here);
			\tab 4) The temperature at the start of combustion drops to $T_3 = \SI{774.2}{\kelvin}$, the temperature at the turbine outlet is $T_6 = \SI{870.7}{\kelvin}$, and thus the pressure at the nozzle inlet rises to $p_6 = \SI{4.684}{\bar}$;
			\tab 5) Cover your ears: $C_7= \SI{811.3}{\meter\per\second}$ (the same remarks apply here as well).
		\item [\ref{exo_cf6_generateur_intercooling}]
			\includegraphics[width=\solutiondiagramwidth]{exo_sol_ts_intercooler}
			\tab 2) $\dot W_\net = \dot m \ c_{p \text{(gases)}} \ (T_\D - T_\C) + c_{p \text{(air)}} \ (T_\B - T_\A) = \SI{-3.536}{\mega\watt}$ (approximately \SI{3400}{hp}, not too bad for a machine first run in 1971… even after 15 years of service hung under a wing, a \textsc{cf6} still sells for several million euros);
			\tab 3) $M_{w1} = \SI{38.2}{\percent}$;
			\tab 4) $\eta_1 = \SI{31.49}{\percent}$;
			\tab 6) $\dot W_{\net 2} = \SI{-3.325}{\mega\watt}$, that is a remarkable increase of \SI{31}{\percent};
			\tab 7) $M_{w1} = \SI{50}{\percent}$, an increase of \SI{+11.8}{pt};
			\tab 8) Power increases by \SI{98.8}{\kilo\joule\per\kilogram}, while consumption increases by \SI{332.6}{\kilo\joule\per\kilogram}, resulting in a marginal efficiency of \SI{29.7}{\percent}. The overall efficiency decreases to $\eta_2 = \SI{31.04}{\percent}$, only \SI{-0.5}{pt}… an interesting compromise!
	\end{description}

