\atstartofhistorysection
\section[A Bit of History: the Napier Nomad]{A Bit of History:\\ the Napier Nomad}
\label{ch_histoire_nomad}\index{Napier Nomad (engine)}

At the end of the Second World War, the British government issued a call for tenders for the development of a highly efficient, \SI{6000}{hp} aeronautical engine, in order to foster the development of military and civilian aircraft. The British engine manufacturer \textit{Napier \& Son} then carried out research that led to the development of a curious and remarkable device: the \textit{Napier Nomad}.

\index{Diesel engine}\index{two-stroke engine}\index{direct injection}\index{engine reactivity}\index{cooling!intermediate}\index{intercooling}\index{turbopropeller}
Based on a twelve-cylinder two-stroke Diesel engine with direct injection, the \textit{Nomad} also featured all the elements of a turboprop engine. In order to increase the pressure and temperature at which heat is supplied, the two units were mounted \emph{in series}, however, in order to allow for a high efficiency at all speeds, each drove one of the two contra-rotating propellers (Figures~\ref{fig_napier_i_circuit} and~\ref{fig_napier_i_photos}). Finally, in order to achieve high powers and increase reactivity throughout the flight envelope, an intercooler and reheat system were added. The result: an astonishing and extravagant mechanical-thermal assembly seemingly produced by the out-of-control fantasy of thermodynamic engineers on a quest for efficiency.

\begin{figure}[ht]%handmade
    \begin{center}
        \includegraphics[width=12cm]{napier_nomad_i_3}
    \end{center}
    \supercaption{Schematic diagram of the thermodynamic circuit of the \textit{Napier Nomad I}. The shafts of the turboshaft and of the Diesel engine each drive a propeller. However, the two units are mounted in series: the air first passes through the compressors, then through the cylinders, and finally through the turbine(s). The engine power, as was customary in 1950, is controlled using a single mechanical control lever!}%
    {\wcfile{NomadSchematic 185kBpng360kB}{Diagram} by users $\cdot$ Commons \wcun{Tataroko-common}{Tataroko-common}, \wcun{Aaa3-other}{Aaa3-other} \& \wcun{Nimbus227}{Nimbus227} (\pd)}
    \label{fig_napier_i_circuit}
\end{figure}

\begin{figure}[ht]%handmade
    \begin{center}
        \includegraphics[width=8cm]{napier_nomad_i_1}
        \includegraphics[width=8cm]{napier_nomad_i_2}
    \end{center}
    \supercaption{The prototype of the \textit{Napier Nomad I}. The displacement was \SI{40}{\liter} and the weight exceeded 2 tons.}%
    {Images edited from photos (\wcfile{Napier Nomad I East fortune front}{1} and \wcfile{Napier Nomad I East fortune Rear view}{2}) \ccbysa by \wcun{Nigel Ish}{Nigel~Ish}}
    \label{fig_napier_i_photos}
\end{figure}

\textit{Napier \& Son} rapidly corrected course: the second prototype of the engine, the \textit{\mbox{Nomad II}}, was greatly simplified. The intercooling, reheat, and centrifugal supercharger were all abandoned (\cref{fig_napier_ii_circuit}). The two large mechanical units, one with pistons and the other with a turbine, were now connected to the same propeller. Both elements were connected with an ingenious but complex continuously-variable mechanical and hydraulic reducer that allowed each unit to operate at its optimal speed.

\begin{figure}[ht]%handmade
	\begin{center}
		\includegraphics[width=12cm]{napier_nomad_ii}
	\end{center}
	\supercaption{Schematic diagram of the thermodynamic circuit of the \textit{Napier Nomad II}. A variable-ratio mechanical-hydraulic reducer connected the two units, which now drove the same propeller.}%
	{\wcfile{NomadSchematic 185kBpng360kB}{Diagram} by users $\cdot$ Commons \wcun{Tataroko-common}{Tataroko-common}, \wcun{Aaa3-other}{Aaa3-other} \& \wcun{Nimbus227}{Nimbus227} (\pd)}
	\label{fig_napier_ii_circuit}
\end{figure}

\index{turbo-compound}\index{Diesel turbo-compound}\index{maximum temperature of an engine}\index{engine!maximum temperature of an}
In an enlightening paper from 1954~\cite{sammonschatterton1955}, the engine designers showed a very clear vision and design approach. According to them, a simple turbocharged Diesel engine only benefits from turbocharging over a very narrow power range — outside of this range, the turbine power is either in surplus (and therefore lost) or insufficient to power the compressor. A different arrangement, in which the propeller would be driven solely by the turbine (with the Diesel engine then only providing supercharging and heat supply) would be far too inefficient at low power and unnecessarily strain the Diesel engine at high power. The simple turboprop, unable to reach the high pressures and temperatures of a Diesel engine, is too inefficient. Only in the chosen arrangement, called \textit{Diesel turbo-compound}, could the cylinder engine and the turboprop unit both contribute at all power levels, each always running at its optimal speed.

\index{efficiency of an engine}\index{engine!efficiency}\index{specific power}
The performance of the \textit{Nomad II} was indeed impressive — with its efficiency of \SI{40}{\percent}, it used a third less fuel than its contemporaries— but its commercial failure was brutal: the project was abandoned in 1955 without a single sale. The engine was terribly heavy (with over \SI{1600}{\kilogram} for \SI{2}{\mega\watt}, its power-to-weight ratio was three times lower than that of a turboprop), which erased a large part of the fuel savings it could have generated. Also, it was both too complex for a regional aircraft and far too slow for jet airliners, and aircraft manufacturers were never interested.

\index{ecological impact}\index{environmental impact}\index{turbocharging}
The curious arrangement conceived by \textit{Napier \& Son} fell into obscurity but, sixty years later, it made a thunderous comeback in racing cars. In 2014, the International Automobile Federation, organizer of the Formula One races, sought to make it easier for new teams to join the sport, by limiting development expenses, increasing technological spin-offs applicable to the industry, and finding itself a (newfound) ecological conscience. The regulations were thus modified: turbocharging would be allowed, but the cars' fuel consumption was limited to \SI[per-mode=symbol]{100}{\liter\per\hour}; most importantly, engine manufacturers would be allowed use the turbocharger to recover energy in the form of electricity, and conversely, accelerate the turbo by reinvesting this electrical energy into it (\cref{fig_formulaone_engine}). Thus, the engine efficiency (and therefore, given the regulatory consumption limit, its power) can be increased at all speeds without sacrificing responsiveness. The system is poetically named \textsc{mgu-h}, but one could say that it is the unexpected revenge of the Anglo-Saxon \textit{turbo-compounding}!

\begin{figure}[ht!]
	\begin{center}
		\includegraphics[width=\textwidth]{formulaone_engine}
	\end{center}
	\supercaption{Thermodynamic circuit of the air in a 2014 Formula 1 engine. The turbocompressor shaft (left) is not connected to the six-cylinder engine (center block) or the car's wheels. However, an electric motor/generator (named \textsc{mgu-h}) allows for extraction or addition of electrical energy. During high power phases, the turbine power (right) is in surplus and can be used to charge onboard batteries or drive the wheels with an electric motor. During low power phases, the turbine power is in deficit and the turbo can be driven by the generator to maintain the compression ratio and increase responsiveness.}%
	{\wcfile{Outline of a 2014 Formula One engine (hybrid MGU-H).svg}{Diagram} \ccbysa \olivier}
	\label{fig_formulaone_engine}
\end{figure}

\atendofhistorysection

