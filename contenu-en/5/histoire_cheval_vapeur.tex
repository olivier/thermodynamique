\atstartofhistorysection
\section[A Bit of History: the Horsepower]{A Bit of History:\\ the Horsepower}
\label{ch_cheval_vapeur}\index{horsepower (unit)}\index{vapors!industrial use}

We traditionally associate the word \emph{engine} with automobile propulsion: machines running on air and gasoline. However, the very first engines were quite different. Heavy, slow, incredibly large, running on coal and water, they were only used to pump water.

	 Let's go back to the beginning of the 19\textsuperscript{th} century. At that time, Europe was heated by coal, which was extracted with great difficulty from constantly flooded mines. The water was removed by working horses through a primitive pumping mechanism. The first engines were installed to replace these horses -- but they were hardly less expensive, and required just as much attention!

\begin{figure}[htp]%handmade
	\begin{center}
		\includegraphics[width=10cm]{Newcomen6325}
	\end{center}
	\supercaption{Cross-section diagram of one of the first steam engines (Newcomen engine, \texttildelow 1720). The condensation caused by water injection into the cylinder led to a drop in internal pressure.\index{Newcomen, engine}\index{engine!Newcomen}}{\wcfile{Newcomen6325}{Engraving} by Newton Henry Black \& Harvey Nathaniel Davis, published in 1913 (\pd)}
	\label{fig_newcomen}
\end{figure}

The level of development of metallurgy (cylinders were made of copper, by hand) and mechanical technology (valves had to be continuously opened and closed by hand, one by one, to let the engine operate) meant that these engines could only operate with very low pressures.

\index{atmosphere, pressure of the}\index{pressure!atmospheric}
For these engines, water is perfectly suitable. When steam at moderate pressure is cooled (for example by mixing it with cold liquid water), it condenses and its pressure drops abruptly (\cref{fig_newcomen}). This is an opportunity to drive a piston that, subjected to atmospheric pressure on its other side, can produce work. Thus, one could almost speak of "implosion engines", since they make the atmosphere work on a cylinder of depressurized steam to produce work.

With this operating mode, the pressure difference obtained reaches a maximum of \SI{1}{\bar}, and the pace is lamentably slow. But these machines operated at reasonable temperatures and pressures, and the operators lacked neither coal nor water.

\index{Watt!James}
It is a young employee of the University of Manchester who first realized the tremendous development potential of the steam engine. By studying a scale model of an engine owned by the university, he made a series of modifications that doubled its efficiency.

\begin{figure}
	\begin{center}
		\includegraphics[width=10cm]{steam_engine_boulton_watt}
	\end{center}
	\supercaption{The \textit{Boulton \& Watt} engine with separate condensation and double-acting piston.\index{engine!Boulton \& Watt}}{\wcfile{SteamEngine_Boulton\%26Watt_1784}{Engraving} by Robert Henry Thurston (1878, \pd)}
	\label{fig_boultonwattengine}
\end{figure}

The first and most important of these modifications was to separate in space the heating and cooling phases of the steam. Previously, condensation by injection of cold water also lowered the temperature of the metallic piston and cylinder, which had to be reheated at each cycle, with a significant heat cost. Now instead, the steam was cooled in a chamber maintained at low temperature by immersion in water (\cref{fig_boultonwattengine}), while the engine cylinder was kept at a high temperature above the boiler.

The second modification consisted of exploiting the two faces of the piston. By using a system of pipes controlled by valves, it became possible to increase the pressure difference driving the piston movement. While the steam condensed at \SI{0,2}{\bar} on one side, the other face now met steam pressurized at \SI{1.4}{\bar}. Not only was the work provided at each piston movement increased, but also the speed (and thus the power) was doubled, since the piston was providing work in both the upward and downward movements.

Finally, a series of mechanical devices reduced the attention that had to be paid to the formidable machinery thus assembled. A flywheel maintained speed, valve openings were mechanically linked to the engine's advancement, and the centrifugal ball governor, a technology imported from water mills, prevented engine runaway or stalling.

\begin{figure}
	\begin{center}
		\includegraphics[width=8cm]{centrifugal_governor}
	\end{center}
	\supercaption{The ball governor, a mechanism originating from windmills and integrated into steam engines by James Watt.}{\wcfile{Centrifugal_governor}{Engraving} by R. Routledge (1900, \pd)}
\end{figure}

\index{Boulton, Matthew}
The young laboratory assistant, who went by the name of \we{James Watt}, found fortune by partnering with a cannon manufacturer and expert coppersmith, Matthew \mbox{Boulton}. The rest is history: \textit{Boulton \& Watt} claimed a huge share of the emerging market for heat engines.

Their success, unfortunately, would come much less from the technological innovations they brought than from the high-profile lawsuits they led to monetize them. Indeed, the two partners excelled in political connections and were at home in the peculiar world of patents and the royalties that result from them. The two Scotsmen in top hats, for example, receive a percentage of the coal savings generated by the machines they sold across the country. And it would take nearly fifteen years before the legal possibility, in the United Kingdom, to use the "expansive power of steam" became finally open to everyone, a process deviously patented by the two partners!

\index{horsepower (unit)}\index{Watt!unit}
Regardless, the \textit{General Conference on Weights and Measures} assigned the unit \si{watt} to power in the \textsc{si} system in 1960. It then dethroned the \emph{horsepower} (\textit{hp})... a unit introduced by the very James Watt nearly a century earlier, while he was comparing his machines with the draft horses they were to replace.
	\begin{IEEEeqnarray*}{rCl}
		\SI{1}{\cheval}_\text{imperial} 	& \equiv & \SI{33 000}{\foot\lbf\per\minute}\\
													& = & \SI{745.6999}{\watt}
	\end{IEEEeqnarray*}

\atendofhistorysection

