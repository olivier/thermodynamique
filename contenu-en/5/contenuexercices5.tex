\begin{boiboiboite}
	\propwater
\end{boiboiboite}

\subsubsection{Temperature and boiling pressure}
\label{exo_temperature_pression_ebullition}

	A student is traveling on a commercial flight and is served a hot drink by the crew (\cref{fig_cafe}); the drink is almost boiling. He/she measures the temperature to be~\SI{88.2}{\degreeCelsius}.

	\begin{enumerate}
		\item What is the pressure in the cabin?
	\end{enumerate}

	The aircraft undergoes rapid depressurization and the cabin pressure equalizes with the local atmospheric pressure (\SI{17.2}{\kilo\pascal}). The student puts on their oxygen mask and unpleasantly notices that the drink, which is cooling down, has started to boil.

	\begin{enumerate}
	\shift{1}
		\item At what temperature will the boiling stop?
	\end{enumerate}

	\begin{figure}[htp]%handmade
		\begin{center}
			\includegraphics[width=5cm]{exercice_cafe}
		\end{center}
		\supercaption{An aerial hot drink with an unidentified taste}{\flickrfile{notbrucelee/7942977960}{Photo} \ccby by \flickruser{notbrucelee}}
		\label{fig_cafe}
	\end{figure}
\exerciseend

\subsubsection{Evaporation of Water}
\label{exo_evaporation_eau}

	\begin{enumerate}
		\item How much heat is needed to completely evaporate a pot of water (\cref{fig_casserole_eau})? The container contains \SI{2.5}{\liter} of water at~\SI{10}{\degreeCelsius}, and the ambient atmospheric pressure is~\SI{1}{\bar}.
		\item Represent the evolution on a temperature-volume diagram, qualitatively (that is, without representing numerical values) and showing the saturation curve.
		\item The water is heated with an electric heating plate of \SI{1500}{\watt}. How long does it take to vaporize the water, and what is the cost incurred by the experiment? The operator charges \num{0.15}\euro{} per \si{\kilo\watt\hour} and the losses from the plate to the room are around~\SI{10}{\percent}.
	\end{enumerate}

	\begin{figure}[htp]%handmade
		\begin{center}
			\includegraphics[width=6cm]{exercice_boiling}
		\end{center}
		\supercaption{An ordinary physics experiment}{\flickrfile{indi/2391675917}{Photo} \ccby by \flickrname{indi}{Indi Samarajiva}}
		\label{fig_casserole_eau}
	\end{figure}
\exerciseend

\subsubsection{Simple Recap Exercise}
\label{exo_cours_vapeur_isotherme}
% This exercise is related to exercise 8.5 (\ref{exo_diagrammes_ts})

	Describe very briefly an experiment where a fixed mass of subcooled liquid water is heated at constant temperature. Represent the evolution on a pressure-volume diagram, qualitatively, showing the saturation curve.
\exerciseend

\subsubsection{High-Pressure Steam Generation}
\label{exo_generation_vapeur}
% This exercise is related to exercise 8.5 (\ref{exo_diagrammes_ts})

	An industrial chemical process requires a steam flow rate of~\SI{2}{\kilogram\per\second} at~\SI{6}{\bar} and~\SI{875}{\degreeCelsius}. The machine responsible for providing this steam is fed by a pressurized liquid water pipeline at~\SI{10}{\degreeCelsius} and~\SI{6}{\bar}.

	\begin{enumerate}
		\item What power in the form of work and heat is required to generate this steam flow?
		\item Represent the evolution undergone by the water on a pressure-volume diagram, qualitatively, showing the saturation curve.
	\end{enumerate}
\exerciseend

\subsubsection{Everything Depends On the Valve}
\label{exo_cocotteminute}

	A student decides to maintain a balanced diet, and to do so, cooks food in a pressure cooker (\cref{fig_cocotte}).

	The valve of the pressure cooker is a small metal cylinder that is partly hollowed out, and sits freely on top of a small vertical exhaust pipe on the lid. The valve weighs \SI{216}{\gram}; it is placed on an exhaust pipe with a diameter of \SI{5}{\milli\metre}. The ambient atmospheric pressure is~\SI{1.1}{\bar}.

	\begin{enumerate}
		\item At which temperature does the pressure cooker allow the student to food their cook?
		\item What temperature and pressure could a person generate inside the pressure cooker by pressing on the valve? How could an accident be then prevented?
	\end{enumerate}

	\begin{figure}[htp]%handmade
		\begin{center}
			\includegraphics[width=8cm, max width=0.65\columnwidth]{exercice_super_cocotte}
		\end{center}
		\supercaption{Pressure cooker, commonly known as a \textit{«~cocotte minute~»} in France.}{\wcfile{Super_Cocotte_decor_SEB-MGR_Lyon-IMG_9918}{Photo} \ccbysa by \wcu{rama}}
		\label{fig_cocotte}
	\end{figure}
\exerciseend

\subsubsection{A First Steam Engine}
\label{exo_moteur_basique_vapeur}

	An engineer conducts an experiment with water vapor, aiming to develop a very simple small engine (\cref{fig_moteurvapeursimple}).

	They put \SI{2}{\liter} of liquid water at~\SI{20}{\degreeCelsius} into a large cylinder. The water is compressed to~\SI{2}{\bar} by a piston.

	They then heat the water, and the piston moves while maintaining constant pressure, until the volume reaches~\SI{300}{\liter}.

	\begin{enumerate}
		\item Represent the evolution on a pressure-volume diagram, qualitatively, showing the saturation curve.
		\item What was the amount of work done?
		\item How much heat had to be supplied?
		\item What would be the transfers of work and heat if the expansion were continued until~\SI{4500}{\liter}?
	\end{enumerate}

	\begin{figure}[htp]%handmade
		\begin{center}
			\includegraphics[height=6cm]{exercice_moteur_simple}
		\end{center}
		\supercaption{A very basic concept of a steam engine}{\wcfile{Conceptual drawing for a simple steam engine.svg}{Sketch} \cczero by \olivier}
		\label{fig_moteurvapeursimple}
	\end{figure}
\exerciseend

\subsubsection{Pumping Water}
\label{exo_pompage_baliani}

	A liquid pump is installed to draw water at~\SI{5}{\degreeCelsius} located in a lower reservoir (\cref{fig_pompage}).

	\begin{enumerate}
		\item Up to what height $\Delta z$ can the pumping be done?
		\item How could we change the setup to pump water to a greater height?
	\end{enumerate}

	\begin{figure}[htp]%handmade
		\begin{center}
			\includegraphics[height=7cm]{exercice_pompe_eau}
		\end{center}
		\supercaption{Water pumping from a reservoir located below. The first observation of the calculated height limit in this exercise was made in 1630 by \wf{Giovanni Battista Baliani}\index{Baliani, Giovanni Battista}.}{\wcfile{Water pump maximum height.svg}{Sketch} \cczero by \oc}
		\label{fig_pompage}
	\end{figure}
\exerciseend

\subsubsection{Steam Turbine On a Light Installation}
\label{exo_turbine_vapeur_legere}

	A company is developing a small steam power plant that can be carried in a standard-sized container. Once connected to an external boiler, it is capable of converting heat from unrefined fuels (such as wood, paper, or coal) into electricity with significant efficiency.

	Within this power plant, the turbine is adiabatic and receives \SI{5}{\tonne\per\hour} of steam at~\SI{90}{\bar} and~\SI{510}{\degreeCelsius} from the boiler. The outlet pressure is (barely above) atmospheric pressure (we will take \SI{1}{\bar}). An engineer predicts (as we will also be able to do after \chaptereight) that the specific internal energy of the steam will then be~\SI{2676.6}{\kilo\joule\per\kilogram}.

	The turbine is mechanically connected to a power generator with an efficiency of~\SI{85}{\percent}.

	\begin{enumerate}
		\item What is the electrical power output of the generator?
	\end{enumerate}

	At the other end of the container, an electric pump (the only other mechanical element in the plant) collects the condensed water in saturated liquid state (\SI{1}{\bar}) and increases its pressure back to~\SI{90}{\bar} to feed the boiler. It is assumed that during pumping, the specific volume of water varies negligibly, and that the compression is reversible.

	\begin{enumerate}
	\shift{1}
		\item What electric power is required to supply the pump?
	\end{enumerate}
\exerciseend

\subsubsection{The Crushed Barrel}
\label{exo_baril}

	In order to perform a physics demonstration, a group of students bring water to boil at ambient pressure in an old oil barrel (capacity \SI{208}{\liter}, height \SI{88}{\centi\meter}).

The barrel is removed from the heat source and hermetically sealed. The purpose of the operation is to observe the barrel being crushed by the atmosphere due to the change in state of the water it contains.

	\begin{enumerate}
		\item Which pressure can be generated inside the barrel by letting it cool down?
		\item What would be the vertical force exerted on the upper wall of the barrel at that point?
	\end{enumerate}

	\textit{Some more challenging questions:}

	\begin{enumerate}
	\shift{2}
		\item There are \SI{5}{\liter} of liquid left at the bottom of the barrel when the lid is closed. What is the steam quality?
		\item How much steam has condensed during cooling?
		\item How much heat had to be removed in order to reach the final pressure?
	\end{enumerate}
\exerciseend

\subsubsection{Newcomen Engine}
\label{exo_moteur_newcomen}\index{engine!Newcomen}\index{Newcomen, engine}

	In their time, around 1720, Newcomen engines (\cref{fig_newcomen}) were at the forefront of technology. Slightly overheated steam (\SI{1}{\bar}, \SI{250}{\degreeCelsius}) was injected into a large cylinder (height \SI{1}{\meter}, diameter \SI{1.5}{\meter}).

	Then this steam was cooled (by allowing a small amount of liquid water at atmospheric pressure and temperature to enter), maintaining the internal pressure at~\SI{0.1}{\bar}. The piston would then descend, providing work.

	The water available to the engine was at~\SI{1}{\bar}, \SI{10}{\degreeCelsius}.

	\begin{enumerate}
		\item Plot the evolution of water on a pressure-volume or temperature-volume diagram, showing the saturation curve.
		\item Before being able to perform the descent, how much heat must be supplied to fill the cylinder with steam?
		\item How much work is produced by the engine during the piston's descent?\\
		\textit{Hint: Consider the work done by the atmosphere on the outer face of the piston.}
		\item What is the efficiency of the engine, if friction and all other heat losses are neglected?
	\end{enumerate}

\begin{figure}[ht!]
	\begin{center}
		\includegraphics[width=9cm]{moteur_newcomen_2}
	\end{center}
	\supercaption{The ingenious atmospheric engine by Newcomen, the first true success in steam power.}{\wcfile{GesNat662}{Engraving} C. L. Moll -- \textit{Die gesammten Naturwissenschaften} (1873, \pd)}
	\label{fig_moteur_newcomen}
\end{figure}
\exerciseend

\subsubsection{Condenser of a Steam Power Plant}
\label{exo_condenseur_riviere}\index{condenser}

In a high-power electric power plant, the condenser is responsible for recovering water at the output of the turbines and removing energy from it so that it can return to the liquid state and re-enter the pumps $\to$ boilers $\to$ turbines circuit. The water from the system (\SI{180}{\tonne\per\hour}) arrives at~\SI{0.5}{\bar} with a specific volume of~\SI{3.1247}{\metre\cubed\per\kilogram}; it must leave at the same pressure, in a saturated liquid state.

To extract heat from the water in the power plant, the condensers use a secondary water circuit directly from a river. Water is taken at~\SI{10}{\degreeCelsius}.

In order to reduce the ecological impact of the power plant, the goal is to discharge the secondary water into the river at a temperature no higher than~\SI{35}{\degreeCelsius}.

	\begin{enumerate}
		\item What flow rate of secondary water should be drawn from the river?
		\item In order to limit heat discharge into the river, where (and how) is the condenser heat also discharged in practice?
	\end{enumerate}
\exerciseend

\subsubsection{Aircraft Catapult On an Aircraft Carrier}
\label{exo_catapulte}

An aircraft catapult is mounted on a military ship (figures \ref{fig_exo_catapulte_1} and \ref{fig_exo_catapulte_2}). It consists of a steam reservoir connected to a long cylinder, in which a piston slides to propel the aircraft during takeoff.

At the beginning of the catapult launch, the steam is at~\SI{140}{\bar} and~\SI{700}{\degreeCelsius}. After a brief run of~\SI{50}{\meter}, the aircraft has left the deck and the steam is at~\SI{4}{\bar} and~\SI{410}{\degreeCelsius}.

	\begin{enumerate}
		\item How much energy did the catapult provide to the aircraft per kilogram of steam?
		\item What must be the diameter of the piston and the total mass of steam, for the thrust provided to the aircraft to always exceed~\SI{2.5}{\tonne}?
	\end{enumerate}

\textit{And a question which we can not yet answer:} What is the \emph{maximum} amount of energy that the catapult could have provided to the aircraft by allowing the steam to expand?

\begin{figure}[ht!]
	\begin{center}
		\vspace{-0.3cm}%handmade
		\includegraphics[width=0.53\columnwidth]{catapulte_vapeur_1}
		\vspace{-0.3cm}%handmade
	\end{center}
	\supercaption{Cylinder of a steam catapult from the \textit{USS Abraham Lincoln}}{\vspace{-0.2cm}\wcfile{Catapulte CVN72 071117-N-9898L-006}{Photo} by Geoffrey Lewis, U.S. Navy (\pd)}%handmade
	\label{fig_exo_catapulte_1}
\end{figure}
\begin{figure}[ht!]
	\begin{center}
		\vspace{-0.4cm}%handmade
		\includegraphics[width=\columnwidth]{catapulte_vapeur_2}
		\\\vspace{-0.3cm}%handmade
	\end{center}
	\supercaption{Piston of a steam catapult from the aircraft carrier \textit{Charles de Gaulle}.}{\vspace{-0.2cm}\wcfile{French aircraft carrier Charles de Gaulle - catapult maintenance 2008}{Photo} \ccbysa by Jean-Michel Roche, Netmarine.net}%handmade
	\label{fig_exo_catapulte_2}\vspace{-0.3cm}%handmade
\end{figure}
\exerciseend

\subsubsection{Turbine of a Nuclear Power Plant}
\label{exo_turbine_balakovo}

In a nuclear power plant, the electricity generator is driven by a steam turbine (\cref{fig_centrale_balakovo}).\\
Most of the steam (heated by the nuclear reactor) passes through the entire turbine. However, in the middle of the turbine, a steam extraction is performed. It allows, on one hand, to heat the water in another part of the circuit (\S\ref{ch_regeneration}), and on the other hand, to precisely control the mass flow rate in circulation. The total flow rate at the inlet is~\SI{317}{\tonne\per\hour} of steam.

We measure the following steam properties:

\begin{description}
\TabPositions{0.3\columnwidth}%handmade, to make the data fit
	\item Inlet: 		\tab\SI{120}{\bar}; 	\SI{565}{\degreeCelsius}
	\item Extraction: 	\tab\SI{10}{\bar}; 	\SI{250}{\degreeCelsius};	\SI{1.2}{\kilogram\per\second}
	\item Outlet: 		\tab\SI{1}{\bar};		\SI{115}{\degreeCelsius}
\end{description}

What is the mechanical power provided by the turbine?

\begin{figure}[ht!]
	\begin{center}
		\includegraphics[width=\columnwidth]{exercice_turbine_centrale2}
		\includegraphics[width=\columnwidth]{exercice_turbine_centrale1}
	\end{center}
	\supercaption{One of the turbines at the Balakovo Russian nuclear power plant (approx.~\SI{1}{\giga\watt} plant power), in maintenance (top) and during installation (bottom).}{\wcfile{BalakovoNPP_turb.JPG}{Photos 1} and \wcfile{BalNPP_m_st2}{2} \ccbysa The Centre of the Public Information Balakovo NPP}
	\label{fig_centrale_balakovo}
\end{figure}
\exerciseend

\exercisesolutionpage
\titreresultats

\begin{description}[itemsep=2em]
	\item [\ref{exo_temperature_pression_ebullition}]
			\tab 1) Interpolating between $T_\text{sat.} = \SI{85}{\degreeCelsius}$ and $T_\text{sat.} = \SI{90}{\degreeCelsius}$ in Table \#2, we obtain $p_\text{sat.} = \SI{0,67}{\bar}$
			\tab 2) Interpolating between $p_\text{sat.} = \SI{0,016}{\mega\pascal}$ and $p_\text{sat.} = \SI{0,018}{\mega\pascal}$ in Table \#3, we obtain $T_\text{sat.} = \SI{56,8}{\degreeCelsius}$ (yuck!)
	\item [\ref{exo_evaporation_eau}]
			\includegraphics[width=\solutiondiagramwidth]{exo_sol_tv_ebullition}
			\tab 1) To heat up the water until the boiling point (B) then complete evaporation (C): $Q_{\A \to \C} = m (q_\fromatob + q_\frombtoc) = \frac{V_\A}{v_\A}(h_{L \SI{0,1}{\mega\pascal}} - h_\A + h_{LV \SI{0,1}{\mega\pascal}}) = \SI{+6582}{\kilo\joule}$.
			\tab 3) $\Delta t = \frac{Q_{\A \to \C}}{\dot Q_\text{moyenne}} = \SI{1}{\hour} ~\SI{21}{\minute}$ with a cost of \num{0,29}\euro{}.
	\item [\ref{exo_cours_vapeur_isotherme}]
			\includegraphics[width=\solutiondiagramwidth]{exo_sol_pv_generation_vapeur}
			\tab see \chaprefp{ch_lv_bain_marie} \& \cref{fig_p-v_eau}.
	\item [\ref{exo_generation_vapeur}]
			\tab 1) Reading in Table \#1 @ $p_\A = \SI{0,6}{\mega\pascal}$, $h_\A = \SI{42,6}{\kilo\joule\per\kilogram}$. Interpolating between \num{800} and~\SI{900}{\degreeCelsius} in this table, we obtain $h_\B = \SI{4336,6}{\kilo\joule\per\kilogram}$. Thus with equation~\ref{eq_lv_chaleur_isobare}, $\dot{Q}_\fromatob = \dot m \Delta h = \SI{+8,59}{\mega\watt}$. $\dot{W}\fromatob = \SI{0}{\watt}$ (\ref{eq_lv_travail_isobare}).
	\item [\ref{exo_cocotteminute}]
			\tab 1) $p_\text{inner} = p_\text{valve} + p_\text{atm.} = \frac{F_\text{weight}}{S_\text{outlet}} + \SI{1}{\bar} = \SI{2,0797}{\bar}$. Interpolating in Table \#2, $T_{\text{sat.} p=\SI{2,0797}{\bar}} = \SI{121,37}{\degreeCelsius}$.
	\item [\ref{exo_moteur_basique_vapeur}]
			\includegraphics[width=\solutiondiagramwidth]{exo_sol_tv_moteur_basique_1}
			\includegraphics[width=\solutiondiagramwidth]{exo_sol_pv_moteur_basique_2}
			\tab 2) $W_\fromatob \approx 0$ (\ref{eq_lv_sf_travail_isochore}); with $m = \frac{V_\B}{v_\B}$ and $v_\D = \frac{V_\D}{m}$, we calculate $W_{\B \to \D} = - m p_\text{const} (v_\D - v_\B) = \SI{-59,6}{\kilo\joule}$ (\ref{eq_lv_sf_travail_isobare}).
			\tab 3) With $v_\D$ we calculate the quality $x_\D \approx \frac{v_\D}{v_{V \SI{0,2}{\mega\pascal}}} = \num{0,1697}$.
				Thus, ${Q}_{\B \to \D} = m (h_\D - h_\B) = m (h_L + x_\D h_{LV} - h_\B) = \SI{+1585,2}{\kilo\joule}$ (\ref{eq_lv_sf_chaleur_isobare}), so twenty-five times more…
			\tab 4) The relationships are identical and yield ${W}_{\B \to \E} = \SI{-899,6}{\kilo\joule}$ and ${Q}_{\B \to \E} = \SI{+7694,3}{\kilo\joule}$ (the efficiency jumps from\num{3,8} to~\SI{11,7}{\percent}… there is a lead to follow here…)
	\item [\ref{exo_pompage_baliani}]
			\tab 1) Hydrostatic pressure in the pipe depends on height ($\Delta p = \rho g \Delta z$). When the pressure at the pump goes below $p_\text{sat.}$, the water starts boiling. For $\Delta p_\text{boiling} = \SI{9,9127e4}{\pascal}$, $\Delta z_\text{boiling} = \SI{10,1}{\metre}$.
			\tab 2) One can do better than heating up the tank… 
	\item [\ref{exo_turbine_vapeur_legere}]
			\tab 1) In A in Table \#1 we interpolate at~\SI{9}{\mega\pascal} between \num{500} and~\SI{600}{\degreeCelsius} to obtain $h_{\SI{510}{\degreeCelsius} \& \SI{90}{\bar}} = \SI{3412}{\kilo\joule\per\kilogram}$.
				The same is done in B where $u_\B > u_{V \SI{1}{\bar}}$ ($h_\B = \SI{2899,4}{\kilo\joule\per\kilogram}$).
				Finally $\dot{W}_\text{electrical} = \eta_\text{conversion} \dot m \Delta h = \SI{-605,8}{\kilo\watt}$.
			\tab 2) $\dot{W}_\text{pump} = \dot m \int v \diff p \approx \dot m v_L \Delta p = \SI{+12,9}{\kilo\watt}$.
	\item [\ref{exo_baril}]
			\tab 1) If we attain $T_\B = \SI{30}{\degreeCelsius}$ with constant volume, then $p_\text{inside min.} = p_{\text{sat.} \SI{30}{\degreeCelsius}} = \SI{0,004247}{\mega\pascal}$. In that case, $\Delta p_\text{max} = \SI{-9,575e4}{\pascal}$ ;
					\tab 2) $F_\text{max} = \Delta p_\text{max} S_\text{lid} = \SI{-22,6}{\kilo\newton}$ (the barrel will of course be crushed before that)
					\tab 3) $x_\A = \num{0,02439}$, $x_\B = \num{1,288e-3}$ ;
					\tab 4) $m_\text{condensed} = \SI{0,11352}{\kilogram}$ ;
					\tab 5) ${Q}_\fromatob = \SI{-1,669}{\mega\joule}$.
	\item [\ref{exo_moteur_newcomen}]
			\includegraphics[width=\solutiondiagramwidth]{exo_sol_tv_newcomen}
			\includegraphics[width=\solutiondiagramwidth]{exo_sol_pv_newcomen}
			\tab 2) Reading Table \#1 gives us $h_\A = \SI{42,1}{\kilo\joule\per\kilogram}$. Through interpolation we obtain $h_\B = \SI{2975}{\kilo\joule\per\kilogram}$ and $m = \frac{V_\B}{v_\B} = \SI{0,7354}{\kilogram}$. Thus we calculate ${Q}_\fromatob = m \Delta h = \SI{+2154}{\kilo\joule}$ (\ref{eq_lv_sf_chaleur_isobare}).
			\tab 3) While calculating $m$ we already obtained $v_\B = \SI{2,406}{\metre\cubed\per\kilogram}$. Thus ${W}_{\text{piston} \to \text{shaft}} = W_{\text{atm.} \to \text{piston}} + W_{\text{piston} \to \text{steam}} = m (p_\C - p_\text{ext.}) (v_\D - v_\C) = \SI{-159}{\kilo\joule}$.
			\tab 4) $\eta \equiv \frac{Q_\text{fournie}}{W_\text{utile}} = \SI{7,38}{\percent}$ (realistic value).
	\item [\ref{exo_condenseur_riviere}]
			\tab 1) $x_\A = \SI{96,44}{\percent}$ \& $x_\B = 0$ ; ainsi $\dot Q_\fromatob = \SI{-111,13}{\mega\watt}$, ce pourquoi il nous faut $\dot{m}_\text{secondaire} \geq \SI{1062,4}{\kilogram\per\second}$.
			\tab 2) Into the atmosphere, through the secondary water which is released through the large towers…
	\item [\ref{exo_catapulte}]
			\tab 1) $\Delta u = \SI{-433,1}{\kilo\joule\per\kilogram}$ ($=w_\fromatob$ if we suppose that the transformation is adiabatic) ;
			\tab 2) $D_\text{min.} = \SI{32,26}{\centi\metre}$ (care must be taken to take atmospheric pressure into account) ; $m = \frac{V_\text{max} - V_\text{min}}{v_\text{max} - v_\text{min}} = \SI{5,243}{\kilogram}$.
			\tab Nota: data in this exercise is purely hypothetical, for lack of reliable published data.
	\item [\ref{exo_turbine_balakovo}] \tab $\dot{W}_\text{turbine} = \SI{-71}{\mega\watt}$.
\end{description}

