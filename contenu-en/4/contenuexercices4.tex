\begin{boiboiboite}
	\propair
	\isentropics
	\isothermals
\end{boiboiboite}

\subsubsection{Air Pressure}
\label{exo_pression_air}

A mass of~\SI{5}{\kilogram} of air is enclosed in a tank of~\SI{2}{\meter\cubed}.

	\begin{enumerate}
		\item What are its specific volume and density?
		\item What is the pressure if the temperature is~\SI{20}{\degreeCelsius}?
	\end{enumerate}
\exerciseend
	
\subsubsection{Heating of an Air Tank}
\label{exo_rechauffement_reservoir_air}

	A compressed air tank made out of sealed concrete has a fixed volume of~\SI{1.2}{\metre\cubed}. Air is stored in it at a pressure of~\SI{2}{\bar}. \\
	The tank is placed in the sun and solar heating raises the temperature from~\SI{5}{\degreeCelsius} to~\SI{60}{\degreeCelsius}.
	
	\begin{enumerate}
		\item What are the mass, specific volume, density, and pressure inside the tank, before and after heating?
	\end{enumerate}

	When the temperature reaches~\SI{60}{\degreeCelsius}, a valve opens and lets air escape to bring the pressure in the tank back down to the initial pressure of~\SI{2}{\bar}. During the release, the temperature of the air inside the tank remains constant.

	\begin{enumerate}
		\shift{1}
		\item How much air mass should be allowed to escape?
	\end{enumerate}

	When the pressure has reached~\SI{2}{\bar}, the valve closes and the tank, once again sealed, cools slowly at constant volume. The final temperature returns to~\SI{5}{\degreeCelsius}.
	
	\begin{enumerate}
		\shift{2}
		\item What is the final pressure in the tank?
	\end{enumerate}
\exerciseend

\subsubsection{Energy and Temperature}
\label{exo_gp_energie_temperature}

	There is air in a flexible compartment at a pressure of~\SI{3}{\bar}. Its internal energy is~\SI{836}{\kilo\joule\per\kilogram}.
	
	It is heated at constant pressure until~\SI{900}{\degreeCelsius}; then it is cooled and expanded while its properties vary according to the relation $p v^{\num{1.1}} = \text{const.}$ until its temperature reaches~\SI{25}{\degreeCelsius}.

	\textit{[Trick question]} How much energy has it received or lost since the beginning of the evolution?
\exerciseend

\subsubsection{Power of an Air Pump}
\label{exo_puissance_pompe_air}

	An air pump (\cref{fig_exo_compresseur_air}) compresses air in a continuous regime, adiabatically. The air temperature increases from~\SI{15}{\degreeCelsius} to~\SI{100}{\degreeCelsius}.
	
	What is the specific power consumed?

\begin{figure}[!bh]
	\begin{center}
		\includegraphics[width=0.6\columnwidth]{compresseur_air_reservoir}
	\end{center}
	\supercaption{A small electric compressor mounted on a portable air tank}{\wcfile{Kolbenkompressor}{Photo} by \wcu{Grikalmis} (retouched,\\ \pd)}
	\label{fig_exo_compresseur_air}
\end{figure}
\exerciseend


\par~\clearfloats\dontbreakpage%handmade
\subsubsection{Turbine of a Turbojet Engine}
\label{exo_gp_turbine_turboreacteur}
	
	A student disassembles the \textit{\wf{Turbomeca Marboré}} turbojet engine from a \wfd{Fouga CM-170 Magister}{Fouga Magister} in order to study and modify its operation. S/he operates the engine on a test bench.
	
	At the inlet of the turbine, the conditions are measured at~\SI{110}{\meter\per\second} and~\SI{1000}{\degreeCelsius}.\\
	At the outlet of the turbine, these properties are measured at~\SI{125}{\meter\per\second} and~\SI{650}{\degreeCelsius}.
	
	The student also measures the heat losses from the turbine as \SI{75}{\kilo\joule\per\kilogram}.
	
	\begin{enumerate}
		\item What is the specific mechanical power developed by the turbine?
		\item What condition must the student maintain to obtain a power of~\SI{1}{\mega\watt}?
	\end{enumerate}
\exerciseend

\subsubsection{Elementary Evolutions: Isothermal Compression}
\label{exo_gp_isotherme}
% This exercise is related to exercise 8.5 (\ref{exo_diagrammes_ts})

A mass of~\SI{3.5}{\kilogram} of air is compressed reversibly in an isothermal manner (constant temperature) from~\SI{2}{\bar} and~\SI{15}{\degreeCelsius} to~\SI{45}{\bar}.
	
\begin{enumerate}
	\item Represent the evolution on a pressure-volume diagram, qualitatively (i.e.\ without numerical values).
	\item What are the amounts of work and heat involved?
	\item If the compression were carried out in a reversible adiabatic manner, would the final volume be different?
\end{enumerate}
\exerciseend

\subsubsection{Elementary Evolutions: Isobaric and Isochoric Coolings}
\label{exo_gp_isobare_isochore}
% This exercise is related to exercise 8.5 (\ref{exo_diagrammes_ts})

	A mass of~\SI{2}{\kilogram} of air in a deformable reservoir is at a pressure of~\SI{4.5}{\bar} and occupies a volume of~\SI{800}{\liter}. It is cooled, and the reservoir maintains constant pressure until the volume has been reduced by~\SI{40}{\percent}.
	
	After this, the cooling is continued at constant volume until the temperature reaches~\SI{25}{\degreeCelsius}.
	
	\begin{enumerate}
		\item Trace the evolution on a pressure-volume diagram.
		\item What is the work done by the air?
		\item What is the heat cost for the entire evolution?
	\end{enumerate}
\exerciseend

\subsubsection{Elementary Evolutions: Isentropic Compression}
\label{exo_gp_isentropique}
	
	What is the minimum amount of work required to compress~\SI{5}{\kilogram} of air at~\SI{1}{\bar} and~\SI{20}{\degreeCelsius} to \SI{50}{\bar} without heat transfer? Trace the evolution followed by the air on a pressure-volume diagram.
\exerciseend

\subsubsection{Elementary Evolutions: Vocabulary}
\label{exo_bete_mechant}

A fixed mass of perfect gas, with the sole purpose of exasperating a student in thermodynamics, slowly undergoes the following evolutions:

		\begin{center}
			\includegraphics[width=\textwidth]{exo_elementaires}
		\end{center}

	Among the evolutions above, which ones are:
	\begin{enumerate}
		\item at constant temperature (isothermal)?
		\item at constant volume (isochoric)?
	\end{enumerate}
\exerciseend

\subsubsection{Elementary Transformations: Pressure and Volume}
\label{exo_retrouver_pv}

Among the reversible evolutions described on each of the graphs in \cref{fig_pvel}, identify (without having to justify) the evolution at constant temperature, at constant pressure, reversible adiabatic, and at constant volume.
	
\begin{figure}[!bh]
	\begin{center}
		\includegraphics[width=0.4\textwidth, max width=0.6\columnwidth]{exo_pv_elementaires1}
		\includegraphics[width=0.4\textwidth, max width=0.6\columnwidth]{exo_pv_elementaires2}
	\end{center}
	\supercaption{Elementary transformations of a perfect gas}{\wcfile{P-v diagrams elementary evolutions.svg}{Scheme} \cczero \oc}
	\label{fig_pvel}
\end{figure}
\exerciseend

\subsubsection{Turbojet Compressor}
\label{exo_compresseur_turboreacteur}

Inside one of the engines of a commercial aircraft, the compressor (\cref{fig_exo_entree_genx2b}) is almost adiabatic.

\begin{figure}
	\begin{center}
		\includegraphics[width=0.8\textwidth]{entree_genx2b}
	\end{center}
	\supercaption{Air intake of one of the four General Electric \textsc{GEnx}-2B turbojets equipping a Boeing 747-8. The two-color fan blades are visible in the foreground; behind, the air flow is divided between the compressor inlet (internal) and the cold flow rectifier stators (external).}{\wcfile{Fan blades and inlet guide vanes of GEnx-2B (2)}{Photo} \ccbysa \olivier}
	\label{fig_exo_entree_genx2b}
\end{figure}

	
	\begin{enumerate}
		\item Starting from the following relation,
			\begin{equation}
				\left( \frac{p_1}{p_2} \right)	= \left( \frac{v_2}{v_1} \right)^{\gamma} \tag{\ref{eq_isentropique_horrible3}}
			\end{equation}
			valid for a reversible adiabatic evolution of a perfect gas, show (without using equation~\ref{eq_isentropique_horrible1}) that:
			\begin{equation}
				\left( \frac{T_1}{T_2} \right)	=  \left( \frac{p_1}{p_2} \right)^{\frac{\gamma -1}{\gamma}}  \tag{\ref{eq_isentropique_horrible2}}
			\end{equation}
		\item What is the minimum theoretical power to be supplied to the compressor?
		\item Under what conditions would this power be obtained?
	\end{enumerate}
	
	In reality, the compressor requires significantly more power to operate. We model the actual evolution inside the compressor with two distinct phases:

	\begin{itemize}
		\item A heating at constant pressure, conducted by friction, with power representing~\SI{15}{\percent} of the theoretical mechanical power calculated earlier;
		\item Then, an ideal compression up to~\SI{8}{\bar}.
	\end{itemize}
	
	\begin{enumerate}
		\shift{3}
		\item Compare the theoretical compression from question~2 and this new evolution on a pressure-volume diagram. Graphically represent the work consumed on one of the evolutions.
		\item What is the power consumed by the compressor in this new scenario?
	\end{enumerate}
\exerciseend

\clearfloats\dontbreakpage%handmade
\subsubsection{Compression and Combustion in a Diesel Engine}
\label{exo_compression_combustion_diesel}
\index{Diesel engine}\index{Diesel}\index{Rudolf}

	In 1890, a young German engineer passionate about thermodynamics (\S\ref{ch_histoire_diesel}) developed a low-power, low-speed, and high-efficiency engine in a laboratory (\cref{fig_exo_moteur_diesel}). The engine was designed to be robust and simple; it had only one cylinder. Here, we study part of its operating cycle. %why not more?
	
	\begin{figure}
		\begin{center}
			\includegraphics[width=0.5\textwidth]{Dieselmotor_1898_retouched}
		\end{center}
		\supercaption{Diesel Engine from 1898, manufactured under license by Sulzer in Switzerland}{\wcfile{Dieselmotor 1898 (retouched)}{Photo} \ccbysa Sulzer AG}
		\label{fig_exo_moteur_diesel}
	\end{figure}
	
	The piston inside the cylinder periodically varies the volume between~\SI{3}{\liter} (\vocab[bottom dead center \& top dead center]{bottom dead center}, piston at the bottom of its stroke) and~\SI{0.3}{\liter} (\vocab[]{top dead center}, piston at the top of its stroke).
	
	The engine starts its cycle at bottom dead center, when it is filled with air at~\SI{20}{\degreeCelsius} and~\SI{1}{\bar}. The piston compresses this air to top dead center.
	
	The compression is done reversibly (very slowly), but non-adiabatically: the air receives heat through the walls throughout the evolution. The engineer predicted that its properties would vary according to the relation $p \ v^{1.5} = \text{constant}$.

	\begin{enumerate}
		\item The work done by a force $\vec F$ on a displacement $\vec l$ is expressed as
			\begin{equation}
				W \equiv \vec F \cdot \vec l 	\tag{\ref{eq_travail_fdl}}
			\end{equation}
			From this equation, express the work done on a fixed mass body in terms of its specific volume and internal pressure.
		\item How much energy in the form of work will the gas compression have cost?
		\item How much energy in the form of heat will the gas have received during compression?
	\end{enumerate}

	When the piston reaches the top of its stroke, fuel is progressively injected into the cylinder to allow for combustion to occur. The amount of injected fuel provides a total heat input of~\SI{2}{\kilo\joule}. The combustion takes place at constant pressure.
	
	\begin{enumerate}
		\shift{3}
		\item Represent the evolution followed by the gas during compression and combustion on a pressure-volume diagram, qualitatively (i.e.\ without numerical values).
		\item What will be the maximum temperature reached within the engine?
		\item To avoid structural failure, the engineer must ensure that the force transmitted by the piston never exceeds~\SI{10}{\kilo\newton}. What constraint must be respected for this?
	\end{enumerate}
\exerciseend

\subsubsection{Single-flow Turbojet Engine}
\label{exo_turboreacteur_simple_flux}
\index{turbojet}

An early 1960s military aircraft is equipped with a single-flow turbojet engine (\cref{fig_exo_turbojet}). We wish to calculate the theoretical maximum speed at which it could accelerate the air at the nozzle outlet.
	
\begin{figure}
	\begin{center}
		\includegraphics[width=\textwidth]{turbojet}
	\end{center}
	\supercaption{Schematic of a turbojet engine. Air flows through the machine from left to right.}{\wcfile{Overall layout of a pure turbojet.svg}{Scheme} \ccbysa \olivier}
	\label{fig_exo_turbojet}
\end{figure}
	
The engine is tested on a stationary test bench. When air passes through the turbojet engine, it goes through four components that we will model as if they were ideal:
	
\begin{description}
	\item [The compressor] (\cref{fig_exo_compresseur_turbojet}) compresses the air adiabatically and reversibly.\\
		At the inlet, the air is at~\SI{0.9}{\bar} and~\SI{5}{\degreeCelsius}; at the outlet, the pressure is increased to~\SI{19}{\bar}.
	\item [The combustion chamber] allows for the heating of air while maintaining its pressure constant.\\
		At the outlet of the combustion chamber, the temperature is increased to~\SI{1100}{\degreeCelsius}.
	\item [The turbine] extracts energy from the air to power the compressor. In the turbine, the air expands adiabatically and reversibly.
	\item [The nozzle] is a component in which no power is added or extracted from the air. As it flows across the nozzle, the air expands adiabatically and reversibly; its speed increases significantly. At the exit of the nozzle, it has returned to atmospheric pressure and is expelled into the atmosphere.
\end{description}
	
The goal of the exercise is to calculate the speed at which the turbojet engine is capable of pushing the admitted air.
\begin{enumerate}
	\item Using the following relation,
		\begin{equation}
			\left( \frac{T_1}{T_2} \right)	= \left( \frac{v_2}{v_1} \right)^{\gamma -1} \tag{\ref{eq_isentropique_horrible1}}
		\end{equation}
		valid for a reversible adiabatic evolution of a perfect gas, show (without using equation~\ref{eq_isentropique_horrible3}) that:
		\begin{equation}
			\left( \frac{T_1}{T_2} \right)	=  \left( \frac{p_1}{p_2} \right)^{\frac{\gamma -1}{\gamma}} \tag{\ref{eq_isentropique_horrible2}}
		\end{equation}
	\item What is the air temperature at the compressor outlet?
	\item What is the specific power consumed by the compressor?
	\item What is the specific power supplied in the form of heat in the combustion chamber?
	\item What must be the temperature at the turbine outlet for it to power the compressor?
	\item What will then be the pressure at the turbine outlet?
	\item What will be the exhaust gas temperature at the nozzle exit?
	\item What will finally be the gas ejection speed at the nozzle exit?
	\item Represent the complete evolution on a pressure-volume diagram, qualitatively.
	\item On the same pressure-volume diagram, plot the evolution that the gas would follow if the compressor could not compress the air reversibly (real compressor, compression with internal friction).
\end{enumerate}
\begin{figure}[htb!]
	\begin{center}
		\includegraphics[width=0.8\textwidth]{atar_compressor}
	\end{center}
	\supercaption{Compressor of a single-flow turbojet engine \wfd{Snecma Atar}{\textsc{snecma} Atar} (1948) dissected. Air flows from the left to the center of the image.}{\wcfile{Compressor of Atar turbojet}{Photo} \ccbysa \olivier}\vspace{-0.5cm}%handmade
	\label{fig_exo_compresseur_turbojet}
\end{figure}
\exerciseend

\exercisesolutionpage
\titreresultats

\begin{description}[itemsep=2em]
	\item [\ref{exo_pression_air}]
					\tab 1) $v_1 = \frac{V_1}{m_1} = \SI{0,4}{\metre\cubed\per\kilogram}$ ; $\rho_1 = \frac{1}{v_1} = \SI{2,5}{\kilogram\per\metre\cubed}$
					\tab 2) $p_1 = \frac{R T_1}{v_1} = \SI{2,103}{\bar}$.
	\item [\ref{exo_rechauffement_reservoir_air}]
					\tab 1) $m_1 = \frac{p_1 V_1}{R T_1} = \SI{3,006}{\kilogram}$ ; $v_1 = \SI{0,3991}{\metre\cubed\per\kilogram}$ ; $\rho_1 = \SI{2,505}{\kilogram\per\metre\cubed}$ ; $p_1 = \SI{2}{\bar}$ ;
					\tab ~~ $m_2 = m_1$ ; $v_1 = v_2$ ; $\rho_1 = \rho_2$ ; $p_2 = \frac{R T_2}{v_2} = \SI{2,395}{\bar}$.\\
					\tab 2) $m_3 = \SI{2,51}{\kilogram}$, ainsi $m_\text{échap.} = m_3 - m_2 \SI{0,4959}{\kilogram}$ ;
					\tab 3) $p_4 = \frac{R T_4 m_4}{V_4} = \SI{1,67}{\bar}$.
	\item [\ref{exo_gp_energie_temperature}]
					\tab $\Delta u = c_v T_3 - u_1 = \SI{-622}{\kilo\joule\per\kilogram}$ (easily calculated with the final temperature and does not depend on the process or intermediate states).
\item [\ref{exo_puissance_pompe_air}]
					\tab $w_{1\to 2} = \Delta h = c_p \Delta T = \SI{+85,4}{\kilo\joule\per\kilogram}$ (\ref{eq_petite_sfee_deltas_h} \& \ref{eq_h_fonction_de_T})
	\item [\ref{exo_gp_turbine_turboreacteur}]
					\tab 1) With equation~\ref{eq_petite_sfee_deltas_h}, $w_\text{turbine} = c_p (T_\B - T_\A) + \frac{1}{2}(C_\B^2 - C_\A^2) - q_\fromatob = \SI{-275}{\kilo\joule\per\kilogram}$ 
					\tab 2) $\dot m = \frac{\dot W_\text{turbine}}{w_\text{turbine}} = \SI{3,64}{\kilogram\per\second}$
	\item [\ref{exo_gp_isotherme}]
					\includegraphics[width=\solutiondiagramwidth]{exo_sol_pv_isoth}
					\tab 2) With equation~\ref{eq_gp_travail_isotherme_sf1}, $w_{1 \to 2} = \SI{+257,58}{\kilo\joule\per\kilogram}$ (so, an amount of work received) ; $W_{1 \to 2} = \SI{+901,2}{\kilo\joule}$ ; $Q_{1 \to 2} = - W_{1 \to 2}$ (so, a an amount of heat expended)\\
					\tab 3) Yes, we would have $v_{2 \text{ad.rév.}} = v_1 \left(\frac{p_1}{p_2}\right)^{\frac{1}{\gamma}} > v_{2 \text{isoth.}} =  v_1 \left(\frac{p_1}{p_2}\right)$.
	\item [\ref{exo_gp_isobare_isochore}]
					\includegraphics[width=\solutiondiagramwidth]{exo_sol_pv_isob_isoch}
					\tab 2) $W_{1 \to 3} = -\int_1^2 p \diff V - \int_2^3 p \diff V = -p_\text{cste} \Delta V - 0 = \SI{+144}{\kilo\joule}$
					\tab 3) $Q_{1 \to 3} = U_3 - U_1 - W_{1 \to 3} = m c_v \left(T_3 - \frac{p_1 V_1}{m R}\right) - W_{1 \to 3} = \SI{-616,5}{\kilo\joule}$
	\item [\ref{exo_gp_isentropique}]
					\includegraphics[width=\solutiondiagramwidth]{exo_sol_pv_isentr}
						\tab Optimal case: reversible adiabatic compression. Using equation~\ref{eq_isentropique_horrible2}, we calculate $T_\B = \SI{896,4}{\kelvin}$ ; $W_\text{minimal} = m \ c_v (T_\B - T_\A) = \SI{+2,166}{\mega\joule}$.
	\item [\ref{exo_bete_mechant}]
		\tab Isothermal $2 \to 3$, isochoric $1 \to 2$.
	\item [\ref{exo_retrouver_pv}]
		\tab \textit{Clockwise, starting horizontally, on both diagrams:} isobaric ($p$~constant), isothermal ($T$~constant), reversible adiabatic, isochoric ($v$~constant).
	\item [\ref{exo_compresseur_turboreacteur}]
		\includegraphics[width=\solutiondiagramwidth]{exo_sol_pv_turbojet_compressor}
		\tab 1) Replace $v_2$ with $\frac{R T_2}{p_2}$, do the same with $v_1$. Work out the algebra and the result will come naturally ;$T_\B = \SI{600,7}{\kelvin}$, ainsi $\dot{W}_{\fromatob} = \SI{+20,87}{\mega\watt}$ ;
						\tab 3) \S\ref{ch_gp_isentropiques} ;
						\tab 5) $T_\C = \SI{279,8}{\kelvin}$ ; $T_\D = \SI{753,1}{\kelvin}$ ; Ainsi $\dot{W}_\text{real compressor} = \dot{W}_{\text{friction losses} \A \to \C} + \dot{W}_\fromctod = \SI{+29,29}{\mega\watt}$ (\SI{+40}{\percent}).
	\item [\ref{exo_compression_combustion_diesel}]
						\includegraphics[width=\solutiondiagramwidth]{exo_sol_pv_diesel}
						\tab 1) see \chaprefp{ch_travail_fdl} \& \chaprefp{ch_travail_pdv};
						\tab 2) $W_{\fromatob} = - m \int_\A^\B p \diff v = \SI{+1,298}{\kilo\joule}$ 
						\tab 3) Wth $p_\B = k v_\B^{\num{-1,5}} = \SI{31,6}{\bar}$, we have $T_\B = \SI{926,3}{\kelvin}$. Then, $Q_{\fromatob} = \Delta U - W_{\fromatob} = \SI{+0,3254}{\kilo\joule}$.
						\tab 5) With constant pressure, with equation~\ref{eq_q_gp_sf_isobare},  $T_\C = \frac{Q_\frombtoc}{m \ c_p} + T_\B = \SI{1483,7}{\kelvin}$ (\SI{1211}{\degreeCelsius}).
						\tab 6) $S < \frac{F_\text{max.}}{p_\C} = \SI{3,164e-3}{\metre\squared}$ (diameter $D_\text{max} = \SI{6,35}{\centi\metre}$).
	\item [\ref{exo_turboreacteur_simple_flux}]
						\includegraphics[width=\solutiondiagramwidth]{exo_sol_pv_turbojet_1}
						\includegraphics[width=\solutiondiagramwidth]{exo_sol_pv_turbojet_2}
						\tab 2) With equation~\ref{eq_isentropique_horrible2}, $T_\B = \SI{664,83}{\kelvin}$
						\tab 3) With equation~\ref{eq_petite_sfee_deltas_h}, $w_\text{compressor} = w_\fromatob = \SI{+388,61}{\kilo\joule\per\kilogram}$
						\tab 4) $T_\C = \SI{1373,15}{\kelvin}$ ; thus $q_\text{combustion} = q_\frombtoc = \SI{+711,86}{\kilo\joule\per\kilogram}$
						\tab 5) Since $w_\text{turbine} = - w_\text{compresseur}$, we have $T_D = \SI{986,47}{\kelvin}$
						\tab 6) With equation~\ref{eq_isentropique_horrible2}, $p_\D = \SI{5,97}{\bar}$
						\tab 7) Idem, with equation~\ref{eq_isentropique_horrible2}, $T_\text{E} = \SI{574,49}{\kelvin}$
						\tab 8) With equation~\ref{eq_petite_sfee_deltas_h}, $C_\text{E} = \left(-2\Delta h\right)^\frac{1}{2} = \SI{909,98}{\metre\per\second}$\\
						\tab Of course, these values do not account for the irreversibilities in an actual turbojet. These effects are addressed in exercise \ref{exo_compresseur_turboreacteur} \pagerefp{exo_compresseur_turboreacteur} and formalized in \chapterten. 
\end{description}
