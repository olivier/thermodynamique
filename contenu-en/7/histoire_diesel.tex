\atstartofhistorysection
\section[A Bit of History: Rudolf Diesel's Dream]{A Bit of History:\\ Rudolf Diesel's Dream}
\label{ch_histoire_diesel}\index{Diesel!Rudolf|(textbf}

	This is the story of an engine born in the margin of thermodynamics lecture notes. "\textit{Kann man Dampfmaschinen konstruieren, welche den vollkommenen Kreisprozess ausführen, ohne zu sehr kompliziert zu sein?}": can we build steam engines that can perform the ideal cycle without being too very complex? The student Rudolf Diesel asked himself this question in the margin of his notes in 1878 in Munich, realizing that the engine cycle followed by the steam engines of his time inevitably condemned them to mediocre efficiencies.

	\index{isotherms, evolutions}\index{evolutions@évolutions!isotherms}\index{direct injection}
	In this way, the concept of a \textit{rational heat engine} would mature over the years, an engine whose characteristics were finally published in 1893~\cite{diesel1893, diesel1893en}. Rudolf Diesel is unequivocal: "an examination of their operating theory will show that gas and air engines operate on a defective principle, and no improvement will produce better results as long as this principle is retained". He was no kinder to the designers of steam engines.\\
	The main features of the proposed engine strictly stemmed from physical precepts: it was about getting as close as possible to the Carnot cycle, by "\textit{producing the highest temperature of the cycle (the combustion temperature) not through and during combustion, but before and independently of it, entirely through the compression of ordinary air}". This was followed by combustion at constant temperature, controlled by progressive fuel injection. Only the exhaust and intake (done at constant pressure with a four-stroke cycle) deviated from the Carnot cycle.

	\begin{figure}
		\begin{center}
			\includegraphics[width=5cm]{rudolf_diesel_1883}
		\end{center}
		\supercaption{Rudolf Diesel in 1883.}{\wcfile{Diesel 1883}{Photo} by unknown author (\pd)}
		\label{fig_rudolf_diesel}
	\end{figure}

	\index{auto-ignition}\index{fuel!auto-ignition}\index{direct injection}
	The characteristics announced on paper give food for thought: the maximum compression pressure must be at least $p_2 = p_1 \left( \frac{T_\text{combustion}}{T_\text{initial}} \right)^{\frac{\gamma}{\gamma -1}}$ (\ref{eq_isentropique_horrible2}), which lead Rudolf Diesel to \SI{300}{\bar} -- twenty times more than existing engines! The concept of direct injection, which is made necessary by the high temperatures reached during compression to avoid premature combustion, is convincing. However, many details were lacking on how to handle coal dust -- the fuel chosen by Rudolf Diesel for its abundance and low cost -- so as to allow its direct injection into the cylinders in practice.

	\index{pressure!maximum of an engine}\index{engine!maximum pressure of an}
	Despite everything, Rudolf Diesel, after a promising start in the design of refrigeration systems, managed to convince the \textit{Maschinenfabrik Augsburg-Nürnberg} company, known today as \textsc{man}, to finance his research. They would be challenging: the transition from theory to practice took four years. Many ambitions were scaled back: the maximum pressure decreased to \num{90} and then \SI{40}{bar}, the coal dust was abandoned in favor of a crude oil for easier handling. Since the structural limits of the engine constrained the cycle, the isothermal combustion was replaced by isobaric at maximum pressure. The second prototype, a single-cylinder nearly three meters tall (\cref{fig_diesel_second_prototype}), was the first to operate autonomously: eight minutes in February 1894. The performance of the third prototype (\cref{fig_diesel_third_prototype}) was independently measured in 1897: \SI{17}{hp} at \SI{154}{rpm}, and an efficiency of \SI{26.2}{\percent}. This efficiency was twice that of its contemporaries with internal combustion, and four times that of the best steam engines!
	\begin{figure}
		\begin{center}
			\includegraphics[width=6cm]{diesel_second_prototype}
		\end{center}
		\supercaption{The second prototype developed at \textsc{man} by Rudolf Diesel, and the first to operate independently, in February 1894. It has only one cylinder with a diameter of \SI{22}{\centi\meter}, and the direct fuel injection is done by a compressed air circuit. The engine is now exhibited at the headquarters of the \textsc{man} company.}%
		{\wcfile{First Diesel}{Photo} \ccbysa MAN SE}
		\label{fig_diesel_second_prototype}
	\end{figure}
	\begin{figure}
		\begin{center}
			\includegraphics[width=8cm]{diesel_third_prototype}
		\end{center}
		\supercaption{Diesel's third prototype, and the first operational engine. The cylinder stroke of \SI{25}{\centi\meter} in diameter reaches \SI{40}{\centi\meter}. It will be tested at the technical university of Munich where it achieved \SI{26.2}{\percent} efficiency in 1897. It is exhibited at the \textit{Deutsches Museum}.}%
		{\wcfile{Historical Diesel engine in Deutsches Museum}{Photo} \ccbysa \olivier}
		\label{fig_diesel_third_prototype}
	\end{figure}

	\index{Diesel!engine}
	\textsc{Man} quickly started sales of the \textit{rational engine}, renamed as \textit{Diesel engine}, which gradually met success in Europe. Its operational regularity, reliability, and especially its low fuel consumption justified its significant purchase cost: due to the materials and precision manufacturing it required, its price per \si{watt} of power is about three times higher than its competitors. The patents filed by Rudolf Diesel brought him a significant income.

	The numerous documents left behind make Diesel a compelling figure: cultivated, diligent, and intelligent (he excelled in all his studies), he had a very keen perception of the economic and social upheavals caused by the rapid mechanization of industry and transportation at the end of the 19\textsuperscript{th} century~\cite{grosser1978,thomas1978,coltrane1997}. After a harsh and miserable childhood, expelled from France and then England, he nurtured a strong social ideal that lead him to write \textit{Solidarismus} ("\textit{the rational and economic salvation of humanity}", 1903~\cite{diesel1903}). For him, the decentralization of mechanical power production, for small businesses or collectives, for example, would constitute a decisive social advancement.

	Despite the remarkable success achieved in fifteen years, Rudolf Diesel struggled to find fulfillment. He was constantly the target of legal disputes, since his critics and competitors argued -- not entirely without merit -- that the engines he commercialized were ultimately very different from the machine described in his patent. The nationalist tensions leading up to the outbreak of World War I shake him. A poor financial manager, made multiple unreasonable expenses and ruinous investments, and, above all, he was plagued by severe migraines and medical problems. In 1913, the man seemed tormented by his own ethical and philosophical questions. His engines exclusively produced power in factories and power plants: did they ultimately contribute to the emancipation or the servitude of the working classes? He ended his life in September.

	\begin{figure}
		\begin{center}
			\includegraphics[width=8.5cm]{diesel_sulzer_rta76}
		\end{center}
		\supercaption{A nine-cylinder \textit{Sulzer RTA76} Diesel engine producing \SI{25}{\mega\watt} of power at \SI{95}{rpm}. This unit is installed here in a factory but the model is commonly used to propel merchant ships.}{\wcfile{SULZER 9RTA76.JPG}{Photo} \ccbysa by \wdu{Sleipnir}}
		\label{fig_sulzer_rta76}
	\end{figure}
	The tragic disappearance of its creator would not suffice to slow down the progression of the Diesel engine. The technological obstacles to its adoption in transportation, in particular the delicate fuel injection system, were overcome one by one. Today, it is used wherever constraints of economy and durability take precedence over lightness and responsiveness.\\
	In merchant ships, multi-story Diesel engines operate on highly turbocharged two-stroke cycles. In these engines exceeding \SI{2000}{\tonne} and \SI{18,000}{hp} (\cref{fig_sulzer_rta76}), the cylinders move slowly on strokes of over two meters, allowing for nearly isothermal combustion at \SI{80}{rpm} and an efficiency exceeding~\SI{50}{\percent}.\\
	At the other end of the spectrum, Diesels powering the smallest utility vehicles benefit from numerous systems to increase their responsiveness and extend their power and torque ranges. In these tiny machines of \SI{80}{hp} rotating beyond \SI{2000}{rpm}, electronic control systems perform up to four fuel injections at \SI{2000}{bar} directly into the cylinder for each combustion, optimizing combustion based on the demanded power~\cite{bosch2005}.

	Ultimately, there is probably not a product in today's industry that we manufacture whose materials or components have not been extracted, assembled, and transported without the contribution of power from a Diesel engine. A great achievement for a curious student!

\index{Diesel!Rudolf|)textbf}
\atendofhistorysection

