\begin{boiboiboite}
	\propwater
	\propair
	\isentropics
	\isothermals
	\carnotefficiencies
\end{boiboiboite}

\subsubsection{Maximum efficiency of an engine}
\label{exo_efficacite_moteur_carnot}

	What is the maximum efficiency that a steam power plant can reach when operating in the atmosphere at room temperature (\SI{15}{\degreeCelsius}), with a maximum temperature of \SI{800}{\degreeCelsius}?
\exerciseend

\subsubsection{Maximum efficiency of a refrigerator}
\label{exo_efficacite_refrigerateur_carnot}

What is the theoretical maximum efficiency that a household freezer could reach when operating between temperatures of \SI{-6}{\degreeCelsius} and \SI{20}{\degreeCelsius}?

	For what reason(s) is the \textsc{cop} reached by conventional freezers (around \num{3}) lower than this value?
\exerciseend

\subsubsection{Maximum efficiency of a heat pump}
\label{exo_efficacite_thermopompe_carnot}

A person wants to install a heat pump to heat their home with a power of \SI{10}{\kilo\watt}.

\begin{enumerate}
		\item Explain briefly why the performance of a heat pump is expressed as:
		\begin{equation}
		\eta_\text{heat pump} = \left| \frac{\dot Q_\out}{\dot W_\net} \right|
		\end{equation}

		\item Estimate the theoretical minimum consumption of the pump on a very cold evening ($T_\text{ext.} = \SI{-12}{\degreeCelsius}$; $T_\text{int.} = \SI{20}{\degreeCelsius}$).

		\item What will be the minimum consumption of the heat pump when the internal and external temperatures are \SI{17}{\degreeCelsius} and \SI{16}{\degreeCelsius} respectively ?

		\item What will be the theoretical minimum consumption of the heat pump in the case where the internal and external temperatures are identical? What happens in theory if the external temperature is higher than inside?
\end{enumerate}

\exerciseend

\subsubsection{Carnot cycle}
\label{exo_cours_carnot}

	Since this cycle plays a central role in thermodynamics, it is useful to be able to describe it precisely:

	\begin{enumerate}

		\item Describe briefly the four phases of a Carnot engine cycle, describing the direction of heat transfers.

		\item Why are heat transfers isothermal?

		\item Is it preferable to use a perfect gas or a liquid-vapor mixture to perform this cycle?

		\item What practical problems does the Carnot cycle pose?

	\end{enumerate}
\exerciseend

\subsubsection{Carnot steam engine}
\label{exo_moteur_carnot_vapeur}
\index{Carnot!cycle}\index{cycle!Carnot}
% This exercise is related to exercise 8.5 (\ref{exo_diagrammes_ts})

An attempt is made to set up a steam power plant based on the Carnot cycle to generate electricity (\cref{fig_exo_carnot_circuit_vapeur}). The boiler operates at a maximum temperature of \SI{275}{\degreeCelsius} and admits water in the state of saturated liquid. When the water leaves the boiler and enters the turbine, it is in the state of saturated vapor.

	\begin{enumerate}

		\item Draw the cycle followed by water on a pressure-volume diagram, qualitatively (that is, without representing numerical values) and indicating the saturation curve.

		\item At what pressure would the water need to be cooled to achieve an efficiency of \SI{40}{\percent}?

		\item What would be the power provided by the plant if its mass flow rate was \SI{9}{\kilogram\per\second}?

		\item What would the cycle and the machine look like if the steam were to be further heated at a constant temperature of \SI{275}{\degreeCelsius} at the outlet of the boiler? How would the efficiency of the engine vary then?

	\end{enumerate}

	\begin{figure}[h!]
		\begin{center}
			\includegraphics[width=7cm]{circuit_carnot_vapeur}
		\end{center}
		\supercaption{Schematic representation of a steam power plant operating with the Carnot cycle.}{\wcfile{Thermodynamic circuit of a steam power plant based on a Carnot cycle.svg}{Schema} \ccbysa \olivier}
		\label{fig_exo_carnot_circuit_vapeur}
	\end{figure}
\exerciseend

\subsubsection{Reversibility of machines}
\label{exo_thermopompe_plusplus}

Briefly show that it is impossible to design a heat pump with efficiency higher than that achieved by a reversible machine, for example in the same way we did with an engine in \cref{fig_plus_que_machine_de_carnot} on page \pageref{fig_plus_que_machine_de_carnot}.
\exerciseend

\subsubsection{Ideal turbine engine}
\label{exo_moteur_turbine_ideal}
\index{Carnot!cycle}\index{cycle!Carnot}
% This exercise is related to exercise 8.5 (\ref{exo_diagrammes_ts})

A group of engineers in an engineering company is working on an air engine concept, operating continuously using turbines and compressors.

	The engineers are using the Carnot cycle as a starting point. They plan to be able to supply heat at a temperature of \SI{600}{\degreeCelsius} and reject heat at a temperature of \SI{20}{\degreeCelsius}. The pressure is \SI{1}{\bar} at the inlet of the adiabatic compressor and \SI{30}{\bar} at the inlet of the adiabatic turbine. These characteristics give the engine a specific mechanical power of \SI{70}{\kilo\joule\per\kilogram}.

	\begin{enumerate}
		\item Represent schematically the general arrangement of this hypothetical engine, showing the path followed by the air, and all heat and work transfers.
		\item Starting from the definition of the efficiency of an engine, show that the efficiency of a reversible engine can be quantified by the equation
			\begin{equation}
				\eta_{\text{Carnot engine}} = 1 - \frac{T_B}{T_H} \tag{\ref{eq_efficacite_moteur_carnot_temperature}}
			\end{equation}
		\item What power will need to be supplied to the engine in the form of heat?
		\item What will be the power rejected in the form of heat?
	\end{enumerate}

	Of course, the Carnot cycle is impractical in an industrial application and the group of engineers immediately adopts a modification. In order to be able to supply heat by internal combustion, it is necessary to later vent the air from the engine. Thus, in the modified engine, the expansion in the adiabatic turbine is interrupted when the pressure reaches \SI{1}{\bar}, and the "used" air is then rejected into the atmosphere. The rest of the engine is not affected.

	\begin{enumerate}
		\shift{4}
		\item Represent the new engine cycle on a pressure-volume diagram, qualitatively, comparing it to that of the Carnot cycle.
		\item What is the temperature of the air when it is rejected from the engine?
		\item What is the reduction in power of the adiabatic turbine compared to the ideal engine?
		\item What mechanical power is saved by removing the compressor that was rejecting heat?
		\item What is now the efficiency of the engine?
	\end{enumerate}
\exerciseend

\subsubsection{Irreversibilities in a refrigerator}
\label{exo_cours_refrigerateur_carnot}

We propose to study the operation of a refrigerator starting from a theoretical cycle allowing for maximum efficiency.

	The refrigerator operates strictly on a Carnot cycle, in continuous mode, with a liquid-vapor mixture.

	\begin{enumerate}
		\item Represent the refrigeration cycle on a pressure-volume diagram, indicating the direction of heat and work transfers.
	\end{enumerate}

	Of course, in practice, adiabatic compression and expansion cannot be carried out reversibly.

	\begin{enumerate}
		\shift{1}
		\item Represent the irreversible cycle on the pressure-volume diagram.
		\item How will each of the heat and work transfers vary compared to the theoretical case?
		\item Briefly show that these variations lead to a decrease in the efficiency (the \textsc{cop}) of the refrigerator.
	\end{enumerate}
\exerciseend

\subsubsection{Refrigeration in stages}
\label{exo_refrigeration_etages}

A chemical plant uses a refrigeration system to control the temperature of hazardous products. We aim to study the least inefficient refrigeration system to equip it, here based on the Carnot cycle with a perfect gas.

The minimum refrigeration temperature is \SI{-50}{\degreeCelsius} and the heat is rejected at \SI{40}{\degreeCelsius}.

While studying the characteristics of the Carnot cycle, a beginner engineer observes that the efficiency of the refrigerator increases if the heat rejection temperature is lowered (equation \ref{eq_efficacite_refrigerateur_carnot_temperature} on page \pageref{eq_efficacite_refrigerateur_carnot_temperature}).

S/he proposes to configure the refrigerator in such a way that it rejects heat at only \SI{10}{\degreeCelsius}. This heat at \SI{10}{\degreeCelsius} would then be captured by a heat pump which would in turn bring it to \SI{40}{\degreeCelsius}.

\begin{enumerate}
			\item Show that if the refrigerator operates on a reversible cycle, the proposed modification can only increase (or at best, keep the same) the total consumption of the refrigeration system.
			\item Represent the cycle of the refrigerator and the heat pump as proposed by the engineer on the same pressure-volume diagram, in a qualitative manner.
			\item Represent, on a pressure-volume diagram, the cycles that would be followed in the two machines if their expansion phases were adiabatic (without heat transfer), but non-reversible.
		\end{enumerate}
\vspace{-0.3cm}\exerciseend

\subsubsection{Gasoline engine based on a Carnot cycle}
\label{exo_carnot_quatre_cylindres}

We aim to quantify the minimum gasoline consumption that could result from a piston-cylinder automotive engine generating \SI{100}{\kilo\watt} of power (about \SI{130}{hp}), given some practical constraints imposed by the limited available volume and weight limits:

\begin{itemize}
		\item The compression ratio (i.e., the ratio $\frac{v_\text{max.}}{v_\text{min.}}$) is \num{12} during the adiabatic phases (to limit mechanical constraints);
		\item The maximum temperature is \SI{1300}{\kelvin} (imposed by material resistance);
		\item The engine has four cylinders, each performing \num{400} cycles per minute.
	\end{itemize}

The engine is fueled by gasoline with a specific heat of combustion of \SI{40}{\mega\joule\per\kilogram}.

If we consider the best engine that can be designed:

\begin{enumerate}
		\item At what temperature would the heat be rejected?
		\item What would be the efficiency of the engine?
		\item What would be the amount of heat to be provided for each combustion, and the corresponding fuel mass?
		\item What would be the hourly gasoline consumption?
	\end{enumerate}
\exerciseend

\exercisesolutionpage
\titreresultats

\begin{description}[itemsep=2em]
\item [\ref{exo_efficacite_moteur_carnot}] $\eta_\text{max.} = \SI{73.1}{\percent}$ (eq. \ref{eq_efficacite_moteur_carnot_temperature}, see example \ref{ex_efficacite_moteur_carnot} on page \pageref{ex_efficacite_moteur_carnot}).
\item [\ref{exo_efficacite_refrigerateur_carnot}] 1) $\textsc{cop}_\text{max.} = \num{10.3}$ (eq. \ref{eq_efficacite_refrigerateur_carnot_temperature}, see example \ref{ex_efficacite_refrigerateur_carnot} on page \pageref{ex_efficacite_refrigerateur_carnot}); 2) Non-reversible compressions and expansions (thus $w_{4\to 3} + w_{2\to 1} > 0$), especially if a valve is used (\chapref{ch_principe_fonctionnement_refrigerateur}); Non-isothermal heat transfers.
\item [\ref{exo_efficacite_thermopompe_carnot}] 1) see \chapref{ch_rendement_thermopompe}; 2) $\dot W_\net = \frac{\dot Q_\out}{\eta_\text{max.}} = \SI{+1.09}{\kilo\watt}$; 3) $\dot W_\net = \SI{+34.5}{\watt}$ (!), $\dot W_\net = \SI{0}{\watt}$; and when $T_\text{ext.} > T_\text{int.}$, $\dot W_\net$ becomes negative: the heat pump operates as an engine\ldots
\item [\ref{exo_cours_carnot}] 1) see \chapref{ch_descritpion_cycle_carnot}, especially figures \ref{fig_carnot_quatre_etapes} and \ref{fig_carnot_quatre_etapes_so}; 2) see \chapref{ch_elaboration_cycle_carnot}; 3) It doesn't matter at all! 4) Its bulk, its power ratio are very large, and its power is infinitely small\ldots
\item [\ref{exo_moteur_carnot_vapeur}]
				\includegraphics[width=\solutiondiagramwidth]{exo_sol_pv_carnot_lv_1}
				\includegraphics[width=\solutiondiagramwidth]{exo_sol_pv_carnot_lv_2}
				We see that when performed under the saturation curve, the Carnot cycle is already easier to perform in practice, since heat transfers occur at constant pressure (and thus do not require a moving part);
				2) $T_B = (1 - \eta) T_H = \SI{55.74}{\degreeCelsius}$; thus by interpolation between \num{55} and \SI{60}{\degreeCelsius} we get $p_{\text{sat. \SI{55.74}{\degreeCelsius}}} = \SI{0.1285}{\bar}$. The condenser is thus depressurized;
				3) $q_{\text{in}} = h_{\text{LV \SI{275}{\degreeCelsius}}} = \SI{+1574.3}{\kilo\joule\per\kilogram}$: we obtain $\dot W_\net = -\dot m \eta q_{\text{in}} = \SI{-566.7}{\kilo\watt}$;
				4) The power ratio and the complexity of the machine will increase, but the efficiency will remain unchanged!
\item [\ref{exo_thermopompe_plusplus}]
				\includegraphics[width=5cm]{carnot_thermopompe_plusplus}
				A heat pump like this could be powered by a Carnot engine; together, they would form a machine capable of carrying heat from $T_L$ to $T_H$ without the need for external work (with the arbitrary values shown here, $\dot Q_{\out~\text{together}} = \SI{-40}{\watt}$).
\item [\ref{exo_moteur_turbine_ideal}]
				\includegraphics[width=\solutiondiagramwidth]{exo_sol_pv_carnot_turbine}
				1) This is the arrangement shown in \cref{fig_carnot_quatre_etapes_so} on page \pageref{fig_carnot_quatre_etapes_so};
				2) By starting from the definition \ref{def_rendement_moteur}: $\eta_{\text{engine}} \equiv \left| \frac{\dot W_\net}{\dot Q_\inn} \right| = - \frac{\dot W_\net}{\dot Q_\inn} = - \frac{-\dot Q_\inn - \dot Q_\out}{\dot Q_\inn} = 1 + \frac{\dot Q_\out}{\dot Q_\inn} = 1 - \left|\frac{\dot Q_\out}{\dot Q_\inn}\right| = 1 - \left|\frac{\dot Q_{TH}}{\dot Q_{TL}}\right|$; With definition \ref{def_echelle_temperature_kelvin} page \pageref{def_echelle_temperature_kelvin} we arrive at the requested equation \ref{eq_efficacite_moteur_carnot_temperature};
				3) $q_{\inn} = -\frac{w_\net}{\eta_{\text{engine}}} = \SI{+105.4}{\kilo\joule\per\kilogram}$;
				4) $q_{\out} = -w_\net - q_{\inn} = \SI{-35.4}{\kilo\joule\per\kilogram}$;
				6) The isothermal compressor is removed, the adiabatic turbine is truncated. $T_E = T_C \left(\frac{p_E}{p_C}\right)^{\frac{\gamma - 1}{\gamma}} = \SI{330.4}{\kelvin} = \SI{57.3}{\degreeCelsius}$ (eq. \ref{eq_isentropique_horrible2});
				7) $w_{\text{lost}} = w_{\text{isentropic turbine 1}} - w_{\text{isentropic turbine 2}} = \SI{-37.54}{\kilo\joule\per\kilogram}$;
				8) $w_{\text{saved}} = w_{\text{isentropic compressor}} = \SI{+35.4}{\kilo\joule\per\kilogram}$;
				9) $\eta_{\text{engine 2}} = \SI{64.4}{\percent}$, which is a \SI{-2}{point} change, very honorable given the considerable simplification of the machine!
\item [\ref{exo_cours_refrigerateur_carnot}] 
				\includegraphics[width=\solutiondiagramwidth]{exo_sol_pv_carnot_refrigerateur_transferts}
				\includegraphics[width=\solutiondiagramwidth]{exo_sol_pv_carnot_refrigerateur_irreversibilites}
				3) $w_{1\to 4}$ decreases, $w_{4\to 3}$ and $w_{3\to 2}$ increase, $w_{2\to 1}$ decreases; $q_{3\to 2}$ increases and $q_{1\to 4}$ decreases;
				4) As $w_\net$ increases and $q_\inn = q_{1\to 4}$ decreases, the $\textsc{cop} = \frac{q_\inn}{w_\net}$ necessarily decreases.
\item [\ref{exo_refrigeration_etages}]
				\includegraphics[height=\solutiondiagramwidth]{exo_sol_pv_refrigeration_etages_1}
				\includegraphics[height=\solutiondiagramwidth]{exo_sol_pv_refrigeration_etages_2}\\
				1) Let's bet that the student did better than the beginner engineer of the exercise: with two reversible systems in series pumping a quantity $q_\inn$ of heat at temperature $T_1 = \SI{-50}{\degreeCelsius}$, with exchange temperature $T_2 = \SI{10}{\degreeCelsius}$ and final high temperature $T_3 = \SI{40}{\degreeCelsius}$, the necessary work is $w_\text{total}
						= w_{\net1} + w_{\net2}
						= \eta_1 q_\inn + \eta_2 (q_\inn + w_{\net1})
						= \left(\frac{T_2}{T_1} - 1\right) q_\inn \ + \ \left(\frac{T_3}{T_2} - 1\right) (q_\inn + w_{\net1})
						= \left(\frac{T_2}{T_1} - 1\right) q_\inn \ + \ \left(\frac{T_3}{T_2} - 1\right) (q_\inn + \left(\frac{T_2}{T_1} - 1\right) q_\inn)
						= \left(\frac{T_2}{T_1} - 1\right) q_\inn \ + \ \left(\frac{T_3}{T_2} - 1\right) (\frac{T_2}{T_1} q_\inn)$ ;
						thus $\eta_\text{togetheraa} \equiv \frac{q_\inn}{w_\text{total}}
						= \frac{1}{\frac{T_2}{T_1} - 1 + \frac{T_3 T_2}{T_2 T_1} - \frac{T_2}{T_1}}
						= \frac{1}{\frac{T_3}{T_1} - 1}$, which is the efficiency of a single reversible machine operating between $T_1$ and $T_3$. Stacking two machines in series therefore brings no theoretical advantage.
\item [\ref{exo_carnot_quatre_cylindres}]
				\tab 1) At the outlet, $T_1 = \SI{208}{\degreeCelsius}$ (eq. \ref{eq_isentropique_horrible2});
				\tab 2) $\eta_{\text{engine}} = \SI{63}{\percent}$;
				\tab 3) $\dot Q_{\inn} = \SI{158.8}{\kilo\watt}$ so $Q_{\text{combustion one cylinder}} = \SI{5.955}{\kilo\joule}$ at each combustion. We obtain, for a cylinder, $m_{\text{fuel combustion}} = \frac{Q_{\text{combustion one cylinder}}}{q_{\text{fuel}}} = \SI{0.149}{\gram}$;
				\tab 4) $\dot{m}_{\text{fuel}} = \SI{14.3}{\kilogram\per\hour}$.
\end{description}

