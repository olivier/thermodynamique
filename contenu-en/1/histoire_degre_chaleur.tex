\atstartofhistorysection
\section[A Bit of History: Measuring the Degree of Heat]{A Bit of History: \\ Measuring the Degree of Heat}
\label{ch_histoire_degre_chaleur_depondt}

\index{Depondt, Philippe}
\begin{center}\textit{By Philippe Depondt\\ \begin{small}Pierre and Marie Curie University, Paris\end{small}}\end{center}

\index{Aristotle}
For Aristotle, in the 4\textsuperscript{th} century B.C. in Greece, fire was one of the four elements of matter along with water, air, and earth. The idea of measuring something, be it fire or anything else, that is, assigning a numerical value to a quantity, was completely foreign to him because his physics was essentially non-mathematical~\cite{koyre1966}: his theories were based on \emph{qualitative} observations. The synthesis of \mbox{Aristotle}'s ideas with Christianity was made in the 12\textsuperscript{th} century by Thomas Aquinas, and these ideas were widely dominant in the scholarly world in Europe until the early 17\textsuperscript{th} century (for example, Galileo had to argue largely \emph{against} these ideas).

Until the 17\textsuperscript{th} century, descriptions of the world would unfortunately remain largely qualitative. The exception provided by astronomers is telling: in establishing his heliocentric model in the early 16\textsuperscript{th} century, Copernicus could rely on measurements dating back to antiquity, and then on those of Arab astronomers from the Middle Ages. Similarly, it was the remarkably rigorous and precise measurements (less than one minute of angle) carried out in Tycho Brahe's modern "laboratory" that led to Johannes Kepler's discovery of his three laws, which formed one of the foundations of Newton's dynamics.

In the case of thermodynamics, the English philosopher \wfd{Francis Bacon (philosopher)}{Francis Bacon}, laying the foundations of inductive reasoning at the beginning of the 17\textsuperscript{th} century, used heat as an example to illustrate his point. In order to study its nature, he proposed in the \textit{Novum \mbox{Organum}} to compile all observations of phenomena in which heat appears, of phenomena where it does not appear, and finally of those where it appears "by degrees." This method remained qualitative, but at approximately the same time, there was an explosion of attempts to make truly quantitative measurements of this "degree of heat."

\index{thermometer}\index{atmosphere, pressure of the}\index{pressure!atmospheric}
It seems that the first thermometer was invented around 1605 by a Dutchman named \we{Cornelis Drebbel}~\cite{locqueneux1996}: based on ideas dating back to \we{Heron of Alexandria} (1\textsuperscript{st} century A.D.), it consisted of a hollow glass sphere extended by a tube pointing downwards and immersed in a colored liquid. If the sphere was heated, the liquid was pushed downwards by the expansion of the air, and conversely, if it was cooled, the liquid rose in the tube. It was thus an air thermometer (\cref{fig_air_thermometer}). This thermometer was later used to monitor fever in patients (\cref{fig_medical_thermometer}), but it had the drawback of being as sensitive to changes in atmospheric pressure as to temperature.

\begin{figure}[ht!]
	\begin{center}
		\includegraphics[height=0.4\textwidth]{thermometre_air_1626}
	\end{center}
	\supercaption{Air thermometer from the early 17\textsuperscript{th} century. The ball was filled with gas whose volume varies with its temperature, pushing the water from the reservoir below whose surface is at atmospheric pressure. The liquid could be colored, and its height variations were measured using a scale. Since atmospheric pressure varies with meteorological conditions, it affected the measurements: it was, in a way, a baro-thermometer.\index{atmosphere, pressure of the}\index{pressure!atmospheric}}{Engraving by Robert Fludd (1626, \pd), selected by Lamouline 2005~\cite{lamouline2005}}
	\label{fig_air_thermometer}
\end{figure}

\begin{figure}[ht!]
	\begin{center}
		\includegraphics[width=0.35\textwidth]{thermometre_medical_1625}
	\end{center}
	\supercaption{Medical thermometer from the early 17\textsuperscript{th} century. The gas bulb was placed in the patient's mouth. One can well imagine that the thermometer’s sensitivity to atmospheric pressure was not the biggest obstacle to its adoption… \index{atmosphere, pressure of the}\index{pressure!atmospheric}}{Drawing by Santori \& Avicenne (\textit{Commentaria in primam Fen primi libr Avicennae}, 1625, \pd), selected by Lamouline 2005~\cite{lamouline2005}}
	\label{fig_medical_thermometer}
\end{figure}

In the middle of the century, liquid thermometers would prove to be much more reliable and easier to use. The glass bulb was now placed at the bottom of the device and filled with colored liquid that rose in a graduated tube; this tube was initially open, but it was found that by closing it, evaporation of the liquid could be prevented (\cref{fig_florence_thermometer}). These improvements had been strongly supported by the Italian grand duke Ferdinando II de' Medici, and these devices were thus called "Florence thermometers."

\begin{figure}[ht!]
	\begin{center}
		\includegraphics[height=0.62\textwidth]{thermometre_florence_1667}
	\end{center}
	\supercaption{Florence thermometer from the mid-17\textsuperscript{th} century. This time, it was the liquid, contained in the lower bulb, that contracted and expanded with temperature. Its volume variations were such that a long spiral-blown glass tube was needed to measure them.}{Engraving by the \textit{Accademia del cimento} (\textit{Staggi di naturali esperientze}, 1667, \pd), selected by Lamouline 2005~\cite{lamouline2005}}
	\label{fig_florence_thermometer}
\end{figure}

\index{temperature!zero}\index{temperature!scale}\index{Amontons, Guillaume}\index{atmosphere, pressure of the}\index{pressure!atmospheric}
The issue of graduations remained. The number of graduations varied widely, with artisans simply trying to reproduce what they had already created themselves; at best, thermometers built by the same person gave roughly similar results. Due to the lack of a universally accepted scale, it was impossible to make measurements in different locations with different instruments for comparison.

In the early 18\textsuperscript{th} century, the Frenchman \we{Guillaume Amontons} built an air thermometer based on the measurement of a pressure difference rather than volume. Having observed that if boiling water continued to be heated, its degree of heat did not increase, he used this as a fixed point of reference. Of course, the measurements needed to be corrected by simultaneously measuring atmospheric pressure. This system allowed Amontons to make a major discovery: if the gas pressure increases as the degree of heat increases, conversely, it decreases as the degree of heat decreases. At a minimum, this pressure must become zero, as well as the degree of heat. This extrapolated minimum corresponds to, in modern units, \SI{-239.5}{\degreeCelsius}... A first measure of absolute zero!

\index{reaumur@Réaumur, René-Antoine Ferchault de}\index{Fahrenheit, Daniel Gabriel}\index{Celsius, Anders}
However, all these thermometers remained difficult to use, significantly limiting their dissemination. \wed{Ren\%C3\%A9-Antoine_Ferchault_de_R\%C3\%A9aumur}{René-Antoine Ferchault de Réaumur}, around the middle of the 18\textsuperscript{th} century, developed a water-alcohol mixture thermometer in which the alcohol level is precisely fixed to ensure the reproducibility of the instrument. He calibrated it by choosing two references (melting ice and boiling water) and dividing this interval into 80 degrees. This scale is called the "Réaumur scale."

In 1724, in Danzig, the German \we{Daniel Gabriel Fahrenheit} described a thermometer that used the expansion of mercury and introduced a scale in which melting ice is at 32 degrees and the temperature of blood at 96 degrees; a mixture of ice, water, and sal ammoniac gave him the zero of his scale (see also section \chaprefp{thermometres_celsius_fahrenheit}).

In 1741, the Swedish \we{Anders Celsius} adopted the Réaumur scale but divided it into 100 intervals instead of 80. This convention was widely spread in France, and in 1794, at the time of the adoption of the metric system by the Convention, the Celsius scale was chosen as the official temperature scale.

The transition from the subjective sensation of hot and cold to the objective measurement of temperature with reliable instruments and a universal scale led to a large number of observations that were not self-evident until then: the temperature of a cellar is not higher in winter than in summer, iron is not "colder" than wood, etc., and all in all, this is quite recent!

\atendofhistorysection

