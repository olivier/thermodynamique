\begin{boiboiboite}
	\propwater
	\propair
	\isentropics
\end{boiboiboite}

\subsubsection{Engine efficiency}
\label{exo_efficacite_moteur}

	The Diesel engine of an excavator has an efficiency of \SI{40}{\percent} and delivers a continuous power of \SI{60}{\kilo\watt} (approximately \SI{80}{hp}). It is powered by fuel with a calorific value of \SI{35}{\mega\joule\per\kilogram}.

	\begin{enumerate}
		\item What is the hourly fuel consumption of the machine?
		\item What is the power rejected as heat in the exhaust pipe?
	\end{enumerate}
\exerciseend

\subsubsection{Refrigerator efficiency}
\label{exo_efficacite_refrigerateur}

	A refrigerator with a \textsc{cop} of \num{1.2} must extract \SI{100}{\kilo\joule} from food placed in the cold chamber. How much electrical energy must it consume for this? How much heat will it have rejected at the end of the cooling process?
\exerciseend

\subsubsection{Heat pump efficiency}
\label{exo_efficacite_thermopompe}

	A heat pump with a \textsc{cop} of \num{3.1} provides a power of \SI{4000}{\watt} to an apartment. What is the electrical power consumed? What is the power absorbed from the atmosphere?
\exerciseend

\subsubsection{Thermodynamics Party}
\label{exo_bieres}

	A group of students, made thirsty by an endless thermodynamics class, prepares for the weekend by placing ten six-packs of bottles containing a beverage containing mostly mineral water in the refrigerator (\cref{fig_six_pack}).

	An experiment conducted on a bottle shows that it is made of \SI{172}{\gram} of glass with a specific heat capacity of \SI{0.75}{\kilo\joule\per\kilogram\per\kelvin}, and that it contains \SI{25}{\centi\liter} of liquid with a specific heat capacity of \SI{4.2}{\kilo\joule\per\kilogram\per\kelvin}.

	When they are inserted in the refrigerator, the packs are at room temperature (\SI{19}{\degreeCelsius}). Four hours later, they have reached a temperature of \SI{5}{\degreeCelsius}.

\begin{figure}[htp]%handmade
	\begin{center}
	\includegraphics[width=2.5cm]{6-pack}
	\end{center}
	\supercaption{A pack of six bottles containing a liquid used to drown the exasperation resulting from the study of thermodynamics}{\wcfile{El_"quintu"_de_Moritz}{Photo} \ccby Moritz Barcelona}
	\label{fig_six_pack}
\end{figure}

	The refrigerator has an efficiency of \SI{95}{\percent}. The imperfectly insulated walls of the refrigerator absorb heat from the room at a rate of \SI{10}{\watt}.

	\begin{enumerate}
		\item How much electrical energy did the refrigerator consume during these four hours?
	\end{enumerate}

	The local electricity grid operator applies a tariff of \SI[per-mode=symbol]{0.15}{\euroo\per\kilo\watt\per\hour}.

	\begin{enumerate}
		\shift{1}
		\item What is the financial cost of the cooling performed?
		\item Has the room where the refrigerator is stored cooled down or warmed up?
		\item Will the room cool down if the refrigerator door is left open?
	\end{enumerate}
\exerciseend

\subsubsection[Operation of a heat pump]{Operation Of a Heat Pump}
\label{exo_fonctionnement_thermopompe}

	Describe the thermodynamic cycle followed by the fluid inside a heat pump, indicating the direction of heat flows and the location (inside/outside) of the different components.

Why do we let the fluid expand in a valve instead of using a turbine that could provide work?

	(One can also practice by focusing on the cycles and configurations of an air conditioner, a refrigerator, or an engine: which part is heated and at what temperature?)
\exerciseend

\subsubsection{Algebra}
\label{exo_algebre_rendement_climatiseur}

Show, starting from the definition of the efficiency of an air conditioner, that it can be expressed as the relation:
	\begin{equation}
		\eta_\text{air conditioner} = \frac{1}{\left| \frac{\dot{Q}_\text{out}}{\dot{Q}_\text{in}} \right| - 1} \tag{\ref{eq_rendement_refrigerateur_qin_qout}}
	\end{equation}

	(This demonstration will be useful in the next chapter (\S\ref{ch_efficacite_maximale_machines}). One can also practice by attacking in the same way equations~\pagerefp{eq_rendement_moteur_qin_qout} and~\pagerefp{eq_rendement_thermopompe_qin_qout}. Pay attention to absolute values!)
\exerciseend

\subsubsection{Cooling Of a Wind Tunnel}
\label{exo_refrigeration_soufflerie}

	The cryogenic wind tunnel \textsc{etw} (for \textit{European Transonic Windtunnel}, \cref{fig_wind_tunnels}) allows the circulation of nitrogen in a closed circuit to observe flows around models. It allows reaching Mach~\num{0.8} at~\SI{4}{\bar} and~\SI{-200}{\celsius}, using a fan of~\SI{50}{\mega\watt}.

	The walls of the wind tunnel are highly insulated, so that its thermal transfers with the outside are negligible compared to other energy transfers. The nitrogen cooling system has a \textsc{cop} of \num{0.8}.

	When the wind tunnel is operating at full capacity, what mechanical power is consumed by the cooling system? What power is rejected as heat into the atmosphere? 

\begin{figure}[htp]
	\begin{center}
		\includegraphics[width=0.8\columnwidth]{etw}
		\includegraphics[width=0.8\columnwidth]{langley_transonic}
	\end{center}
	\supercaption{Buildings of the \textsc{etw} (European Transonic Windtunnel) in Cologne and test section of the National Transonic Facility of NASA, of similar size and capabilities.}{\wcfile{Etwrp}{Photo 1} \ccbysa by \wcu{Dantor}\\
	\wcfile{Pathfinder_I_with_Pressure_Wing_-_GPN-2000-001291}{Photo 2} by Fred Jones (NASA, \pd)}
	\label{fig_wind_tunnels}
\end{figure}
\exerciseend

\subsubsection{Electricity Generation With a Gas Turbine}
\label{exo_generatrice_electricite_turbine}

	A gas turbine (in English the term \vocab{gas turbine} can be used to describe the engine as a whole) is set up to operate an electric generator (\cref{fig_exo_turbomotor}); it operates with a flow rate of \SI{0.5}{\kilogram\per\second} of atmospheric air.

\begin{figure}[ht!]
	\begin{center}
	\includegraphics[width=\textwidth]{turbomoteur_generatrice}
	\end{center}
	\supercaption{A static gas turbine engine, in this configuration named \vocab{turboshaft}, powering an electric generator. The gas is typically expanded (in the turbine, between C and D) to atmospheric pressure.}{\wcfile{Overall layout of a turboprop engine and a turboshaft engine.svg}{Diagram} \ccbysa \olivier}
	\label{fig_exo_turbomotor}
\end{figure}

	\begin{itemize}
		\item The air enters the machine at \SI{20}{\degreeCelsius} and \SI{1}{\bar}; it is compressed ($\fromatob$) to \SI{30}{\bar} in the compressor.
		\item The air then receives heat through combustion, at constant pressure ($\frombtoc$), until its temperature reaches \SI{1000}{\degreeCelsius}.
		\item Finally, the air is expanded in a turbine ($\fromctod$) until it reaches atmospheric pressure and is discharged outside.
	\end{itemize}

	The compressor is mechanically powered by the turbine, and the shaft connecting them also drives the electric current generator.

	In order to quantify the maximum efficiency that could be achieved by the machine, we consider that the compressor and the turbine are reversible adiabatic (meaning that compression and expansion occur very slowly and without heat transfer).

	\begin{enumerate}
		\item Draw the path followed by the air during one cycle on a pressure-volume diagram, qualitatively (meaning without representing numerical values).
		\item At what temperature does the air exit the compressor?
		\item What is the power of the compressor?
		\item At what temperature is the air rejected into the atmosphere? What power is rejected as heat into the atmosphere?
		\item What is the efficiency of the machine?
		\item How do the four energy transfers of this theoretical machine compare to those of a real machine, where the compressor and the turbine cannot be reversible?
	\end{enumerate}
\exerciseend

\subsubsection{Cycle Of a Steam Engine}
\label{exo_centrale_vapeur_cycle}\index{cycle!Rankine}\index{Rankine!cycle}

	In a steam power plant, water circulates continuously through four components:

	\begin{itemize}
		\item A quasi-adiabatic pump where water enters as saturated liquid and its pressure is raised from \SI{0.5}{\bar} to \SI{40}{\bar};
		\item A boiler where its temperature is raised to \SI{650}{\degreeCelsius} at constant pressure;
		\item A quasi-adiabatic turbine that allows the water to return to \SI{0.5}{\bar} while expending energy as work;
		\item A condenser that cools the water at constant pressure (\SI{0.5}{\bar}) until its return to the pump.
	\end{itemize}

	We accept the following assumptions:
		\begin{itemize}
			\item At the turbine outlet, the steam is at a temperature of \SI{110}{\degreeCelsius} (after \chaptereightshort, we will be able to predict this outlet temperature).
			\item The compression in the pump is reversible, and the density of water does not vary as it passes through~it.
		\end{itemize} 

	\begin{enumerate}
		\item Draw the cycle followed by water on a pressure-volume diagram and on a temperature-volume diagram, qualitatively.
		\item What is the efficiency of the engine?
		\item What would happen if, in order to eliminate heat rejection, we removed the condenser by connecting the pump inlet directly to the turbine outlet?
	\end{enumerate}
\exerciseend

\subsubsection{Industrial Refrigeration}
\label{exo_refrigeration_supermache}

	A supermarket chain is seeking your expertise to assess the profitability of an ambitious project to renew a fleet of refrigerators.

	All supermarkets in the company use the same model of refrigerator. Its efficiency is \SI{100}{\percent}.

	You travel to a representative supermarket, which allows you to take measurements and quantify the thermal transfers of the building. You highlight that:

	\begin{itemize}
		\item The power absorbed as heat by the cold chambers of the refrigerators, averaged over the year, is \SI{80}{\kilo\watt}.
		\item In the winter, the building loses heat with an average power of \SI{400}{\kilo\watt}. It is heated with a heat pump unit of \textsc{cop} \num{4}.
		\item In the summer, the building absorbs heat with an average power of \SI{160}{\kilo\watt}. It is cooled with an air conditioning unit of \textsc{cop} \num{0.9}.
		\item During autumn and spring, the heating/cooling needs are almost negligible.
	\end{itemize}

	The company is considering replacing its entire fleet of refrigerators with a model of \SI{220}{\percent} efficiency, which requires a significant investment. It has a total of 100 supermarkets and pays \SI{0.15}{\euro} per \si{\kilo\watt\hour} on average for electricity.

What would be the annual financial savings generated by changing the refrigerator model?
\exerciseend

\subsubsection{Operation Of an AC Unit}
\label{exo_fonctionnement_climatiseur}\index{cycle!de réfrigération (ou climatisation)}\index{climatiseur}\index{reversibilite@réversibilité!réversible vs. inversable}\index{inversable vs. réversible}

	An air conditioner operates according to the circuit schematized in \cref{fig_air_conditioner}. The fluid used in the circuit is air (in practice, fluids that liquefy and evaporate within the machine are often used instead, but the operating principle remains the same).

\begin{figure}
	\begin{center}
	\includegraphics[width=\textwidth]{circuit_conditionnement_air}\vspace{-0.5cm}%handmade
	\end{center}
	\supercaption{Schematic diagram of an air conditioner. The air in the air conditioner circuit circulates continuously ($\A \to \B \to \C \to \D \to \A$), without ever leaving the machine.}{\wcfile{Circuit_air_conditioner.svg}{Diagram} \ccbysa \olivier}
	\label{fig_air_conditioner}
\end{figure}

	The air inside the circuit circulates continuously. Both the compressor and the turbine are adiabatic and we consider them to be reversible. Heat transfers occur at constant pressure.

	When the air conditioner is started, the outside temperature and the inside temperature are both \SI{30}{\degreeCelsius}.

	The temperatures of the air inside the circuit are $T_\A = \SI{20}{\degreeCelsius}$, $T_\B = \SI{60}{\degreeCelsius}$, and $T_\C = \SI{40}{\degreeCelsius}$.

	\begin{enumerate}
		\item Represent the evolution on a pressure-volume diagram, qualitatively, and indicate the work and heat transfers.
		\item What is the pressure ratio between A and B?
		\item What is the temperature of the air at point D?
		\item Calculate the specific powers for each of the four energy transfers, and thus calculate the efficiency of the air conditioner.
		\item The owner wants to obtain a flow of cool air at \SI{12}{\degreeCelsius} with a flow rate of \SI{0.25}{\cubic\meter\per\second}. What electrical power must be provided to the air conditioner for this purpose?
		\item What will be the minimum flow rate of outside air to be circulated in the outdoor section of the air conditioner?
		\item During the winter, the owner wants to modify the air conditioner to turn it into a heat pump. Describe (qualitatively) a modification of the circuit for this purpose, and draw the evolution of the air in the circuit on a new pressure-volume diagram, indicating the energy transfers.
	\end{enumerate}
\exerciseend

\subsubsection{Conditioning Pack Of an Aircraft}
\label{exo_pack_conditonnement}\index{packaging unit}\index{refrigeration cycle (or air conditioning)}\index{air conditioner}\index{heat pump}

	A "conditioning pack" or simply "pack" is a thermodynamic machine used in commercial aircraft to pressurize the fuselage and to maintain the temperature in the cockpit, cabin, and cargo holds at a comfortable level regardless of external conditions.

	The packs (often called \textsc{ecs} or \textsc{acm}, for Environment Control System and Air Cycle Machine) are often placed around the wing box in the unpressurized area of aircraft (\cref{fig_pack_ssj}). An interesting feature of their operation is that it is the air from the thermodynamic circuit itself that is inserted into the cabin for the passengers.

	\begin{figure}
		\begin{center}
			\includegraphics[height=.35\textwidth]{liebherr_pack}
		\end{center}
		\supercaption{A \textsc{ecs} intended for a Comac C919, approximately \SI{1.5}{\meter} in length.}{\wcfile{Air conditioning pack of Comac C919 (1)}{Photo} \ccbysa \olivier}
		\label{fig_pack_c919}
	\end{figure}

	Here we study the heating and air conditioning functions of a conditioning pack: for this purpose, we model its operation in a simplified manner.

	The air intended for the cabin begins its journey at the inlet of the turbine engines of the airplane (except when the airplane, on the ground, is connected to a source of conditioned or pressurized air). In the compressor of one of these engines, its pressure is multiplied by \num{5} during an approximately adiabatic and reversible process; then it is led to the pack.

	\index{heat exchanger}
	As it enters the pack, this air passes through a heat exchanger where it loses heat (figure \ref{fig_pack}). This heat is extracted by a separate air flow, called \textsc{ram}: it is air from outside at atmospheric conditions, extracted and discharged under the fuselage. The cabin air and \textsc{ram} air circuits are at very different pressures and are never mixed.

	\begin{figure}
		\begin{center}
			\includegraphics[width=\textwidth]{pack}
		\end{center}
		\supercaption{Diagram representing the air arriving in the conditioning pack from the engines (\textsc{en1} and \textsc{en2}) or the auxiliary power unit (\textsc{apu}) on the left. This air exits into one of the three circuits A, B, or C after losing heat to the \textsc{ram} air.}{Diagram \cczero \oc}
		\label{fig_pack}
	\end{figure}

	After passing through the heat exchanger, the air can follow three distinct circuits in the conditioning pack before reaching the cabin:
		\begin{description}
			\item[Circuit A] is used in cold weather when one wants to raise or maintain the cabin at a higher temperature than the outside temperature;
			\item[Circuit B] is used in moderate weather when the cabin needs to be kept at a temperature close to the outside temperature;
			\item[Circuit C] is used in hot weather when there is a high demand for cabin air cooling.
		\end{description}

	The pack automatically controls the flow of outside air (\textsc{ram}) and selects the circuit for the air intended for the cabin to follow, in order to bring its temperature to the value requested by the crew in the cockpit (\cref{fig_320_ecs}).

\textbf{Circuit A: heating in cold weather}

In circuit A, the air intended for the cabin is simply expanded in a valve (figure \ref{fig_soupape}) before being inserted into the cabin.

	\begin{figure}
		\begin{center}
			\includegraphics[height=.2\textwidth]{outflow_valve_1}
			\includegraphics[height=.2\textwidth]{outflow_valve_2}
		\end{center}
		\supercaption{Airflow regulation valve of an \textsc{ecs} intended for a Comac C919.}{Photos \wcfile{Regulation valve of environmental control system of C919 (1)}{1} \& \wcfile{Regulation valve of environmental control system of C919 (2)}{2} \cczero \oc}
		\label{fig_soupape}
	\end{figure}

	In the valve, the pressure drops sharply and the specific volume increases; however, no work or heat transfer is performed. This is a process known as a "Joule \& Gay-Lussac expansion" (see \chaprefp{ch_principe_de_joule}). The process is entirely irreversible.

	When the airplane is on the ground in cold weather conditions (\SI{-35}{\degreeCelsius}, \SI{1}{\bar}):

	\begin{enumerate}
		\item What is the \emph{maximum} temperature of the air that the packaging unit can supply to the cabin?\\ (for this, we will fully close the outside air \textsc{ram} circuit).
		\item Represent the evolution on a pressure-volume diagram, in a qualitative way.
		\item What is the minimum flow rate of outside air required to circulate in the \textsc{ram} circuit in order to bring \SI[per-mode=symbol]{0.5}{\kilogram\per\second} of conditioned air at \SI{24}{\degreeCelsius} into the cabin?
		\item Draw the evolution that the conditioned air would undergo on the pressure-volume diagram above.
	\end{enumerate}

	\begin{figure}
		\begin{center}
			\includegraphics[height=.35\textwidth]{ram_pack_air_intakes_747}%\vspace{-0.5cm}%handmade
		\end{center}
		\supercaption{The air intakes of the \textsc{ram} circuit at the wing box of a Boeing 747-8I.}{\wcfile{Air intakes of RAM air for environmental control systems of Boeing 747-8I}{Photo} \ccbysa \olivier}
		\label{fig_747_ram_intake}
	\end{figure}

	\begin{figure}
		\begin{center}
			\includegraphics[width=.8\textwidth]{pack_sukhoi}%\vspace{-0.5cm}%handmade
		\end{center}
		\supercaption{Packaging unit positioned at the wing root of a Sukhoi SuperJet SSJ100}{\wcfile{Air conditioning systems of a Sukhoi Superjet}{Photo} \ccbysa A.Katranzhi}
		\label{fig_pack_ssj}
	\end{figure}

\textbf{Circuit B: cabin air conditioning in moderate weather}

In practice, it is possible to lower the temperature of the air intended for the cabin with a much lower \textsc{ram} air flow. For this reason, when cooling needs are significant, the air intended for the cabin goes through circuit B. It is then expanded using a turbine to the cabin pressure (\SI{1}{\bar}). We assume that the turbine is ideal (reversible adiabatic expansion).

	When the external conditions are \SI{20}{\degreeCelsius}, \SI{1}{\bar}:

	\begin{enumerate}
	\shift{4}
		\item At what temperature will the air enter the cabin if the \textsc{ram} circuit is closed?
		\item How much energy will be extracted from the air by the turbine?
		\item Draw the evolution followed by the air on a pressure-volume diagram, qualitatively.
		\item What is the \emph{minimum} temperature to which the circuit can bring the air intended for the cabin?
		\item How much energy will be extracted from the air by the turbine in that case?
		\item Draw the evolution on the pressure-volume diagram above.
	\end{enumerate}

\textbf{Circuit C: cabin air conditioning in hot weather}

When the aircraft is on the ground in very hot weather conditions (\SI{45}{\degreeCelsius}, \SI{1}{\bar}), the air intended for the cabin goes through circuit C.

Upon passing through the heat exchanger, its temperature only drops to \SI{217}{\degreeCelsius}.

It is then compressed in a compressor (reversible adiabatic process) to \SI{20}{\bar}.

It then passes again through a heat exchanger crossed by the \textsc{ram} air circuit.

Finally, it is expanded in a turbine (in practice, this is the turbine used in circuit B) down to atmospheric pressure, and then fed into the cabin. We model the expansion as a reversible adiabatic process.

	\begin{enumerate}
	\shift{10}
		\item Represent the path followed by the air through the packaging unit and schematically show the evolution on a pressure-volume diagram.
		\item At what temperature should the air be brought to in the second heat exchanger, before expansion, to obtain an air flow at \SI{5}{\degreeCelsius} in the cabin?
		\item What is the mechanical energy that the packaging unit receives or provides to operate in this case?
	\end{enumerate}

\textbf{Conclusion}

\begin{enumerate}
\shift{13}
	\item What is the \textsc{cop} of the heating system generated with circuit A in question 3?
	\item What is the \textsc{cop} of the air conditioning performed with circuit C in question 12?
\end{enumerate}

\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=\textwidth]{320_panel}
		\includegraphics[width=\textwidth]{320_efis}
	\end{center}
	\supercaption{Control interface of the \textsc{ecs} in the center of the upper panel of the cockpit of an Airbus A320 and the corresponding \textsc{efis} display panel.}{\wcfile{Overhead panel of an Airbus A320 during cruise}{Photo} \ccbysa \olivier}
	\label{fig_320_ecs}
\end{figure}
\exerciseend

\exercisesolutionpage
\titreresultats

\begin{description}[itemsep=2em]
	\item [\ref{exo_efficacite_moteur}]
			\tab 1) $\dot Q_\inn = \frac{-\dot W_\net}{\eta_\text{engine}} = \frac{-(\num{60e3})}{\num{0,4}} = \SI{+150}{\kilo\watt}$; thus $\dot m = \frac{\dot Q_\inn}{c_\text{carburant}} = \SI{15,4}{\kilogram\per\hour}$ (about 12 liters per hour);
			\tab 2) $\dot Q_\out = -\dot Q_\inn - \dot W_\net = \SI{-90}{\kilo\watt}$.
	\item [\ref{exo_efficacite_refrigerateur}]
			\tab $W_\net = \frac{Q_\inn}{\eta_\text{refrigeration}} = \SI{+83,3}{\kilo\joule}$; $Q_\out = -Q_\inn - W_\net = \SI{-183,3}{\kilo\joule}$.
	\item [\ref{exo_efficacite_thermopompe}]
			\tab $\dot W_\net = \frac{-\dot Q_\out}{\eta_\text{heat pump}} = \frac{-(\num{-4e3})}{\num{3,1}} = \SI{+1,29}{\kilo\watt}$; $\dot Q_\inn = -\dot W_\net - \dot Q_\out = \SI{+2,71}{\kilo\watt}$.
	\item [\ref{exo_bieres}]
			\tab 1) If we assume that the density of the liquid is equal to that of liquid water ($\rho_\text{liquid} = \SI{e3}{\kilogram\per\metre\cubed}$), the heat $Q_\inn$ absorbed by the refrigerator is $Q_\inn
				= - Q_\text{glass} - Q_\text{liquid} - Q_\text{walls}
				= - n_\text{bottles} (m_\text{glass} c_\text{glass} + m_\text{liquid} c_\text{liquid}) (\Delta T)_\text{packs} - \dot Q_\text{walls} \Delta t
				= - \num{60} (\num{0,172} \times \num{0,75e3} + \num{0,25} \times \num{4,2e3}) \times (5 - \num{19}) - \num{10} \times 4 \times \num{3600}
				= \SI{+1134,4}{\kilo\joule}$. Thus, $W_\net = \frac{Q_\inn}{\eta_\text{refrigeration}} = \SI{+1194,1}{\kilo\joule}$.
			\tab 2) $W_\net = \SI{+1194,1}{\kilo\joule} = \SI{+1194,1}{\kilo\watt\second} = \SI{+0,332}{\kilo\watt\hour}$. Thus the cost adds up to~\SI{0,05}{\euroo} (!).
			\tab 3) The room will be heated up by the heat rejection $Q_\out = -Q_\inn - W_\net = \SI{-2,329}{\mega\joule}$.
			\tab 4)Opening the door only increases the heat $Q_\inn$ that needs to be extracted from the cold chamber, which will consequently increase $Q_\out$ and the warming of the room (with a net power $\dot Q_\net = \dot Q_\inn + \dot Q_\out = -\dot W_\net$).
	\item [\ref{exo_fonctionnement_thermopompe}]
			\tab See \chaprefp{ch_principe_fonctionnement_refrigerateur}, and in particular figures~\ref{fig_refrigerateur_climatiseur_thermopompe_so}, \ref{fig_principe_du_refrigerateur_soupape} and \ref{fig_agencement_thermopompe}.
	\item [\ref{exo_algebre_rendement_climatiseur}]
			\tab $ \eta_\text{conditioner}
			\equiv \left|\frac{\dot Q_\inn}{\dot W_\net}\right|
			= \frac{\dot Q_\inn}{\dot W_\net}
			= \frac{\dot Q_\inn}{-\dot Q_\inn - \dot Q_\out}
			= \frac{1}{-1 - \frac{\dot Q_\out}{\dot Q_\inn}}$.
			Now, by definition $\dot Q_\out < 0$ et $\dot Q_\inn > 0$; ainsi $\frac{\dot Q_\out}{\dot Q_\inn} = -\left|\frac{\dot Q_\out}{\dot Q_\inn}\right|$.
			We thus have $\eta_\text{conditioner} = \frac{1}{-1 + \left|\frac{\dot Q_\out}{\dot Q_\inn}\right|}$. It is now your turn, with equations~\ref{eq_rendement_moteur_qin_qout} et~\ref{eq_rendement_thermopompe_qin_qout}!
	\item [\ref{exo_refrigeration_soufflerie}]
			\tab The \SI{50}{\mega\watt} expended by the fan are entirely dissipated as friction in the wind tunnel, and thus converted into heat that must be removed if we want to maintain a constant temperature. Therefore, $\dot W_\net = \frac{\dot Q_\inn}{\eta_\text{refrigeration}} = \SI{+62.5}{\mega\watt}$ (quite a refrigerator…). It follows that $\dot Q_\text{out} = -\dot Q_\inn - \dot W_\net = \SI{-112.5}{\mega\watt}$.
	\item [\ref{exo_generatrice_electricite_turbine}]
			\includegraphics[height=\solutiondiagramwidth]{exo_sol_pv_turbomoteur_1}
			\includegraphics[height=\solutiondiagramwidth]{exo_sol_pv_turbomoteur_2}
			\tab 2) With equation~\ref{eq_isentropique_horrible2}, $T_\B = T_\A \left(\frac{p_\B}{p_\B}\right)^{\frac{\gamma - 1}{\gamma}} = \SI{774,7}{\kelvin}$;
			\tab 3) With equations~\ref{eq_grande_sfee_deltas_h} and \ref{eq_h=cpT}, $\dot W_\fromatob = \dot m c_p (T_\B - T_\A) = \SI{+242}{\kilo\watt}$;
			\tab 4) With equation~\ref{eq_isentropique_horrible2}, $T_\D = T_\C \left(\frac{p_\D}{p_\C}\right)^{\frac{\gamma - 1}{\gamma}} = T_\C \left(\frac{p_\B}{p_\A}\right)^{-\frac{\gamma - 1}{\gamma}} = \SI{481,8}{\kelvin}$. Therefore, the rejected air must lose $\dot Q_\fromdtoa = c_p \Delta T = \SI{-94,8}{\kilo\watt}$ in order to return to its initial state (\S\ref{ch_construction_cycle});
			\tab 5) $\eta_\text{engine} \equiv \left|\frac{\dot W_\net}{\dot Q_\inn} \right| = \left|\frac{\dot W_\fromatob + \dot W_\fromctod}{\dot Q_\frombtoc} \right| = \SI{62,3}{\percent} $ (quite honorable, only attainable with perfect turbine and compressor);
			\tab 6) With a real compressor $\dot W_{\fromatob_2} > \dot W_\fromatob$ et $T_{\B_2} > T_\B$.
						It follows that if $T_\C$ is kept constant, $\dot Q_{\frombtoc_2} < \dot Q_\frombtoc$.
						Nevertheless, we still have $\dot W_{\fromctod_2} < \dot W_\fromctod$ and $T_{\D_2} > T_\D$ in the turbine. The power $\dot W_\net$ decreases, the rejection $\dot Q_\out$ increases. We will see in \chapterseven that the efficiency also decreases.\\
					\textit{Note: We had already studied this engine in exercises \ref{exo_turbomoteur_puissances_spe} \pagerefp{exo_turbomoteur_puissances_spe} and especially \ref{exo_generatrice_electrique} \pagerefp{exo_generatrice_electrique}. Our ability to analyze and quantify performance improves each time…}
	\item [\ref{exo_centrale_vapeur_cycle}]
			\includegraphics[height=\solutiondiagramwidth]{exo_sol_tv_moteur_vapeur}
			\includegraphics[height=\solutiondiagramwidth]{exo_sol_pv_moteur_vapeur}
			\tab 1) To assist in constructing these diagrams, one can review figures~\ref{fig_t-v_eau} \pagerefp{fig_t-v_eau} and~\ref{fig_p-v_eau} \pagerefp{fig_p-v_eau}, as well as section \chaprefp{ch_lv_evolutions_elementaires}.
			\tab 2)	$h_\A = h_{L \SI{0,5}{\bar}}$;
						$h_\B = h_\A + \int_\A^\B v \diff p = h_\A + v_L \Delta p $ (\ref{eq_lv_so_travail_isochore} \& \ref{eq_lv_so_chaleur_isochore} with $q_\fromatob = 0$);
						$h_\C = h_{\SI{650}{\degreeCelsius} \& \SI{4}{\mega\pascal}}$;
						$h_\C = h_{\SI{110}{\degreeCelsius} \& \SI{0,05}{\mega\pascal}}$;
						With these data, we easily calculate $\eta_\text{engine} \equiv \left|\frac{w_\net}{q_\inn}\right| = -\frac{w_\net}{q_\inn} = -\frac{w_\text{turbine} + w_\text{pump}}{q_\text{boiler}} = -\frac{(h_\D - h_\C) + (h_\B - h_\A)}{h_\C - h_\B} = \SI{31,47}{\percent}$ (interesting insofar as any type of fuel can be used).
			\tab 3)In this case, the pump would (almost) force the vapor to follow the path from $\D$ to $\C$, bringing it back to a temperature of \SI{650}{\degreeCelsius} and making it impossible to add heat in the boiler. We would then effectively have a machine made up of a turbine and a pump exchanging water: it would be impossible to generate work this way…
	\item [\ref{exo_refrigeration_supermache}]
			\tab In a supermarket, both in spring and autumn, the energy savings provided by the new refrigerators amount to \SI{43.6}{\kilo\watt}.\\
			In summer, the heat to be absorbed by the air conditioners is reduced. Therefore, an additional saving of \SI{48.5}{\kilo\watt} in electricity can be added at the level of the air conditioners.\\
			In winter, the heat required by the heat pumps is increased. Therefore, an additional expense of \SI{10.9}{\kilo\watt} must be added at the level of the heat pumps.\\
			In the end, this represents an annual savings of \SI{1,672e12}{\joule}, or \SI{69,7}{\kilo\euroo} per supermarket, which must be compared to the required investment and the costs incurred (including the call for your expertise…).
	\item [\ref{exo_fonctionnement_climatiseur}]
				\includegraphics[height=\solutiondiagramwidth]{exo_sol_pv_climatiseur}
				\includegraphics[height=\solutiondiagramwidth]{exo_sol_pv_thermopompe}
				\tab 2)	With equation~\ref{eq_isentropique_horrible2}, $\frac{p_\B }{p_\A} = \left(\frac{T_\B}{T_\A}\right)^{\frac{\gamma}{\gamma - 1}} = \num{1,565}$;
				\tab 3) Idem, expansion C $\to$ D is reversible adiabatic, $T_\D = T_\C \left(\frac{p_\D}{p_\C}\right)^{\frac{\gamma - 1}{\gamma}} = T_\C \left(\frac{p_\A}{p_\B}\right)^{\frac{\gamma - 1}{\gamma}} = T_\C \frac{T_\A}{T_\B} = \SI{275,6}{\kelvin}$ or \SI{2,4}{\degreeCelsius};
				\tab 4) With equations \ref{eq_petite_sfee_deltas_h} and \ref{eq_h=cpT}, $w_\inn = \SI{+40,2}{\kilo\joule\per\kilogram}$;
						$q_\out = \SI{-20,1}{\kilo\joule\per\kilogram}$;
						$w_\out = \SI{+37,76}{\kilo\joule\per\kilogram}$;
						$q_\inn = \SI{+17,6}{\kilo\joule\per\kilogram}$;
						Thus with \cref{def_rendement_climatiseur_refrigerateur}, $\eta_\text{conditioner} = \num{7,213}$;
				\tab 5) We want to obtain in 2 (outlet in cabin) $\dot m_\text{internal air} = \frac{\dot V_2}{v_2} = \frac{\dot V_2 p_2}{R T_2} = \SI{0,305}{\kilogram\per\second}$.
						We must therefore remove a power $\dot Q_\text{internal air} = \dot m_\text{internal air} c_p (T_2 - T_1) = \SI{-5,51}{\kilo\watt}$ (\ref{eq_grande_sfee_deltas_h} \& \ref{eq_h=cpT}) from the internal air.
						The air conditioning unit will therefore require $\dot W_\net = \frac{-\dot Q_\text{internal air}}{\eta_\text{conditioner}} = \SI{765}{\watt}$.
				\tab 6) To minimize $\dot m_\text{external air}$, we need to maximize its exit temperature $T_4$. However, we necessarily have $T_4 \leq T_\B$, otherwise the heat transfer would occur in the wrong direction. Thus, $\dot m_\text{external air min.} = \frac{-\dot Q_\text{out air conditioner}}{c_p (T_{4 \text{max.}} - T_3)} = -\frac{-\dot Q_\inn - \dot W_\net}{c_p (T_{4 \text{max.}} - T_3)} = \SI{0.208}{\kilogram\per\second}$ (theoretical minimum).
				\tab 7) In principle, it is sufficient to reverse the positions of the compressor and the turbine. In practice, the temperature ranges will also need to be shifted to allow heat absorption in cold weather.
	\item[\ref{exo_pack_conditonnement}]
			\includegraphics[width=0.9\textwidth]{exo_sol_circuit_acm}
			\includegraphics[height=\solutiondiagramwidth]{exo_sol_pv_pack_1}
			\includegraphics[height=\solutiondiagramwidth]{exo_sol_pv_pack_2}
			\includegraphics[width=\solutiondiagramwidth]{exo_sol_pv_pack_3}
			\tab\tab 1) If the \textsc{ram} circuit is closed, then $T_{3\A} = T_{2\A}$; and since $T_{4\A} = T_{3\A}$ (\S\ref{ch_principe_de_joule}) we obtain $T_{4\A}
			= T_{1\A} \left(\frac{p_{2\B}}{p_{1\B}}\right)^{\frac{\gamma-1}{\gamma}}
			= \SI{377,19}{\kelvin} = \SI{104,1}{\degreeCelsius}$.
			\tab 3) In the \textsc{ram} exchanger we have
				$\dot Q_\text{cabin air} = - \dot Q_\text{\text{ram} air}$ so
				$\dot m_\text{cabin air} q_\text{cabin air} =  -\dot m_\text{\text{ram} air} q_\text{\text{ram} air}$ or 
				$\dot m_\text{cabin air} c_p(T_{3\A} - T_{2\A}) = -\dot m_\text{\text{ram} air} c_p(T_\text{\textsc{ram} outlet} - T_\text{\textsc{ram} inlet})$. Thus, $m_\text{\text{ram} air}$ is minimized when $T_\text{\textsc{ram} outlet}$ is maximized; now we necessarily have $T_\text{\textsc{ram} outlet} \leq T_{2\C}$. Thus,
				$\dot m_\text{\text{ram} air min.} = -\dot m_\text{cabin air} \frac{T_{3\A} - T_{2\A}}{T_{2\C} - T_\text{\textsc{ram} inlet}} = \SI{0,29}{\kilogram\per\second}$ (so about \SI{200}{\liter\per\second} at the inlet).
			\tab 5) If the \textsc{ram} circuit is closed, then $T_{3\B} = T_{2\B}$; we have $T_{4\B}
			= T_{3\B} \left(\frac{p_{4\B}}{p_{3\B}}\right)^{\frac{\gamma-1}{\gamma}}
			= T_{2\B} \left(\frac{p_{1\B}}{p_{2\B}}\right)^{\frac{\gamma-1}{\gamma}}
			= T_{1\B} = \SI{293,15}{\kelvin} = \SI{20}{\degreeCelsius}$.
			\tab 6) $w_\text{turbine B} = c_p (T_{4\B} - T_{3\B}) = -w_\text{compression B} = \SI{-172}{\kilo\joule\per\kilogram}$
			\tab 8) We obtain $T_{4\B \text{min.}}$ when $T_{3\B} = T_{3\B \text{min.}} = T_\text{extérieur}$. Then $T_{4\B \text{min.}} = T_{3\B \text{min.}} \left(\frac{p_{4\B}}{p_{3\B}}\right)^{\frac{\gamma-1}{\gamma}} = \SI{185,1}{\kelvin} = \SI{-88,1}{\degreeCelsius}$ (a purely theoretical result, of course);
			\tab 9) It decreases : $w_\text{turbine B2} = c_p (T_{4\B \text{min.}} - T_{3\B \text{min.}}) = \SI{-108,6}{\kilo\joule\per\kilogram}$.
			\tab 12) $T_{5\C} = T_{6\C} \left(\frac{p_{5\B}}{p_{6\B}}\right)^{\frac{\gamma-1}{\gamma}} = \SI{654,6}{\kelvin} = \SI{381,5}{\degreeCelsius}$.
			\tab 13) We first calculate $T_{4\C} = T_{3\C} \left(\frac{p_{4\C}}{p_{3\C}}\right)^{\frac{\gamma-1}{\gamma}} = \SI{728,4}{\kelvin}$. In the pack, the work transfers are $w_\text{pack}
			= w_\text{pack compressor} + w_\text{pack turbine}
			= c_p(T_{4\C} - T_{3\C}) + c_p(T_{6\C} - T_{5\C})
			= \SI{-138,9}{\kilo\joule\per\kilogram}$;
			Thus, the air performs net work in the pack, which continuously receives energy in the form of pneumatic (mechanical) power.
			\tab 14) In order to calculate these \textsc{cop}, the cycles must be completed by returning the air from the cabin condition back to the inlet condition (\S\ref{ch_construction_cycle}).
			In question~3 we have $\eta_\text{heat pump}
			= \left|\frac{q_\out}{w_\inn}\right|
			= -\frac{c_p (T_{4\A} - T_{1\A})}{c_p (T_{2\A}-T_{1\A})}
			= \num{0,424}$ (a rare application where a \textsc{cop} less than~\SI{100}{\percent} is acceptable).
			\tab\tab 15) In question~12 we have $\eta_\text{conditioner} = \left|\frac{q_\inn}{w_\inn}\right| = \frac{c_p(T_{1\C} - T_{6\C})}{c_p (T_{2\C} - T_{1\C} + T_{4\C} - T_{3\C} + T_{6\C} - T_{5\C})} = \num{0,842} $.\\
			In practice, however, the net work done by the air in the pack is not recovered: it is dissipated through friction into the \textsc{ram} air.We thus have $w_\net = c_p(T_{2\C} - T_{1\C})$ and the \textsc{cop} is decreased.
			\tab A few final comments : 1) In reality, adiabatic processes are not reversible, which further reduces the efficiencies calculated here. 2) The low values of these efficiencies result from compromises made to reduce the size, complexity, and especially the weight of the onboard systems. In this application, the available mechanical power is high, pneumatic energy is widely available, and any excess in volume or weight has disproportionate consequences. 3) In the most recent aircraft (referred to as \textit{more electric}), such as the \wfd{Boeing 787}{B787} and the \wfd{Airbus A350 XWB}{A350}, the packs are now powered by electricity rather than using the same air intended for the cabin.
\end{description}
