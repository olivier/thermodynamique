\index{cycle!thermodynamic|(textbf}
\section{Graphical Conventions}
\label{ch_conventions_graphiques}

	We start by agreeing on some graphical and notation conventions, which are summarized in \cref{fig_graphical_conventions_net_work}.

	\begin{figure}[ht!]
		\begin{center}
			\includegraphics[width=\textwidth]{conventions_graphiques_travail_net}
		\end{center}
		\supercaption{New graphical and notation conventions for energy transfers.
			The white arrows are oriented according to the physical direction of the transfers; The algebraic sum of all the work received and carried out is represented by a single transfer named \vocab[work!net]{net work}.}{\wcfile{Thermodynamic systems graphic conventions.svg}{Diagram} \cczero \oc}
		\label{fig_graphical_conventions_net_work}
	\end{figure}

	We use large white arrows to represent \emph{the physical direction of transfers}. We do not change our sign convention (transfers are positive when directed toward the system and negative when coming from it), but only the graphical convention for their orientation, in order to make the visualization of transfers in machines more intuitive.

	\index{power!net}\index{energy@energy!net transfer}
	The algebraic sum of the work received $W_\inn$ and provided $W_\out$ by a machine is named the \vocab[work!net]{net work} $W_\net$. The net work can be positive (received by the machine from external sources) or negative (provided by the machine to external sources), depending on the application.
	\begin{IEEEeqnarray}{rCl}
		W_\net 			& \equiv & W_\inn + W_\out 					\nonumber \\
		\dot W_\net 	& \equiv & \dot W_\inn + \dot W_\out 	\nonumber \\
		w_\net 			& \equiv & w_\inn + w_\out
	\label{def_travail_net}
	\end{IEEEeqnarray}

	We define the \vocab[heat!net]{net heat} in the same way:
	\begin{IEEEeqnarray}{rCl}
		Q_\net 			& \equiv & Q_\inn + Q_\out					\nonumber \\
		\dot Q_\net 	& \equiv & \dot Q_\inn + \dot Q_\out	\nonumber \\
		q_\net 			& \equiv & q_\inn + q_\out
	\end{IEEEeqnarray}

	Therefore, for example, a car engine receives a positive net heat and produces a negative net work.


\section{Transforming Heat and Work}


	\subsection{Building thermodynamic cycles}
	\label{ch_construction_cycle}\index{cycle!thermodynamic}

	We want to compare different ways of transforming work and heat. For these comparisons to be valid, we must always take into account \emph{all} the transformations undergone by the fluid until it returns to its initial state.

		For example, it is easy to cool a room with a compressed air bottle (simply make the fluid work during its expansion to lower its temperature); but if we want to continuously cool the room, then we also need to consider the energy required to \emph{return} the air to the bottle, at its initial pressure and temperature, at the end of the process.

		A second example is that of a car engine, which releases heat carried by the exhaust gases. To account for this lost energy, we count the heat that would need to be removed from the gases in order to bring them back to the engine's inlet temperature. This imaginary cooling takes place outside the engine in practice, but from a thermodynamic standpoint, it is an integral part of the energy transformation process.

		Therefore, every time we analyze the operation of a heat engine, we will make sure to continue the fluid's evolution until it returns to its initial state (same temperature, same pressure, same internal energy, etc.). We then say that it has completed a \vocab[cycle!thermodynamic]{thermodynamic cycle} (\S\ref{ch_premier_principe_sf}).


	\subsection{Producing work with heat}
	\label{ch_engine_operation_principle}\index{engine!operation principle}

		Let us start by compressing a fluid: we increase its pressure and reduce its specific volume, which requires a certain amount of work. After that, we heat up this fluid: its pressure and volume tend to increase. By expanding the fluid back to its initial pressure, we will recover more work than what we initially invested. Finally, in order to bring the fluid back to its initial state, it needs to be cooled down.

		In the end, the fluid has expended more work when it was expended than it received when it was compressed. Over a cycle, it will thus have \emph{produced} work and absorbed (more precisely, \emph{transformed}) heat. This is the operating principle of an engine.

		There are infinitely many possible cycles to perform this transformation, but they all involve at least four energy transfers: compression, heating, expansion, and cooling. We can separate these evolutions in space, as shown in \cref{fig_engine_thermodynamic_cycle_mv}, or in time, as illustrated in \cref{fig_engine_thermodynamic_cycle_et}. Depending on technological and practical constraints, some of these transfers can be performed simultaneously.

		\begin{figure}[ht!]
			\begin{center}
				\includegraphics[width=\textwidth]{moteur_so}
			\end{center}
			\supercaption{Engine thermodynamic cycle.
				The fluid absorbs heat supplied at high temperature $T_H$. The compression power is lower than the expansion power: the net power in the form of work $\dot W_\net = \dot W_\inn + \dot W_\out$ is negative.}{\wcfile{Open system engine thermodynamic cycle.svg}{Diagram} \cczero \oc}
			\label{fig_engine_thermodynamic_cycle_et}
		\end{figure}

		\begin{figure}[ht!]
			\begin{center}
				\includegraphics[width=\textwidth]{moteur_sf}
			\end{center}
			\supercaption{Engine thermodynamic cycle carried out by separating the stages in time (rather than in space as shown in \cref{fig_engine_thermodynamic_cycle_et}). The fluid is heated by a high-temperature heat source $T_H$. The net work $W_\net = W_\inn + W_\out$ is negative.}{\wcfile{Closed system engine thermodynamic cycle.svg}{Diagram} \cczero \oc}
			\label{fig_engine_thermodynamic_cycle_mv}
		\end{figure}

		It is possible to mechanically link the sections that consume and provide energy in the form of work. In the case where the fluid circulates continuously, the compressor and the turbine can be connected by the same shaft, as shown in \cref{fig_engine_thermodynamic_cycle_axe_et}. In the case where the processes are separated in time, such as in an internal combustion engine, the processes can be linked by performing multiple offset cycles simultaneously (with multiple cylinders) or by storing energy in a flywheel. The engine then does not receive external work, and the resulting output is a power $\dot W_\net$.
		
		\begin{figure}[ht!]
			\begin{center}
				\includegraphics[width=\textwidth]{moteur_so_travail_net}
			\end{center}
			\supercaption{Engine thermodynamic cycle, in which the compressor and the turbine are mechanically coupled. Since the turbine provides a power $\dot W_\out$ greater than that consumed by the compressor ($\dot W_\inn$), it is able to not only drive it but also to provide an excess $\dot W_\net$ sent outside of the engine.}{\wcfile{Coupled open system engine thermodynamic cycle.svg}{Diagram} \cczero \oc}
			\label{fig_engine_thermodynamic_cycle_axe_et}
		\end{figure}


	\subsection{Extracting heat with work}
	\label{ch_principe_fonctionnement_refrigerateur}\index{refrigerator!operation principle|(textbf}\index{heat pump!operation principle|(textbf}\index{air conditioner!operation principle|(textbf}\index{heat!transfer to higher temperature}\index{refrigeration cycle (or air conditioning)}

		When work is provided to a fluid, its temperature tends to rise (with a brief exception for liquid/vapors between their saturation points) and it can thus provide heat to a body that was initially at a higher temperature ("hotter") than itself.

			Conversely, when a fluid is expanded, its temperature decreases and it can thus absorb heat from a body that was initially "colder" than itself.

			By performing these steps one after the other, we obtain a \vocab[refrigeration cycle (or air conditioning)]{refrigeration cycle}: a machine capable of extracting heat at low temperature and rejecting it at high temperature. Such a cycle is depicted in \cref{fig_refrigerateur_climatiseur_thermopompe_so} (stages separated in space) and \ref{fig_refrigerateur_climatiseur_thermopompe_sf} (stages separated in time).

			\begin{figure}[ht!]
				\begin{center}
					\includegraphics[width=\textwidth]{refrigerateur_climatiseur_thermopompe_so}
				\end{center}
				\supercaption{A refrigeration thermodynamic cycle, for use in refrigerators, air conditioners, and heat pumps.\\
					A power $\dot Q_\inn$ in the form of heat is absorbed at low temperature (the fluid is then heated) while a power $\dot Q_\out$ is rejected at high temperature (the fluid is then cooled).}{\wcfile{Open system heat pump refrigeration air conditioning thermodynamic cycle.svg}{Diagram} \cczero \oc}
				\label{fig_refrigerateur_climatiseur_thermopompe_sf}
			\end{figure}

			\begin{figure}[ht!]
				\begin{center}
					\includegraphics[width=\textwidth]{refrigerateur_climatiseur_thermopompe_sf}
				\end{center}
				\supercaption{A refrigeration cycle carried out by separating the stages in time (rather than in space as shown in \cref{fig_refrigerateur_climatiseur_thermopompe_sf})}{\wcfile{Closed system heat pump refrigeration air conditioning thermodynamic cycle.svg}{Diagram} \cczero \oc}
				\label{fig_refrigerateur_climatiseur_thermopompe_so}
			\end{figure}

			Careful examination of these two figures will reveal a major surprise: they are exactly the same arrangement as for an engine! The only difference lies in the operating temperatures. The temperature reached during compression must be \textbf{higher than the high temperature $T_H$}, and the temperature reached during expansion must be \textbf{lower than the low temperature $T_B$}. Unless these conditions are met, the heat transfers will occur in the wrong direction.

			In a refrigeration cycle, the fluid has a larger volume when compressed (after having being heated) than when expanded (after having being cooled): this time compression requires more power than the expansion. The net power $\dot W_\net$ in the form of work is therefore positive, meaning that the machine must be powered by external work.

			In practice in refrigeration systems, a trick is often used to lower the temperature: instead of a turbine, a simple valve (sometimes called a \vocab{throttling valve}) is used. This component without moving parts does not deliver work (therefore increasing the power consumed by the machine), but it is much simpler to manufacture and use. This modification is depicted in \cref{fig_principe_du_refrigerateur_soupape}.

		\begin{figure}[ht!]
				\begin{center}
					\includegraphics[width=\textwidth]{refrigerateur_climatiseur_thermopompe_so_soupape}
				\end{center}
				\supercaption{A Modified refrigeration cycle. When using liquids/vapors, it is possible to avoid extracting work during expansion. The use of a simple valve is sufficient to lower the temperature of the fluid.}{\wcfile{Open system heat pump refrigeration air conditioning thermodynamic cycle throttling valve.svg}{Diagram} \cczero \oc}
				\label{fig_principe_du_refrigerateur_soupape}
			\end{figure}

			\index{Joule's law!Gay-Lussac expansion}\index{Gay-Lussac's law!Joule expansion}\index{work!during irreversibilities}
			The throttle valve, in thermodynamic terms, allows for a completely irreversible expansion, increasing the volume and reducing the pressure without extracting work. If a perfect gas were used, this would have no effect on the temperature (as in the experiments of Joule and Gay-Lussac studied in \chaprefp{ch_principe_de_joule}) and therefore no interest; but when liquids/vapors are used, the throttling expansion is a technologically simple way to lower the temperature.

			\clearpage
			Refrigeration cycles have two main types of applications:
			\begin{description}
				\item[Heat pumps]\index{heat pump} (\cref{fig_agencement_thermopompe}) are arranged to reject heat to a high-temperature body, most often a building;
				\item[Refrigerators and air conditioners]\index{refrigerator}\index{air conditioner} (\cref{fig_refrigerator_air_conditioner_layout}) are arranged to extract heat from a low-temperature body (a cold enclosure).
			\end{description}

		\begin{figure}[ht!]
				\begin{center}
					\includegraphics[width=\textwidth]{agencement_thermopompe}
				\end{center}
				\supercaption{Arrangement of a heat pump. The machine is configured to reject heat inside (where the temperature is higher) which it extracts from outside (where the temperature is lower).}{\wcfile{Heat pump air conditioner indoor outdoor.svg}{Diagram} \cczero \oc}
				\label{fig_agencement_thermopompe}
			\end{figure}

		\begin{figure}[ht!]
				\begin{center}
					\includegraphics[width=\textwidth]{agencement_refrigerateur_climatiseur}
				\end{center}
				\supercaption{Arrangement of a refrigerator or air conditioner. The machine is configured to reject heat outside (where the temperature is higher) which it extracts from the inside (where the temperature is lower). This is exactly the same machine as in \cref{fig_agencement_thermopompe}.}{\wcfile{Heat pump air conditioner indoor outdoor.svg}{Diagram} \cczero \oc}
				\label{fig_refrigerator_air_conditioner_layout}
			\end{figure}

			In these two types of applications, it is exactly the same machine, operating with the same cycle. The only difference concerns the internal/external arrangement of the components: a heat pump is nothing more than a refrigerator positioned to "cool the outside".

			\clearpage
			The similarity between an air conditioner and a heat pump allows these two functions to be performed by a single machine, which is then referred to as \vocab[reversibility!reversible vs. invertible]{reversible} in the industry. In thermodynamics, the word “reversible” has a different meaning, as we will see in \chapterseven, and so we will call this kind of machine \vocab[invertible vs. reversible]{invertible} here. Depending on the needs, the direction of fluid flow is reversed, which causes the inversion of heat transfers. This type of machine is shown in \cref{fig_agencement_thermopompe_climatiseur_inversable}.


\begin{figure}[ht!]
	\begin{center}
		\includegraphics[width=\textwidth]{agencement_thermopompe_climatiseur_inversable}
	\end{center}
	\supercaption{Layout of an invertible (commonly called “reversible”) air conditioner. By rotating both valves 90 degrees counterclockwise, the function changes from a heat pump to an air conditioner.}{\wcfile{Heat pump air conditioner invertible reversible thermodynamic cycle.svg}{Diagram} \cczero \oc}
	\label{fig_agencement_thermopompe_climatiseur_inversable}
\end{figure}


\section{Cycle Efficiency}

	\index{efficiency!relative}
	The \vocab{efficiency}\footnote{Some authors make a distinction between the efficiency $\eta$ defined in~\ref{def_efficacite_machines_thermiques} and a \vocab{relative efficiency} or \vocab{effectiveness} $\Phi \equiv \frac{\eta_\text{real}}{\eta_\text{theoretical}}$ comparing the efficiency achieved in practice with the maximum achievable efficiency by the machine in theory. It is then necessary to carefully define the assumptions associated with the calculation of the maximum efficiency.} $\eta$ of a heat engine compares the useful transfer or transformation it performs with the energy cost it incurs. We will adopt the following principle definition:
	\begin{equation}
		\eta \equiv \left| \frac{\text{useful transfer}}{\text{energy expenditure}} \right|
		\label{def_efficacite_machines_thermiques}
	\end{equation}

	By convention, the efficiency is always expressed as a positive number; thus we use an absolute value in \cref{def_efficacite_machines_thermiques}. For each of the three types of heat engines, we will define and quantify this "useful transfer" and this "energy expenditure".

\subsection{Efficiency of an engine}
\label{ch_rendement_moteur}\index{engine!efficiency of|textbf}\index{efficiency!of an engine|textbf}

	The function of a thermal engine, like those found on board automobiles or in power plants, is to produce work, meaning a negative quantity $\dot W_\net$ (\cref{fig_engine_transfers}). The expense incurred to generate this work is the heat it receives, namely the quantity $\dot Q_\inn$ (usually originating from the combustion of fuel or the fission of atomic nuclei).

	\begin{figure}[ht!]
		\begin{center}
			\vspace{-0.5cm}
			\includegraphics[width=6cm]{efficacite_moteur}
			\vspace{-1cm}
		\end{center}
		\supercaption{Energy transfers associated with an engine.
			We aim to obtain a large transfer $\dot W_\net$ (output) from the transfer $\dot Q_\inn$ (input).
			The rejection $\dot Q_\out$ is undesirable.}{\wcfile{Basic_thermodynamic_engines.svg}{Diagram} \cczero \oc}
		\label{fig_engine_transfers}
	\end{figure}

	According to definition~\ref{def_efficacite_machines_thermiques}, the efficiency $\eta_\text{engine}$ of the thermal engine is therefore:
	\eq{
		\eta_\text{engine} &\equiv& \left| \frac{\dot W_\net}{\dot Q_\inn} \right| \label{def_rendement_moteur}
	}\dontbreakpage%handmade
	\begin{anexample}
		An automobile engine receives a power of~\SI{100}{\kilo\watt} in the form of heat from gasoline combustion; it provides \SI{55}{\kilo\watt} as work at the transmission shaft. What is its efficiency?

			\begin{answer}
				The efficiency is $\eta_\text{engine} = \left| \frac{\dot W_\net}{\dot Q_\inn} \right| = \left| \frac{\num{-55e3}}{\num{+100e3}} \right| = \num{0.55} = \SI{55}{\percent}$.
					\begin{remark} This engine rejects $\dot Q_\out = -\dot W_\net - \dot Q_\inn = -(\num{-55e3}) - \num{100e3}= \SI{-45}{\kilo\watt}$. This heat is largely expelled through the exhaust pipe.\end{remark}
					\begin{remark} It is evident that one must always supply at least as much heat $\dot Q_\inn$ as the engine outputs in work $\dot W_\net$; therefore, the efficiency of an engine will always necessarily be less than~\num{1}.\end{remark}
			\end{answer}
		\end{anexample}

		The net power $\dot W_\net$ in the form of work can be expressed in terms of other energy transfers, as follows:
		\begin{IEEEeqnarray}{rCl}
			\dot W_\net 			& = & \dot W_\inn + \dot W_\out = -\dot Q_\inn - \dot Q_\out	\nonumber \\
			\eta_\text{engine} 	& = & 1 - \left| \frac{\dot Q_\out}{\dot Q_\inn} \right|	\label{eq_rendement_moteur_qin_qout}
		\end{IEEEeqnarray}

		This equation~\ref{eq_rendement_moteur_qin_qout} will be very useful in the next chapter (\chaprefp{ch_efficacite_moteur_carnot}), where we will want to relate the heat transfers $\dot Q_\inn$ and $\dot Q_\out$ to the temperatures at which they occur.\clearpage%handmade


	\subsection{Efficiency of a refrigerator or air conditioner}
	\label{ch_rendement_refrigerateur}\index{air conditioner!efficiency of}\index{refrigerator!efficiency of}\index{efficiency!of an air conditioner|textbf}\index{efficiency!of a refrigerator|textbf}

		The function of a refrigerator or an air conditioner is to extract heat, meaning to generate a power $\dot Q_\inn$ (heat extracted every second from the compartment to be cooled) of positive sign. This transfer (\cref{fig_refrigerator_transfers}) is made possible by supplying work to the refrigerator, $\dot W_\net$, an "expense" that is necessarily positive.

		\begin{figure}[ht!]
			\begin{center}
				\vspace{-0.5cm}
				\includegraphics[width=6cm]{efficacite_refrigerateur_climatiseur}
				\vspace{-1cm}
			\end{center}
			\supercaption{Energy transfers associated with a refrigerator or an air conditioner.
				We aim to obtain a large transfer $\dot Q_\inn$ (output) from the transfer $\dot W_\net$ (input).}{\wcfile{Basic_thermodynamic_engines.svg}{Diagram} \cczero \oc}
			\label{fig_refrigerator_transfers}
		\end{figure}

		According to definition~\ref{def_efficacite_machines_thermiques}, the efficiency (also called the \vocab[coefficient of performance (COP)]{coefficient of performance}\index{COP (coefficient of performance)}, $\textsc{cop}_\text{refrigeration}$) of a refrigerator or an air conditioner is therefore:
		\begin{equation}
			\eta_\text{refrigerator} = \eta _\text{air conditioner} \equiv \left| \frac{\dot Q_\inn}{\dot W_\net} \right|
			\label{def_rendement_climatiseur_refrigerateur}
		\end{equation}\dontbreakpage %handmade

		\begin{anexample}
			A refrigerator consumes an electrical power of \SI{100}{\watt}; it extracts heat from the cold chamber with a power of \SI{120}{\watt}. What is its efficiency?

			\begin{answer}
				The efficiency (or \textsc{cop}) is $\eta_\text{refrigerator} = \left| \frac{\dot Q_\inn}{\dot W_\net} \right| = \left| \frac{\num{+120}}{\num{+100}} \right| = \num{1.2} = \SI{120}{\percent}$.
					\begin{remark} This refrigerator rejects $\dot Q_\out = -\dot W_\net - \dot Q_\inn = \num{-100} - \num{120}= \SI{-220}{\watt}$ outside of the cold chamber (usually, within the building itself).\end{remark}
					\begin{remark} Household refrigerators and air conditioners often have efficiencies greater than 1, but depending on the desired temperatures, the efficiency can indeed be lower.\end{remark}
			\end{answer}
		\end{anexample}\clearpage%handmade

		In order to prepare for the next chapter (\chaprefp{ch_efficacite_refrigerateur_carnot}), and while paying attention to the pitfalls associated with the use of absolute values, we can express this efficiency in terms of heat transfers only:
		\begin{equation}
			\eta_\text{refrigerator} = \eta_\text{air conditioner} = \frac{1}{\left| \frac{\dot Q_\out}{\dot Q_\inn} \right| - 1}
			\label{eq_rendement_refrigerateur_qin_qout}
		\end{equation}


	\subsection{Efficiency of a heat pump}
	\label{ch_rendement_thermopompe}\index{heat pump!efficiency of}\index{efficiency!of a heat pump|textbf}

		A heat pump operates exactly the same way as an air conditioner. Its function is to generate a transfer $\dot Q_\out$ to the "hot" section (usually inside a building). This transfer, represented in \cref{fig_heat_pump_transfers}, is made possible by supplying work to the heat pump, $\dot W_\net$, an "expense" that is necessarily positive.

		\begin{figure}[ht!]
			\begin{center}
				\vspace{-0.5cm}
				\includegraphics[width=6cm]{efficacite_thermopompe}
				\vspace{-1cm}
			\end{center}
			\supercaption{Energy transfers associated with a heat pump.
				We aim to obtain a large transfer $\dot Q_\out$ (output) from the transfer $\dot W_\net$ (input).}{\wcfile{Basic_thermodynamic_engines.svg}{Diagram} \cczero \oc}
			\label{fig_heat_pump_transfers}
		\end{figure}

		\index{COP (coefficient of performance)}
		The efficiency $\eta_\text{heat pump}$ (also known as the \vocab[coefficient of performance (COP)]{coefficient of performance} $\textsc{cop}_\text{heat pump}$) of the heat pump is thus defined as:
		\begin{equation}
			\eta_\text{heat pump} \equiv \left| \frac{\dot Q_\out}{\dot W_\net} \right|
			\label{def_rendement_thermopompe}
		\end{equation}

			\begin{anexample}
				A heat pump consumes an electrical power of \SI{100}{\watt}; it heats the inside of a room with a power of \SI{350}{\watt}. What is its efficiency?

				\begin{answer}
					The efficiency is $\eta_\text{heat pump} = \left| \frac{\dot Q_\out}{\dot W_\net} \right| = \left| \frac{\num{-350}}{\num{+100}} \right| = \num{3.5}$.
						\begin{remark} The heat pump rejects more heat than it consumes in work – that is its whole purpose. If the COP were equal to or less than 1, it would be more economical and much simpler to use an electric heater instead.\end{remark}
				\end{answer}
			\end{anexample}\clearpage%handmade

		Just as we did for the previous sections, we can express this efficiency in terms of heat flows only:
		\begin{equation}
			\eta_\text{heat pump} = \frac{1}{1 - \left| \frac{\dot Q_\inn}{\dot Q_\out} \right|}
			\label{eq_rendement_thermopompe_qin_qout}
		\end{equation}


	\subsection{On the low performance of machines}\index{cycle!thermodynamic, low efficiency}

		\thermoquotebegin{O}
			% Original quote in French:
			% L’on a souvent agité la question de savoir si la puissance motrice de la chaleur est limitée, ou si elle est sans bornes ; si les perfectionnements possibles des machines à feu ont un terme assignable, terme que la nature des choses empêche de dépasser par quelque moyen que ce soit, ou si au contraire ces perfectionnemens sont susceptibles d’une extension indéfinie…
			% Translation to English (2024):
			The question has often been agitated as to whether the motory power of heat is limited, or whether it is boundless; whether the possible improvements of fire engines have an assignable limit, a limit which the nature of things prevents from being surpassed by any means whatsoever, or whether on the contrary these improvements are capable of indefinite extension…
		\thermoquoteend{Sadi Carnot, 1824~\cite{carnot1824}\index{Carnot!Sadi}}{}

		In all the cases we have studied above, for each cycle, we have included an undesirable transfer. In the engine cycle, some of the energy is wasted in the form of heat rejection ($\dot Q_\out$). In refrigeration cycles, work must be supplied ($\dot W_\inn$) to carry out a heat transfer that \textit{a priori} could have seemed "free" ($\dot Q_\out$ then being equal to $\dot Q_\inn$).

		Engineering students will certainly be indignant about the role played by these losses in this chapter -- and about the modest efficiencies achieved by the machines described in the examples. Why are the efficiencies calculated in the examples and in the following exercises so low, and more importantly, how can we design cycles with greater efficiency? We take these worries to our heart, and will take care of them in \chapterseven.
\index{cycle!thermodynamic|)textbf}

