\subsubsection{Simple Evolutions}
\label{exo_evolutions_simples}

    (an exercise simply designed to practice the sign conventions and vocabulary of the chapter)

A mass of \SI{400}{\gram} of water is placed in a sealed reservoir. It undergoes a process during which it receives \SI{50}{\kilo\joule\per\kilogram} of heat, and so its internal energy increases by~\SI{4}{\kilo\joule}.

\begin{enumerate}
	\item Did it receive or provide work, and how much?
\end{enumerate}

This same mass is then provided with \SI{800}{\joule} of work in an adiabatic manner.

\begin{enumerate}
	\shift{1}
	\item What is the change in its specific internal energy?
\end{enumerate}
\exerciseend

\subsubsection{Arbitrary Evolutions of a Gas in the Laboratory}
\label{exo_evolutions_arbitraires}

A mass of \SI{80}{\gram} of helium is contained in a cylinder of \SI{0.04}{\meter\cubed}. The gas is first cooled reversibly at constant pressure until \SI{0.02}{\meter\cubed} and \SI{2}{\bar}; then heated at constant volume until \SI{4}{\bar}.

\begin{enumerate}
	\item Plot the evolution on a pressure-volume diagram.
	\item What is the work provided or received by the gas?
\end{enumerate}
\exerciseend

\subsubsection{Truck Pneumatic Suspension}
\label{exo_pneumatique_camion}

The pneumatic suspension system of a truck trailer can be modeled with an air cylinder. When the trailer is loaded, the piston attached to the trailer descends inside the cylinder that is attached to the wheel axle, compressing the air trapped inside (\cref{fig_truck}).

\begin{figure}
\begin{center}
\includegraphics[height=8cm]{piston_camion}
\end{center}
\supercaption{Schematic modeling of a truck pneumatic suspension system. The piston, at the center, compresses a mass of air (in blue) when the trailer is loaded.}{\wcfile{Piston of a truck pneumatic suspension.svg}{Diagram} \cczero \oc}
\label{fig_truck}
\end{figure}

Initially, the truck is loaded very gradually. The air inside the cylinder neither loses nor receives heat. Its characteristics then change according to the relationship $p v^{\num{1.4}} = \num{5.438e4}$ (in \textsc{si} units).

The compression starts at $p_\A = \SI{2.5}{\bar}$. Once the loading has been completed, the pressure has risen to $p_\B = \SI{10}{\bar}$.

\begin{enumerate}
	\item Represent the gas evolution during loading on a pressure-volume diagram, qualitatively (without showing numerical values).
	\item The work $W$ done by a force $\vec F$ over a displacement $\vec l$ is expressed as
	\begin{equation}
		W \equiv \vec F \cdot \vec l 	\tag{\ref{eq_travail_fdl}}
	\end{equation}
	From this equation, express the work done on a body of fixed mass in terms of its specific volume and internal pressure.
	\item How much energy did the gas receive during loading?
	\item How much energy would the gas give back if the truck were unloaded very gradually?
\end{enumerate}

The truck is unloaded abruptly and the piston rises quickly until the final pressure $p_\C$ drops back to its initial value $p_\C = p_\A = \SI{2.5}{\bar}$.

\begin{enumerate}
	\setcounter{enumi}{3}
	\item Represent the evolution on the previous pressure-volume diagram, qualitatively.
	\item What can be done to bring the gas back to the exact state it was in before loading?
\end{enumerate}
\exerciseend

\subsubsection{Air Compressor}
\label{exo_compresseur_air}

In a small air compressor (\cref{fig_compressor}), a piston compresses a fixed mass of air slowly and without friction. The cylinder is equipped with fins, which allow heat dissipation. Thus, the compression is done at constant internal energy.

We spend \SI{150}{\kilo\joule\per\kilogram} of work in order to compress the air.

\begin{enumerate}
	\item What is the heat transfer during compression?
\end{enumerate}

Before starting the compression, the air is at atmospheric pressure and density (\SI{1}{\bar}; \SI{1.2}{\kilogram\per\metre\cubed}). The diameter of the cylinder is \SI{5}{\centi\metre} and its inner depth is \SI{15}{\centi\metre}.

\begin{enumerate}
	\shift{1}
	\item What is the mass of air included in the cylinder?
\end{enumerate}

During compression, it is observed that pressure and specific volume are related by the relation $pv = k$ (where $k$ is a constant).

\begin{enumerate}
	\shift{2}
	\item To which pressure can the air be compressed at the end of the compression?
\end{enumerate}

\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=8cm]{compresseur_air}
	\end{center}
	\supercaption{Cross-sectional diagram of a small piston air compressor. The intake and exhaust valves are not shown.}{\wcfile{Piston of an air compressor.svg}{Diagram} \ccbysa Christophe Dang Ngoc Chan \& \olivier}
	\label{fig_compressor}
\end{figure}
\exerciseend

\subsubsection{Cycle Of a Gasoline Engine}
\label{exo_cycle_moteur_essence}\index{Otto!cycle}\index{cycle!Otto}

We want to study the operation of a four-cylinder gasoline engine (\cref{fig_exo_pistons}). Like all reciprocating heat engines, it produces work by varying the pressure and volume of small amounts of air trapped in its cylinders. Here, we simplify the details of its operation to reduce it to the ideal case, where all evolutions are reversible.

The engine has a displacement of \SI{1.1}{\liter}; it is equipped with four cylinders of diameter \SI{7}{\centi\metre} and has a compression ratio (ratio between maximum and minimum volumes in a cylinder) of \num{7.9}. Air enters the engine under atmospheric conditions (\SI{1}{\bar}, \SI{0.84}{\metre\cubed\per\kilogram}).

\begin{figure}
	\begin{center}
		\includegraphics[width=6cm, max width=0.8\columnwidth]{pistons_cutaway}
	\end{center}
	\supercaption{Cutaway view of pistons and cylinders of an automobile engine.}{\wcfile{Internal combustion engine pistons of partial cross-sectional view}{Photo} \ccbysa by \wcu{Mj-bird}}
	\label{fig_exo_pistons}
\end{figure}

We can describe a cycle inside a cylinder with the following four steps:
	
\begin{description}\index{top dead center \& bottom dead center}
	\item [From A to B] the air is adiabatically compressed from the bottom dead center to the top dead center. During this evolution, we know that its properties are related by the relation $p \ v^{k_1} = k_2$. At B, the pressure has reached \SI{16.97}{\bar}.
	\item [From B to C] it is heated at constant volume (as if the piston were stationary) until the pressure reaches \SI{75}{\bar}. By measuring temperature, it is found that its specific internal energy increases by \SI{1543.3}{\kilo\joule\per\kilogram}.
	\item [From C to D] the air is adiabatically expanded from the top dead center to the bottom dead center. Its properties are related by the relation $p \ v^{k_1} = k_3$.
	\item [From D to A] it is cooled at constant volume (as if the piston were stationary) until it returns to its state at A.\\
	(In practice, this cooling phase takes place outside the engine, in the atmosphere. However, it can be modeled this way without introducing errors.)
\end{description}

\begin{enumerate}
	\item Represent the cycle followed by the air on a pressure-volume diagram, qualitatively.
	\item {What is the mass of air present in a cylinder?\\
		{\tiny hint: $V_\text{displacement} = 4(V_\text{max. cylinder} - V_\text{min. cylinder}) = 4(V_\text{bottom dead center} - V_\text{top dead center})$}}
	\item What is the specific work received by the air during compression (from A to B)?
	\item What is the specific heat received by the air during combustion (from B to C)?
	\item What is the specific work released by the air during expansion (from C to D)?
	\item What is the specific heat rejected by the air during the cooling phase?
	\item What is the engine efficiency, i.e., the ratio of the net work output during the cycle to the heat input during combustion?
	\item How many cycles per second must be performed for the engine to produce a power of \SI{80}{hp} (\SI{58.84}{\kilo\watt})?
\end{enumerate}
\exerciseend

\subsubsection{Work in a Diesel Engine}
\label{exo_quatre_cylindres}\index{engine!reciprocating}
	
We are studying the operation of a four-cylinder reciprocating engine by modeling its operation in the most favorable case, i.e., with very slow evolutions (perfectly reversible).

\begin{figure}
	\begin{center}
		\includegraphics[width=8cm]{quatre_cylindres}
	\end{center}
	\supercaption{Schematic representation of the operation of a four-cylinder engine. Pistons A and C are going up, and pistons B and D are going down. They are all connected to the same motor axis, not shown here.}{\wcfile{Four pistons in parallel.svg}{Diagram} \ccbysa \olivier}
	\label{fig_quatre_cylindres}
\end{figure}

\index{compression (engine cycle)}\index{engine cycle!compression}\index{intake (engine cycle)}\index{engine cycle!intake}\index{exhaust (engine cycle)}\index{engine cycle!exhaust}\index{expansion (engine cycle)}\index{engine cycle!expansion}\index{four-stroke cycle}
Inside the engine block schematized in \cref{fig_quatre_cylindres}, four pistons linked to the motor axis by a crankshaft (not shown) are in motion. The evolution is different in each cylinder:

\begin{description}
	\item [Cylinder A:] {compression (the air remains trapped in the cylinder).\nopagebreak\\
		The air compression starts at \SI{0.8}{\bar} and its properties are related by the equation $pV^{\num{1.3}} = k_1$.}
	\item [Cylinder B:] {intake.\\
		Air is taken in at constant pressure of \SI{0.8}{\bar}.}
	\item [Cylinder C:] {exhaust.\\
		Air is expelled at constant pressure of \SI{1.1}{\bar}.}
	\item [Cylinder D:] {expansion.\\
		The high-pressure, high-temperature air is trapped in the cylinder; its properties are also related by the equation $pV^{\num{1.3}} = k_2$.}
\end{description}
		
Of course, the role of each cylinder changes twice per revolution. Here, we are studying the work transfers over half an engine cycle.

Even though cylinders B and C are not closed systems, for the purposes of this exercise, we can model their evolutions as if they were, without introducing errors.

The atmospheric conditions are \SI{1}{\bar} and \SI{1.225}{\kilogram\per\metre\cubed}. The engine displacement is \SI{1.5}{\liter} and the compression ratio (i.e., the ratio of the minimum to maximum volumes within each cylinder) is \num{22}.

\begin{enumerate}
	\item Represent the evolution in each of the cylinders on the same pressure-volume diagram, qualitatively.
	\item What is the energy required to move cylinders B and C?
	\item What is the energy received by the gas in cylinder~A?
\end{enumerate}

We want the engine to deliver a power of \SI{30}{\kilo\watt} at a speed of \num{2000} \si{revolutions/min}. Its mechanical losses are around \SI{15}{\percent}.

\begin{enumerate}
	\shift{3}
	\item What is the work that must be developed by cylinder D during expansion?
	\item What should be the pressure generated by combustion in cylinder~D, so that the expansion may release enough energy to operate the engine?
\end{enumerate}
\exerciseend

\subsubsection{Taking Heat From Where it is Cold}
\label{exo_prendre_de_la_chaleur}\index{heat pump}\index{refrigeration cycle (or air conditioning)}

A student is conducting an experiment with a bit of air in a cylinder, controlling its volume with a piston. The goal is to extract heat from the outside, where the temperature is low, to reject it inside the room.

The mass of air trapped in the cylinder is \SI{6e-3}{\kilogram}.

Initially, the air in the cylinder occupies a volume of \SI{0.5}{\liter}. The pressure and temperature are room conditions (\SI{1}{bar}; \SI{18}{\degreeCelsius}).

\begin{description}
	\item[From A to B] The student isolates the cylinder well with a thermal insulator, and slowly expands the gas by increasing its volume to \SI{4.5}{\liter}. We know that during this type of expansion, pressure and volume are related by the equation
	\begin{equation*}
		p v^{\num{1,4}} = k_2
	\end{equation*}
	where $k_2$ is a constant (we will see where this relationship comes from and learn how to calculate the temperature $T_\B$ in \chapterfourshort). 
	
	The gas temperature drops dramatically during the expansion: at B, the thermometer finally reads $T_\B = \SI{121}{\kelvin}$.
	
	\item[From B to C] The student sets the cylinder volume to be constant by mechanically locking the piston, removes the thermal insulator, and places the cylinder outside the building (outside temperature: \SI{-5}{\degreeCelsius}). The gas temperature and pressure slowly rise.
\end{description}

\begin{description}
	\item[From C to A] When the pressure reaches \SI{1}{\bar} precisely, the air temperature is indicated as $T_\C = \SI{262}{\kelvin}$.
	
	The student wishes to return to the initial conditions (by reducing the volume to the initial volume) while keeping the pressure constant at \SI{1}{\bar}.
\end{description}

\begin{enumerate}
	\item Sketch the evolution on a pressure-volume diagram.
	\item {Show that during a reversible evolution of a closed system whose properties are related by a relation of the form $pv^{k_1} = k_2$ (where $k_1 \neq 1$ and $k_2$ are constants), the specific work done is:
				\begin{equation*}
				w_{\A\to\B} = \frac{p_\B v_\B - p_\A v_\A}{k_1 - 1}
				\end{equation*}}
	\item What is the work done by the gas during expansion?
	\item The heat capacity of air when its volume is fixed is \SI{718}{\joule\per\kilogram\per\kelvin}. How much heat was absorbed or released from the outside air?
	\item How much work will be needed to perform the C $\to$ A return at constant pressure?
	\item Over the entire cycle, will the student have done or received work?
	\item {[difficult question]} The return path at constant pressure requires heat transfer. In which direction and to what extent? Why can't (unfortunately) this transfer be  entirely done inside the building?
\end{enumerate}
\exerciseend

\exercisesolutionpage
\titreresultats

\begin{description}[itemsep=2em]
	\item [\ref{exo_evolutions_simples}]
					\tab 1) $W_\fromatob = \Delta U - m q_\fromatob = \SI{-16}{\kilo\joule}$ (so the work is done)
					\tab 2) $\Delta u = q_\frombtoc + w_\frombtoc =  0 + \frac{W_\frombtoc}{m} = \SI{+2}{\kilo\joule\per\kilogram}$.
	\item [\ref{exo_evolutions_arbitraires}]
					\includegraphics[width=\solutiondiagramwidth]{exo_sol_pv_1}
					\tab 2) $W_{\A\to\C} = W_\fromatob + W_\frombtoc = -p_{\text{cst.}} \left[V\right]_{V_\A}^{V_\B } + 0 = \SI{+4}{\kilo\joule}$
	\item [\ref{exo_pneumatique_camion}]
					\includegraphics[width=\solutiondiagramwidth]{exo_sol_pv_camion}
					\tab 2) see \chaprefp{ch_travail_fdl} \& \chaprefp{ch_travail_pdv}
					\tab 3) $v_\A = \SI{0,336}{\metre\cubed\per\kilogram}$ et $v_\B = \SI{0,125}{\metre\cubed\per\kilogram}$ ; so $w_\fromatob = -k \left[\frac{1}{\num{-0,4}} v^{\num{-0,4}}\right]_{v_\A}^{v_\B } = \SI{+102,1}{\kilo\joule\per\kilogram}$.
					\tab 4) $w_\frombtoa =  - w_\fromatob$
					\tab 5) Cooling at constant pressure, for example.
	\item [\ref{exo_compresseur_air}]
					\tab 1) $q_\fromatob = \Delta u - w_\fromatob = - w_\fromatob$.
					\tab 2) $m = \frac{V_\A}{v_\A} = \SI{3,534e-4}{\kilogram}$
					\tab 3) $\frac{v_\B }{v_\A} = \exp \left[-\frac{w_\fromatob}{k}\right]$ ; so $p_\B = p_\A \frac{v_\A}{v_\B } = \SI{6,05}{\bar}$
	\item [\ref{exo_cycle_moteur_essence}]
					\includegraphics[width=\solutiondiagramwidth]{exo_sol_pv_moteur_essence}
					\tab 2) $V_\A = \SI{3,149e-4}{\metre\cubed}$ ; so $m_\A = \frac{V_\A}{v_\A} = \SI{3,748e-4}{\kilogram}$
					\tab 3) $k_1 = \num{1,3699}$ et $k_2 = \SI{7,8753e4}{\usi}$ ; so $w_\fromatob = -k_2 \left[\frac{1}{-k_1 + 1} v^{-k_1 + 1} \right]_{v_\A}^{v_\B } = \SI{+260,7}{\kilo\joule\per\kilogram}$.
					\tab 4) $q_\frombtoc = \Delta u - w_\frombtoc = \Delta u - 0 = \SI{+1543,3}{\kilo\joule\per\kilogram}$
					\tab 5) $w_\fromctod = -k_3 \left[\frac{1}{-k_1 + 1} v^{-k_1 + 1} \right]_{v_\C}^{v_\D} = -\frac{k_3}{k_2} w_\fromatob = -\frac{p_\C}{p_\B } w_\fromatob = \SI{-1152,2}{\kilo\joule\per\kilogram}$
					\tab 6) $q_\fromdtoa = -w_\fromatob - q_\frombtoc - w_\fromctod = \SI{-651,8}{\kilo\joule\per\kilogram}$
					\tab 7) $\eta_{\text{moteur}} = \left|\frac{w_\fromatob + w_\fromctod}{q_\frombtoc}\right| = \SI{57,8}{\percent}$ (very honorable)
					\tab 8) $f = \frac{\dot W_{\text{moteur}}}{m_\A \ (w_\fromatob + w_\fromctod)} = \SI{176,1}{\hertz}$ (176 combustions per second), so approximately \SI{5300}{rotations per minute} with a four-stroke, four-cylinder engine.
	\item [\ref{exo_quatre_cylindres}]
					\includegraphics[width=\solutiondiagramwidth]{exo_sol_pv_quatre_cylindres}
					\tab 2) $W_{\text{cyl. B}} = -p_\B \left[V\right]_{V_\text{min.}}^{V_\text{max.}} = - p_\B \left(\frac{V_\text{displacement}}{4}\right) = \SI{-30}{\joule}$ ; $W_{\text{cyl. C}} =  \SI{+41,3}{\joule}$
					\tab 3) $V_\text{A1} = \frac{22}{21\times4}V_\text{cylindrée} = \SI{3,9286e-4}{\metre\cubed}$ and $V_\text{A2} = \frac{V_\text{A1}}{22} = \SI{1,7857e-5}{\metre\cubed}$. So, $k_1 = \SI{2,9895}{\usi}$, and finally $W_{\text{cyl. A}} = \frac{k_1}{\num{0,3}} \left(V_\text{A2}^{\num{-0,3}} - V_\text{A1}^{\num{-0,3}}\right) = \SI{+160}{\joule}$.
					\tab 4) $\dot n = \SI{2000}{rpm} = \SI{33,3}{rps}$: there are therefore \num{66,7} evolutions per second ($f = \SI{66,7}{\hertz}$). We obtain $W_\text{4 cylinders} = \frac{1}{f} \frac{1}{\eta_\text{mech}} \dot W_\text{engine}$ ; and finally $W_\text{cyl. D} = W_\text{4 cylinders} - W_\text{cyl. A} - W_\text{cyl. B} -W_\text{cyl. C} = \SI{-700,7}{\joule}$.
					\tab 5) We calculate $k_2$ based on $W_\text{cyl. D}$, and we obtain $p_\text{D1} = \SI{194,8}{\bar}$.
	\item [\ref{exo_prendre_de_la_chaleur}]
					\includegraphics[width=\solutiondiagramwidth]{exo_sol_pv_prelever}
					\tab 3) After obtaining $p_\B = \SI{4,61e-2}{\bar}$, we calculate $W_\fromatob = \frac{p_\B V_\B - p_\A V_\A}{k_1 - 1} = \SI{-73,14}{\joule}$.
					\tab 4) $Q_\frombtoc = m c_v \Delta T = \SI{+607,4}{\joule}$
					\tab 5) $W_{\C \to \A} = \SI{+400}{\joule}$ (easy!)
					\tab 6) $W_\text{cycle} = \SI{+326,86}{\joule}$, so work done by the student.
					\tab 7) $Q_{\C \to \A} = \SI{-934,26}{\joule}$
					\tab 8) It is a matter of temperature\ldots
\end{description}

