\begin{boiboiboite}
	\propwater
	\propair
	\isentropics
	\deltaentropy
	\carnotefficiencies
\end{boiboiboite}

\subsubsection{Knowledge Recap Questions}
\label{exo_questions_cours}

	In order to tackle exercises on such a substantial topic, it is necessary to first master the basics!

	\begin{enumerate}
		\item How is the change in the entropy of a body calculated during any real evolution?
		\item Can the entropy of a body be reduced?
		\item What is the difference between specific entropy and heat capacity (both have the same units!)?
		\item What would \cref{fig_experience_creation_entropie_t-s} \pagerefp{fig_experience_creation_entropie_t-s} look like if the heat transfer was continued beyond an infinitesimal amount of heat $\tdiffi Q$ until cup A and water bottle B were at the same temperature?
	\end{enumerate}
\exerciseend

\subsubsection{Elementary variations of an ideal gas}
\label{exo_ts_variations_elementaires}

	Among the evolutions of an ideal gas described in \cref{fig_tsel}, identify the evolution at constant temperature, constant pressure, isentropic, and constant volume (\textit{this exercise is parallel to exercise \ref{exo_retrouver_pv}~\pagerefp{exo_retrouver_pv}}).

	\begin{figure}[htp]%handmade
		\begin{center}
			\includegraphics[width=0.6\columnwidth]{exo_ts_elementaires1}
			\includegraphics[width=0.6\columnwidth]{exo_ts_elementaires2}
		\end{center}
		\supercaption{Reversible elementary evolutions of an ideal gas, represented on a temperature-entropy diagram.}{\wcfile{T-s diagrams elementary evolutions.svg}{Diagram} \cczero \oc}
		\label{fig_tsel}
	\end{figure}
\exerciseend

\subsubsection{Expansion of a liquid/vapor}
\label{exo_detente_liquide_vapeur}

	We have~\SI{10}{\kilogram} of water at~\SI{45}{\bar} and~\SI{600}{\degreeCelsius}.

	\begin{enumerate}
		\item What is the maximum amount of work that can be extracted from this mass of water without supplying heat, if it can expand to~\SI{4}{\bar}?
		\item If the expansion were continued to a lower pressure, at what temperature would the water condense?
		\item Represent the process on a temperature-entropy diagram, qualitatively (i.e., without showing numerical values), also showing the saturation curve.
	\end{enumerate}
\exerciseend

\subsubsection{Heating at constant temperature}
\label{exo_chauffage_isotherme_eau}

	A quantity of heat of~\SI{3000}{\kilo\joule\per\kilogram} is slowly supplied to a mass of water saturated at~\SI{200}{\degreeCelsius}. The temperature is kept constant throughout the process.

What is the amount of work developed by the water during the process? Represent the process on a temperature-entropy diagram, qualitatively, also showing the saturation curve.
\exerciseend

\subsubsection{Temperature-Entropy Diagrams}
\label{exo_diagrammes_ts}

	Represent qualitatively, on a temperature-entropy diagram (including the saturation curve when relevant), the evolutions that we studied in previous chapters:
	\begin{enumerate}
		\item Simple evolutions: exercises \ref{exo_gp_isotherme} and \ref{exo_gp_isobare_isochore} on page \pageref{exo_gp_isobare_isochore}, \ref{exo_cours_vapeur_isotherme} and \ref{exo_generation_vapeur} on page \pageref{exo_generation_vapeur};
		\item Thermodynamic cycles: exercises \ref{exo_moteur_carnot_vapeur} and \ref{exo_moteur_turbine_ideal} on page \pageref{exo_moteur_turbine_ideal}.
	\end{enumerate}
\exerciseend

\subsubsection{Carnot Cycle}
\label{exo_ts_carnot}

	Represent the cycle followed by the fluid inside a heat pump operating according to the Carnot cycle on a temperature-entropy diagram, qualitatively showing also the two heat transfers.

How would the cycle be modified if the compression and expansion remained adiabatic but were not reversible? How would the two heat transfers be affected?
\exerciseend

\subsubsection{Steam Turbine}
\label{exo_turbine_vapeur_isentropique}

	In the engine room of a large ship (\cref{fig_turbine_uss_hornet}), a steam flow rate of \SI{250}{\tonne\per\hour} enters the turbine at \SI{55}{\bar} and \SI{660}{\degreeCelsius}.

	\begin{figure}[htp]%handmade
		\begin{center}
		\includegraphics[width=\columnwidth]{turbine_uss_hornet}
		\end{center}
		\supercaption{Inspection window of one of the low-pressure turbines (power approximately \SI{25}{\mega\watt}) of the aircraft carrier \textit{USS Hornet} launched in 1943.}{\flickrfile{maralinga/15759249479}{Photo} \ccbysa by \flickrname{maralinga}{Tony Kent}}
		\label{fig_turbine_uss_hornet}
	\end{figure}

	In the turbine, steam expands following an approximately reversible adiabatic evolution. When the pressure reaches \SI{1}{\bar}, steam is extracted at a low flow rate (\SI{1}{\kilogram\per\second}) to heat another part of the power plant. The remaining steam in the turbine is expanded to a pressure of \SI{0.18}{\bar}.

	What is the mechanical power developed by the turbine?
	\exerciseend

\subsubsection{Direction of Transformations (1)}
\label{exo_sens_transfos_un}

	A mass of air undergoes an evolution without any heat transfer. There are two states:
		\begin{itemize}
			\item State X: at \SI{1}{\bar} and \SI{300}{\degreeCelsius};
			\item State Y: at \SI{5}{\bar} and \SI{500}{\degreeCelsius}.
		\end{itemize}

	In which direction ($\fromxtoy$ or $\fromytox$) can the evolution take place?

	Represent the process on a pressure-volume diagram and on a temperature-entropy diagram, qualitatively.
\exerciseend

\subsubsection{Direction of Transformations (2)}
\label{exo_sens_transfos_deux}

	Water undergoes a process during which \SI{2}{\mega\joule\per\kilogram} of heat is extracted from it, while its temperature is fixed at \SI{250}{\degreeCelsius}. There are two states, one at the beginning and the other at the end:
		\begin{itemize}
			\item State X: in the saturated vapor state at \SI{200}{\degreeCelsius};
			\item State Y: in the saturated liquid state at \SI{240}{\degreeCelsius}.
		\end{itemize}
	Which of the two states must have occurred before the other?
\exerciseend

\subsubsection{Expansion of Compressed Air}
\label{exo_detente_air_irreversible}

	The air in a thermally insulated cylinder is expanded from \SI{6.8}{\bar} and \SI{430}{\degreeCelsius} to \SI{1}{\bar}.

	At the end of the expansion, the temperature is measured at \SI{150}{\degreeCelsius}.

	Is the expansion reversible? Represent the process on a temperature-entropy diagram, qualitatively.
\exerciseend

\subsubsection{Air Pump}
\label{exo_pompe_air}

	Air enters a small centrifugal pump with a flow rate of \SI{4}{\kilogram\per\minute} (\cref{fig_pompe_stockholm}). The pump is not isentropic, but its heat losses can be neglected.

	\begin{figure}[htp]%handmade
		\begin{center}
			\includegraphics[width=0.6\columnwidth]{stockholm_pump}
		\end{center}
		\supercaption{Public air compressor in Stockholm for cyclists. A heat exchanger integrated under the bodywork fortunately ensures the temperatures calculated in this exercise are never attained.}{\wcfile{Cykelpump-Stockholm}{Photo} \ccbysa \wdun{JakobVoss}{Jakob Voß} (cropped)}
		\label{fig_pompe_stockholm}
	\end{figure}

	At the inlet, the air is at \SI{1}{\bar} and \SI{15}{\degreeCelsius}.\\
	At the outlet, the pressure is \SI{2}{\bar} and the temperature is measured at \SI{97}{\degreeCelsius}.

	\begin{enumerate}
		\item What is the power required to operate the compressor?
		\item What would be the power if the compression were isentropic?
		\item What would be the heat and work transfers required to return the air to its initial conditions (minimizing heat transfers)?
	\end{enumerate}
\exerciseend

\subsubsection{Theoretical Power Plant}
\label{exo_cycle_carnot_vapeur}\index{Carnot!cycle de}\index{cycle!de Carnot}

	During the design of an power plant, a group of enthusiastic engineers is studying the possibility of having water follow a Carnot cycle. The heat released by coal combustion is transferred to a steam boiler. The steam is expanded in a turbine, which powers an electric generator.

	\begin{description}
		\item[From A to B] Water is compressed in an isentropic pump.\\
			At A, the liquid-vapor mixture is at a pressure of \SI{0.04}{\bar}.\\
			At B, the water is in the saturated liquid state, at a pressure of \SI{40}{\bar}.
		\item[From B to C] Water is heated at constant pressure (\SI{40}{\bar}) in the boiler.
			At C, the water is in the saturated vapor state.
		\item[From C to D] Water is expanded in an isentropic turbine.
			At D, the water is at the initial pressure, i.e., \SI{0.04}{\bar}.
		\item[From D to A] Water is cooled in a condenser at constant pressure (\SI{0.04} bar).
	\end{description}

	\begin{enumerate}
		\item Sketch the elements of the circuit followed by the steam, and represent the process on a temperature-entropy diagram, qualitatively, also showing the saturation curve.
		\item What is the quality of the water when condensation is interrupted (at A)? What is the specific enthalpy at that point?
		\item What is the quality at the turbine outlet (at D) and the specific enthalpy at this point?
		\item What is the power developed by the turbine?
		\item What is the power of the boiler?
		\item What is the power of the pump?
		\item What is the efficiency of the power plant?
	\end{enumerate}
\exerciseend

\subsubsection{Irreversible Heat Transfers}
\label{exo_transferts_chaleur_irreversibles}\index{température!gradient de}\index{moteur!température minimale d’un}

	A steam engine operates on a Carnot cycle, with a continuous flow rate of \SI{2}{\kilogram\per\second}, between the saturation points of water. The engine is designed to exploit a heat source at a moderate temperature (\SI{300}{\degreeCelsius}), from the combustion of industrial waste, and it rejects heat into a river at a low temperature (\SI{5}{\degreeCelsius}).

The boiler has thick walls to reduce the impact of manufacturing imperfections and to withstand the high pressure of the water. This thickness imposes a significant temperature gradient across the walls (\SI{10}{\degreeCelsius}). The same applies to the condenser (gradient: \SI{10}{\degreeCelsius}).

\begin{enumerate}
	\item By how much does the entropy of the \{heat source + water\} system increase?
	\item By how much does the entropy of the \{heat sink + water\} system increase?
	\item What is the power loss associated with this increase in entropy?
	\item Which physical properties of the boiler wall material are most desirable to minimize this issue?
\end{enumerate}
\exerciseend

\subsubsection{Irreversible Compressions and Expansions}
\label{exo_compressions_detentes_irreversibles}

	The team of engineers in charge of the engine from the previous exercise (Carnot cycle operating between \SI{290}{\degreeCelsius} and \SI{15}{\degreeCelsius}, exercise \ref{exo_transferts_chaleur_irreversibles}) discovers that the compression and expansion phases are not reversible.

The compressor does bring the water to a high temperature, but its energy consumption is \SI{10}{\percent} higher than expected. The turbine does bring the water to a low temperature, but it provides \SI{10}{\percent} less mechanical energy than expected.

\begin{enumerate}
	\item By how much does the entropy of the steam increase in each of these two components?
	\item By how much do the heat rejections increase?
	\item What is the efficiency loss of the system compared to a reversible system?
\end{enumerate}
\exerciseend

\exercisesolutionpage
\titreresultats

\begin{description}[itemsep=2em]
		\item [\ref{exo_questions_cours}]
						\includegraphics[width=\solutiondiagramwidth]{exo_sol_ts_tasse}
						\tab 1) See \S\ref{ch_entropie_definition} page~\pageref{ch_entropie_definition} ;
						\tab 2) Yes, a simple heat extraction is enough: see example~\ref{exemple_delta_entropie_basics} page~\pageref{exemple_delta_entropie_basics} ;
						\tab 3) Specific heat capacity: heat per unit mass $\tdiffi q$ needed to generate a temperature change $\tdiff T$ (equation~\ref{eq_def_capacite_calorifique_massique} page~\pageref{eq_def_capacite_calorifique_massique}: $c\ \equiv \ \frac{\diffi q}{\diff T}$). Specific entropy: specific heat divided by the temperature at which it is supplied during a reversible process (equation~\ref{eq_variation_franche_entropie} page~\pageref{eq_variation_franche_entropie}) ;
						\tab 4) Both temperatures evolve until they equalize; $\Delta s_\A + \Delta s_\B > 0$.
		\item [\ref{exo_ts_variations_elementaires}]
						\includegraphics[width=\solutiondiagramwidth]{exo_sol_ts_detente_eau}
						\tab 1) $u_1 = \SI{3276.4}{\kilo\joule\per\kilogram} $ and $u_2 = \SI{2703.3}{\kilo\joule\per\kilogram}$: $W_\text{max.} = \SI{-5.731}{\mega\joule}$.
						\tab 2) $T_3 = \SI{103.51}{\degreeCelsius}$
		\item [\ref{exo_chauffage_isotherme_eau}]
						\includegraphics[width=\solutiondiagramwidth]{exo_sol_ts_chauffage_isotherme_eau}
						\tab $s_2 = \SI{8.671}{\kilo\joule\per\kelvin\per\kilogram}$ ; thus $u_2 = \SI{2660.89}{\kilo\joule\per\kilogram}$ ; finally $w_{1\to 2} = \SI{-1.19}{\mega\joule\per\kilogram}$.
		\item [\ref{exo_diagrammes_ts}]
						\includegraphics[width=\solutiondiagramwidth]{exo_sol_ts_multiples1}
						\includegraphics[width=\solutiondiagramwidth]{exo_sol_ts_multiples2}
						\includegraphics[width=\solutiondiagramwidth]{exo_sol_ts_multiples3}
						\includegraphics[width=\solutiondiagramwidth]{exo_sol_ts_multiples4}
						\includegraphics[width=\solutiondiagramwidth]{exo_sol_ts_multiples5}
						\includegraphics[width=\solutiondiagramwidth]{exo_sol_ts_multiples6}
		\item [\ref{exo_ts_carnot}]
						\includegraphics[width=\solutiondiagramwidth]{exo_sol_ts_carnot_thermopompe}
						\tab 2) In this case $W_{\B\to\C_\text{irr.}} > W_{\frombtoc’}$ and, as negative values, $W_{\D\to\A_\text{irr.}} > W_{\fromdtoa’}$. Thus the rejected heat $Q_{\fromctod}$ increases (which may initially seem like an interesting result) and the heat intake $Q_{\fromatob}$ decreases (and we see that the increase in $Q_{\fromctod}$ is actually only due to the inefficiencies of the compressor and the turbine and only serves to reduce the efficiency).
		\item [\ref{exo_turbine_vapeur_isentropique}]
						\tab The process is as described in example~\ref{exemple_turbine_vapeur_isentropique} page~\pageref{exemple_turbine_vapeur_isentropique}: $h_1 = \SI{3803.5}{\kilo\joule\per\kilogram}$ (dry steam); $h_2 = \SI{2677.7}{\kilo\joule\per\kilogram}$ (dry steam); $h_3 = \SI{2413.6}{\kilo\joule\per\kilogram}$ (mixture with 91.9\% quality); thus $\dot{W}_\text{turbine} = \SI{-96.26}{\mega\watt}$.
		\item [\ref{exo_sens_transfos_un}]
						\includegraphics[width=\solutiondiagramwidth]{exo_sol_ts_bonsens1}
						\tab With equation~\ref{eq_delta_s_gp_p} we find $s_\Y - s_\X = \SI{-161,08}{\joule\per\kelvin\per\kilogram} < \int_\X^\Y \left(\frac{\tdiffi q}{T}\right)_\text{real path} = \SI{0}{\kilo\joule\per\kelvin\per\kilogram}$ (since the process is adiabatic). Therefore, the direction is $\fromytox$.
		\item [\ref{exo_sens_transfos_deux}]
						\includegraphics[width=\solutiondiagramwidth]{exo_sol_ts_bonsens2}
						\tab Assuming $\fromxtoy$, then $\Delta s = \SI{-3,728}{\kilo\joule\per\kelvin\per\kilogram}$ but $\int_\X^\Y \left(\frac{\tdiffi q}{T}\right)_\text{real path} = \SI{-3,823}{\kilo\joule\per\kelvin\per\kilogram}$, so we are reassured: the direction is indeed $\fromxtoy$.
		\item [\ref{exo_detente_air_irreversible}]
						\tab With equation~\ref{eq_delta_s_gp_p} we get $\Delta s = \SI{+39,77}{\joule\per\kelvin\per\kilogram}$ but --\textit{aha}!-- $\int_1^2 \left(\frac{\tdiffi q}{T}\right)_\text{real path} = \SI{0}{\kilo\joule\per\kelvin\per\kilogram}$, thus the transformation is irreversible. We could also have used the very classic equation~\ref{eq_isentropique_horrible2} \pagerefp{eq_isentropique_horrible2} to find that $T_{2 \text{isentropic}} < \SI{150}{\degreeCelsius}$.
		\item [\ref{exo_pompe_air}] 
						\tab 1) $\dot{W}_\text{pompe} = \dot{m} c_p \Delta T = \SI{+5.493}{\kilo\watt}$ (equations~\ref{eq_petite_sfee_deltas_h} \&~\ref{eq_h_fonction_de_T});
						\tab 2) Using equation~\ref{eq_isentropique_horrible2} $T_{2 \text{is.}} = \SI{351.3}{\kelvin}$, i.e., \SI{78.1}{\degreeCelsius}, thus $\dot{W}_\text{ideal} = \SI{+4.231}{\kilo\watt}$ ;
						\tab 3) One possibility: isentropic expansion to obtain $\dot{W}_{2\to 1} = \SI{-4.231}{\kilo\watt}$, then a necessary cooling without work of $\dot{Q}_{2\to 1} = \SI{-1.262}{\kilo\watt}$. All reversible transformations with a net sum of transfers taking these values (e.g., during a cooled expansion) will allow to return to 1.
		\item [\ref{exo_cycle_carnot_vapeur}]
						\includegraphics[width=\solutiondiagramwidth]{exo_sol_ts_carnot_vapeur1}
						\tab The diagram is presented in~\cref{fig_exo_carnot_circuit_vapeur} \pagerefp{fig_exo_carnot_circuit_vapeur} ;
						\tab 2) $x_\A = \frac{s_\B - s_L}{s_{LV}} = \num{0.2949}$ ; thus $h_\A = h_L + x_\A h_{LV} = \SI{838.7}{\kilo\joule\per\kilogram}$ ;
						\tab 3) Same process: $x_\D = \num{0.7014}$ thus $h_\D = \SI{1827.5}{\kilo\joule\per\kilogram}$ ;
						\tab 4) $w_{\text{turbine}} = h_\D - h_\C = \SI{-973.3}{\kilo\joule\per\kilogram}$ ;
						\tab 5) $q_{\text{boiler}} = h_\C - h_\B = \SI{+1713}{\kilo\joule\per\kilogram}$ ;
						\tab 6) $w_{\text{pump}} = h_\B - h_\A =  = \SI{+248.8}{\kilo\joule\per\kilogram}$ ;
						\tab 7) $\eta_{\text{plant}} = \left|\frac{w_{\text{net}}}{q_{\text{inn}}}\right| = \frac{-w_{\text{turbine}} - w_{\text{pump}}}{q_{\text{boiler}}} = \SI{42.29}{\percent}$. Since all phases are reversible and heat transfers are isothermal, we have $\eta_{\text{plant}} = \eta_{\text{Carnot engine}} = 1 - \frac{T_{\text{consenser water}}}{T_{\text{boiler water}}}$ (\ref{eq_efficacite_moteur_carnot_temperature}).
		\item [\ref{exo_transferts_chaleur_irreversibles}]
						\tab 1) $\dot{S}_{\text{high temp. wall}} = \dot{m} \ (\Delta s_{\text{combustion}} + \Delta s_{\text{water}}) = \SI[per-mode = symbol]{+91.77}{\joule\per\kelvin\per\second} = \SI{+91.77}{\watt\per\kelvin}$ ;
						\tab 2) $\dot{S}_{\text{low temp. wall}} = \SI{+188.3}{\watt\per\kelvin}$, and we see that a gradient of~\SI{10}{\degreeCelsius} is more penalizing at low temperature than at high temperature ;
						\tab 3) $\dot{W}_{\text{lost}} = \dot Q_{\text{inn}} \left(\eta_{\text{high temp.}} - \eta_{\text{low temp.}}\right) = \SI{77.9}{\kilo\watt}$
						\tab 4) To reduce temperature gradients, materials with very high thermal conductivity are needed (this is of course not the only quality required of them…).
		\item [\ref{exo_compressions_detentes_irreversibles}]
						\tab 1) $\Delta s_\text{compressor} = \SI{+67}{\joule\per\kelvin\per\kilogram}$ ; $\Delta s_\text{turbine} = \SI{+382}{\joule\per\kelvin\per\kilogram}$ ;
						\tab 2) $\Delta q_\out = \SI{-110,2}{\kilo\joule\per\kilogram}$, or \SI{+14,6}{\percent} ;
						\tab 3) $\eta_\text{real plant} =\SI{39,82}{\percent}$, or \SI{-9}{pt}.
	\end{description}

