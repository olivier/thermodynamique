\atstartofhistorysection
\section[A Bit of History: From the Steam Turbine to the Has Turbine]{A Bit of History:\\ from the Steam Turbine\\ to the Gas Turbine}
\label{ch_histoire_turbine}\index{turbine!history}\index{turbomachine (turbine engine)!history}

\index{Turbinia (ship)}\index{Parsons, Charles Algernon}
At the beginning of the \textsc{xx}\textsuperscript{th} century, the \vocab[turbomachine (turbine engine)!history]{turbine} replaced pistons and cylinders in all steam engines. 
A turbine has a complex geometry, sensitive to manufacturing imperfections, making its construction more delicate than that of cylindrical pistons. In return, it makes for an engine with simple arrangement, little vibration, and with easier assembly, maintenance, and lubrication, allowing for increased power or reduced volume. Anglo-Irish engineer \wfd{Charles Algernon Parsons}{Charles Parsons} dramatically demonstrated this in 1897 with the \textit{Turbinia} (\cref{fig_turbinia}), the first ship of its kind, which was so fast that no military vessel could catch up with it. Ten years later, the entire Royal Navy had switched to turbines for ship propulsion.

	\begin{figure}[ht]%handmade
		\begin{center}
			\includegraphics[width=8cm]{turbinia}
		\end{center}
		\supercaption{The \textit{Turbinia}, Charles Parsons' yacht used as a demonstrator for his research in maritime propulsion. With its three steam turbines and nine propellers, it reached \SI[per-mode=symbol]{60}{\kilo\metre\per\hour} and allowed its owner to ridicule the Royal Navy during Queen Victoria's jubilee parade in 1897.}%
			{\wcfile{Turbinia At Speed}{Photo} by Alfred John West, 1897 (\pd)}
		\label{fig_turbinia}
	\end{figure}

	\index{von Ohain, Hans}\index{Ohain, Hans von}\index{Whittle, Frank}
	In the realm of gas engines, the situation was quite different: until the late 1930s, all engines were piston-cylinder based. Piston technology peaked in the aeronautical sector, where cylinders were arranged in a star pattern behind the propellers to reduce bulkiness and vibrations. In these machines, such as the \textit{Twin Wasp} by \textit{Pratt \& Whitney}, the mechanical arrangement of cylinders, connecting rods, and crankshafts was absolutely phenomenal (\cref{fig_twin_wasp}), and the feeding and exhaust systems going to and back from dozens of combustion chambers were  labyrinthine. Two engineers, the German \wf{Hans von Ohain} and the English \wf{Frank Whittle}, independently dedicated themselves to the design of a gas turbine engine in order to break free from this complexity.

\begin{figure}[ht]%handmade
	\begin{center}
		\includegraphics[width=8cm]{twin_wasp}
	\end{center}
	\supercaption{Cross-section of a \textit{Pratt \& Whitney Twin Wasp} engine (1932), showing the internal arrangement with connecting rods and crankshafts connecting the two rows of seven pistons arranged in a star pattern. The engine, with a displacement of \SI{30}{\liter}, produced over \SI{1000}{hp} and was produced in over \num{170 000} units.}%
		{\wcfile{Cutaway of Pratt \& Whitney Twin Wasp (1932)}{Photo} \ccbysa \olivier}
	\label{fig_twin_wasp}
\end{figure}

	Developing a gas turbine engine is much more challenging than for steam. Certainly, air (or burnt gases) and dry steam have very similar properties: thus a steam turbine works very well with compressed air. The difficulty lies at the other end of the engine. In steam engines, compressing water is done in the liquid state, which is very efficient. Compressing water at \SI{10}{\degreeCelsius} from \num{1} to \SI{10}{\bar}, for example, only requires $w_\fromatob \approx v_L (p_\B - p_\A ) = \num{0.001} (10 - 1) \times \num{e5} = \SI{900}{\joule\per\kilogram}$ (\ref{eq_pompe_liquide}). In contrast, doing the same with air requires a minimum specific power of $w_\fromatob = c_p \Delta T = c_p \left( T_\A \left(p_\B/p_\A\right)^\frac{\gamma - 1}{\gamma} - T_\A \right) = \num{1005} \left(\num{283,15} \left(10\right)^\frac{\num{0,4}}{\num{1,4}} - \num{283,15} \right) = \SI{265}{\kilo\joule\per\kilogram}$ (\ref{eq_gp_travail_isentropique_so} \& \ref{eq_isentropique_horrible2}), almost three hundred times more!

	\index{efficiency!isentropic}\index{turbine!isentropic efficiency}\index{compressor!isentropic efficiency}\index{efficiency!of a compressor}\index{efficiency!of a turbine}
	We saw in \S\ref{ch_cycle_de_rankine} that using liquid compression is not without consequences -- it must be compensated by greater power at the boiler and reduces the thermodynamic efficiency -- but it greatly facilitates the development of the engine. Since almost all of the net power of the engine comes from the turbine, a highly irreversible or incomplete expansion only affects the power and efficiency of the engine. In a gas turbomachine, on the other hand, the turbine also powers the compressor: it plays a dual role. As long as it does not produce enough power to match that of the compressor, the engine will not run at all. The isentropic efficiency of the turbine and compressor thus become paramount parameters (we will revisit this in \S\ref{ch_rapport_des_puissances} with the concept of \vocab{work margin}\index{work!margin of}) and it follows that the development of a gas turbomachine is an ambitious undertaking.

	\index{von Ohain, Hans}\index{Ohain, Hans von}\index{Whittle, Frank}\index{reliability}\index{engine!reliability of a}
	Both Whittle and von Ohain focused their efforts on an ingenious aeronautical engine called \vocab[turbojet]{\mbox{turbojet}}\index{turbojet!single flow}: it is the exhaust gases, in large quantities and with high residual pressure, that will provide the engine's thrust (\S\ref{ch_turboreacteur}). The operating principle is very simple (the air flow is continuous and there is only one moving part) but the challenges are numerous. Like an aircraft wing, the compressor blades tend to stall at low power and during transient phases, causing abrupt and destructive flow variations. In the combustion chambers, it is necessary to prevent the flame from impinging on the walls (which would cause them to melt) or from extending, especially during ignition or reignition, into the turbine. Weight constraints require the use of lightweight materials which complicate manufacturing. The two engineers carried out their work in the heart of the Second World War, each funded by military budgets, and the first jet aircraft flew in 1940. The subsequent production aircraft were delicate to operate, unresponsive, and their service life barely reached \SI{20}{\hour}. They arrived too late and in insufficient numbers to affect the course of the conflict.

\begin{figure}
	\begin{center}
		\includegraphics[width=8cm]{he_s1}
	\end{center}
	\supercaption{Cross-sectional diagram of the \textit{Heinkel He S-1}, the first prototype tested by Hans von Ohain in 1937. The compressor consists of an axial stage and a centrifugal stage; the turbine is centripetal. There is only one moving part and its speed is uniform.\index{compressor!centrifuge}}%
		{\wcfile{Ohain USAF He S-1 page58}{Diagram} USAF (\pd)}
	\label{fig_heinkel}
\end{figure}

	At the end of the war, there was a surge of enthusiasm: aviation embraced the engine it had been waiting for over three decades. To understand why the \mbox{turbojet} engine became the Holy Grail of 20\textsuperscript{th} century aeronautics, a bit of flight mechanics is needed. In subsonic flight, a well-designed aircraft has a \vocab{drag coefficient}\index{drag coefficient} $C_D \equiv F_D \div \left(\frac{1}{2} A_\text{ref.} \rho \ C_\text{flight}^2\right)$ that is almost constant. Thus, when reducing the reference area $A_\text{ref.}$ of the wing surface and the ambient density $\rho$ (by gaining altitude), the flight speed $C_\text{flight}$ can be increased \emph{while keeping the drag $F_D$ constant}. The energy cost of moving the aircraft remains constant -- however, the required power $\dot W_\text{engine} = F_x C_\text{flight}$ increases proportionally to the speed. These characteristics make aircraft relatively energy-efficient machines, but very power-hungry, since they need to maintain the same thrust at very high speeds.

	\index{specific thrust}\index{specific!thrust}\index{specific power (of an engine)}\index{specific!power}
	The turbojet engine had two advantages to address this issue. Firstly, it was compact, lightweight, and vibration-free, which is highly desirable for an application where drag (and thus the thrust to be provided) increases proportionally with the weight of the aircraft. Secondly, the propeller, which is very efficient at low speeds but with whose tips reach supersonic speeds early, thus limiting the speed of aircraft, was completely eliminated. Because of these qualities, the low efficiencies due to irreversible compressions and expansions, low pressure ratios, and excessively high gas speeds in the nozzles were acceptable.

	Thus, the graceful \textit{Lockheed Constellation}, the culmination of the era of propeller aviation, was instantly rendered obsolete by the arrival of the much faster \textit{De Havilland Comet} in 1949, a remarkable quad-jet of the same size (\cref{fig_trois_avions}). Even though it was initially unable to cover the same distance and it featured higher fuel consumption per kilometer, the \textit{Comet} left no chance for its competitors. Its speed was an obvious quality for passengers, but also for the airlines, significantly increasing their productivity.

\begin{figure}[ht]
	\begin{center}
		\includegraphics[width=9cm]{lockheed_constellation}
		\includegraphics[width=9cm]{dhc_comet}
		\includegraphics[width=9cm]{boeing_707}
	\end{center}
	\supercaption{From top to bottom:\\%
		The \textit{Lockheed Constellation} (1943), the culmination of the propeller aircraft era: four \textit{Wright Duplex-Cyclone} supercharged 18-cylinder engines, capable of covering \SI{3700}{\kilo\meter} at \SI[per-mode=symbol]{500}{\kilo\meter\per\hour}.\\%
		The \textit{De Havilland Comet} (1949), the first jet airliner: four \textit{Halford Ghost} single-flow turbojet engines, capable of covering \SI{2400}{\kilo\meter} at \SI[per-mode=symbol]{740}{\kilo\meter\per\hour}.\\%
		The \textit{Boeing 707} (1957), with a configuration and performances anticipating those of all its successors: four \textit{Pratt \& Whitney JT3C} single-flow turbojet engines, capable of covering \SI{4300}{\kilo\meter} at \SI[per-mode=symbol]{900}{\kilo\meter\per\hour}.}%
		{\wcfile{Lockheed1049twa (4423687925)}{Constellation Photo} \ccbysa by Bill Larkins\\%
		 \wcfile{BEA Airtours Comet G-ARCP 1N}{Comet Photo} and \wcfile{Monarch B-720 G-AZFB 4N}{707} (edited) \ccbysa by Piergiuliano Chesi}
	\label{fig_trois_avions}
\end{figure}

	The \textit{Comet}, after a serious design flaw was corrected, was itself surpassed by the \textit{Boeing 707} in 1957. Capable of flying further while carrying more payload, and even faster (at \SI[per-mode=symbol]{900}{\kilo\meter\per\hour}, the speed that all airliners have adopted since, the air on the wing's upper surface barely reaches the speed of sound), the \textit{707} marked the entry into the \textit{jet age}, where airliners were no longer built by dozens but by thousands. Thus, in just twenty-five years, the gas turbine engine doubled the speed of aircraft and divided the price of tickets by four.

\begin{historyquote}
\begin{footnotesize}
"Ready?" "Takeoff, top!" The mechanic pushes the throttle levers with me.\\
NNggnniiiaavvrrooooooaaaaaaarrrrooouuummmmm...\\
"N1 green." It’s pushing hard, but accelerates ever so gently, given the weight of the behemoth.\\
"80 knots" "Thrust available"\\
I have the tips of my feet on the rudder pedals, a precision similar to a kickboxing move. I'm enjoying every bit of it.\\
120 knots. I'm in control, guys. 432 passengers and 15 crew members are strapped in the back, ears and senses alert.\\
140 knots. Two bursts of light beacons pass by on the sides. The rudder, precise.\\
"V1." Another 20 knots to reach before the machine can fly. I can see end of the runway coming up, over there ahead.\\
"Rotate." At 170 knots, I pull gently, then more firmly.\\
Five degrees of pitch. Ten degrees. It’s no longer rolling, the needle is at 185 knots.\\
Twelve-degree pitch. Come on, my dear, we must climb.\\
"Positive climb" "Landing gear up." The truth lies tonight between twelve and thirteen degrees of pitch, where the airspeed indicator needle comes to a standstill. We pass the hill, and three hundred feet below, the \textit{747} flying by must feel like an earthquake.
\begin{flushright}\vspace{-0.5em}Jacques Darolles, 1998\\ \textit{The Most Beautiful Office in the World}~\cite{darolles2000}\end{flushright}\index{Darolles, Jacques}
\end{footnotesize}
\end{historyquote}

	\index{reliability}\index{engine!reliability of a}
	Nearly sixty years after the first flight of the \textit{707}, airliners still fly at the same speed, but turbojet technology has continued to advance~\cite{rollsroyce2005}. With their fan blades made of carbon-epoxy or blown titanium composites, turbine stators printed in ceramics, multiple laser-drilled pneumatic turbine cooling circuits, electronic control and diagnostic and remote monitoring systems, they slowly but surely continue to increase in efficiency. Reliability is also remarkable: a modern engine on average only experiences an in-flight failure every \num{200000} flight hours, and is separated from the aircraft for maintenance only every \num{20000} hours or \num{10000} flights. Will a new type of engine ever render the turbojet obsolete and propel aviation forward into a new era?

\atendofhistorysection

