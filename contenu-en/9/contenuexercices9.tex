\begin{boiboiboite}
	\propwater
	\propair
\end{boiboiboite}

\subsubsection{Superheated Rankine cycle}
\label{exo_porcheville_rankine}
\index{Rankine!superheated cycle}\index{cycle!superheated Rankine}\index{superheating (process)}

\begin{comment}
	Some data for this power plant found on the web:
		1,800 tons/hour
		Oil 10 times more expensive than coal (1,000 euros/ton vs 100 euros)
		Each boiler, 545°C 180 bar, 130t oil/hour
		Inlet pressure for turbines 150 bars
		Temperature of the water towards the Seine: 3°C
		Coal about 35 MJ/kg
\end{comment}

The \textit{Électricité de France} power plant in Porcheville (\cref{fig_porcheville}) receives heat from the combustion of oil, and uses a steam cycle to power an electric generator.

\begin{figure}
	\begin{center}
		\includegraphics[width=8cm]{porcheville}
		\vspace{-0.5cm}%handmade
	\end{center}
	\supercaption{The Porcheville power plant, fueled by coal until 1987, then on oil until 2017, when in closed down. It mainly served peak demands.}{\wcfile{Porcheville Power Plant}{Photo} \cczero \oc}
	\label{fig_porcheville}
\end{figure}

In the power plant, water cycles between the pressures of \SI{0.1}{\bar} and \SI{140}{\bar}. The steam reaches \SI{545}{\degreeCelsius}, and the turbines have an isentropic efficiency of \SI{80}{\percent}.

For the purposes of the exercise, we consider that the cycle is based on a superheated Rankine cycle.

\begin{enumerate}
	\item Sketch the physical water circuit in the power plant; plot the cycle followed on a temperature-entropy diagram, qualitatively (without representing numerical values) and also show the saturation curve.
	\item What is the enthalpy of the water at the turbines’ outlet?
	\item What is the enthalpy of the water at the pumps’ outlet?
	\item What is the thermal efficiency of the installation?
	\item What is the specific steam consumption of the installation, i.e., the mass of steam that must enter the turbine for the installation to provide \SI{1}{\kWh} of mechanical energy?
	\item What hourly steam flow rate is required in the circuit in order to achieve a mechanical power of \SI{60}{\mega\watt}?
\end{enumerate}
\exerciseend

\subsubsection{Implementation of a reheating process}
\label{exo_porcheville_resurchauffe}
\index{Rankine!reheated cycle}\index{cycle!reheated Rankine}\index{reheating (process)}

The Porcheville installation described in Exercise~\ref{exo_porcheville_rankine} is modified to accommodate a series of reheating tubes. The water expansion is stopped at \SI{18}{\bar} in the turbine, and the steam is brought back to the maximum temperature of the cycle (i.e., \SI{545}{\degreeCelsius}).

The power plant is fueled by heavy fuel oil known as "ULSD", with a density of \SI{1050}{\kilogram\per\meter\cubed} and a heat of combustion of \SI{40.2}{\mega\joule\per\kilogram}.

The air used for combustion enters the boiler at a temperature of \SI{15}{\degreeCelsius} and a pressure of \SI{1}{\bar}. It is heated to a temperature of \SI{820}{\degreeCelsius} at constant pressure through combustion, before passing around the water pipes. When it leaves the boiler, its temperature is \SI{180}{\degreeCelsius}.

\begin{enumerate}
	\item What is the new thermal efficiency of the power plant?
	\item What is its new specific steam consumption?
	\item What air flow rate must be admitted to the boiler to maintain a net mechanical power of \SI{60}{\mega\watt}?
	\item What is the boiler efficiency?
	\item What is the hourly volumetric flow rate of fuel?
	\item An engineer proposes to pass the intake air duct through the exhaust gases (without mixing them) to increase its temperature before combustion. Do you think this is a good idea?
\end{enumerate}
\exerciseend

\subsubsection{Cycle with regeneration}
\label{exo_brise_glace}\index{regeneration@regeneration (process)}\index{superheating (process)}\index{Rankine!superheated cycle}\index{cycle!superheated Rankine}

In a polar icebreaker ship (\cref{fig_50letpodeby}), a steam system powers the propellers from a nuclear reactor.

\begin{figure}
	\begin{center}
		\includegraphics[width=11cm]{50letpodeby}
	\end{center}
	\supercaption{The \textit{50 Let Podeby}, a 25,000-ton nuclear-turbo-electric powered icebreaker (two reactors of $\SI{171}{\mega\watt}_{\text{heat}}$, three engines of $\SI{17.6}{\mega\watt}_{\text{mech.}}$). Its construction started in 1989 but it only entered service in 2007.}{\wcfile{50letPob_pole.JPG}{Photo} \ccbysa by \wcu{Kiselev d}}
	\label{fig_50letpodeby}
\end{figure}

The cycle is based on a superheated Rankine cycle between pressures of \num{30} and \SI{0.5}{\bar}. Through contact with the pipes of pressurized water that passes through the reactor, the steam reaches \SI{310}{\degreeCelsius}.

For the sake of not overloading this exercise, we consider that the turbine is isentropic and perfectly insulated.

\begin{enumerate}
	\item What is the thermal efficiency of the installation?
	\item We define the specific steam consumption as the inverse of the net power of the installation: it is the mass of steam that has passed through the turbine when the installation has generated \SI{1}{\kWh} of mechanical energy.\\
	What is the specific consumption of the installation?
\end{enumerate}

An engineer proposes to modify the cycle and make it regenerative by extracting steam from the turbine to insert it into the compression circuit.

S/he suggests separating the compression into two stages, one from \SI{0.5}{\bar} to \SI{6}{\bar}, and the second from \SI{6}{\bar} to \SI{30}{\bar}; and then inserting the extracted steam into the feedwater between the two pumps. The steam extraction rate is such that the water at the exit of the mixer is exactly at the saturation point.

\begin{enumerate}
	\item Sketch the proposed installation (i.e., the physical circuit followed by the steam).
	\item Represent the thermodynamic cycle on a temperature-entropy diagram qualitatively, also showing the water saturation curve.
	\item What proportion of the steam flow rate would need to be extracted at \SI{6}{\bar} in the turbine to heat the water to saturation between the two pumps?
	\item Does the shaft power increase or decrease, and by how much?
	\item Does the efficiency of the installation increase or decrease, and by how much?
\end{enumerate}
\exerciseend

\exercisesolutionpage
\titreresultats

\begin{description}[itemsep=2em]
	\item [\ref{exo_porcheville_rankine}]
					\tab 1) See figures~\ref{fig_cycle_rankine_surchauffe} and~\ref{fig_ts_lv_rankine_surchauffe} \pagerefp{fig_ts_lv_rankine_surchauffe};
					\tab 2) With $s_\E = s_\D = \SI{6,5399}{\kilo\joule\per\kilogram}$ and $\eta_\text{T} = \SI{80}{\percent}$, we obtain $h_\E = \SI{2287,7}{\kilo\joule\per\kilogram} $ as in example~\ref{exemple_turbine_centrale} \pagerefp{exemple_turbine_centrale};
					\tab 3) Using equation~\ref{eq_pompe_liquide} we obtain $h_\B = \SI{205,9}{\kilo\joule\per\kilogram}$ as in example~\ref{exemple_pompe_centrale};
					\tab 4) $\eta_\text{thermique} = \left|\frac{(h_\E - h_\D) + (h_\B - h_\A)}{(h_\D - h_\B)}\right| = \SI{35,29}{\percent}$ (\ref{def_rendement_moteur});
					\tab 5) \textsc{ssc} = \SI[per-mode=symbol]{3.15}{\kilogram\per\kilo\watt\per\hour};
					\tab 6) $\dot{m}_\text{water} = \SI{52.5}{\kilogram\per\second}$.
					
	\item [\ref{exo_porcheville_resurchauffe}]
					\tab 1) $h_{\text{D2}} = \SI{2960.8}{\kilo\joule\per\kilogram}$, $h_{\text{E2}} = \SI{3570.3}{\kilo\joule\per\kilogram}$, $h_\text{F} = \SI{2642.7}{\kilo\joule\per\kilogram}$, thus the efficiency reaches $\eta_\text{thermal 2} = \SI{36.31}{\percent}$ (+1 point, already a significant improvement);
					\tab 2) \textsc{ssc} = \SI[per-mode=symbol]{2.576}{\kilogram\per\kilo\watt\per\hour} (\SI{-18}{\percent}, good work);
					\tab 3) In the boiler, the heat lost by the air is gained by the water: $\dot m_\text{air} = \frac{-\dot Q_\text{water}}{c_p \ \Delta T} = \frac{\dot W_\net}{\eta_\text{thermal}} \frac{1}{c_p \ (T_\text{air 3} - T_\text{air 2})} = \SI{256,9}{\kilogram\per\second}$.
					\tab 4) $\eta_\text{boiler} = \frac{\dot Q_\text{water}}{\dot Q_\text{received by the air}} = \SI{79.5}{\percent}$;
					\tab 5) $\dot{V}_\text{fuel} = \frac{\dot Q_\text{received by the air}}{\rho_\text{fuel} c_\text{fuel} \eta_\text{boiler}} = \SI{17.7}{\cubic\meter\per\hour}$;
					\tab 6) It is an excellent idea. In this way, we reduce the heat carried away by the exhaust gases at the boiler exit, immediately increasing $\eta_\text{boiler}$.
					
	\item [\ref{exo_brise_glace}]
					The \textit{50 Let Podeby} actually operates between \SI{29} and \SI{0.75}{\bar}, but these values that are not tabulated in the Steam Tables for this book.
					\tab 1) Using the diagram from figures~\ref{fig_cycle_rankine_surchauffe} and~\ref{fig_ts_lv_rankine_surchauffe} on page~\pageref{fig_ts_lv_rankine_surchauffe}, $h_\A = \SI{340,5}{\kilo\joule\per\kilogram}$, $h_\B = \SI{343,54}{\kilo\joule\per\kilogram}$, $h_\D = \SI{3017,4}{\kilo\joule\per\kilogram}$, $h_\E = \SI{2284,5}{\kilo\joule\per\kilogram}$, thus $\eta_\text{thermal} = \SI{27,294}{\percent}$ ;
					\tab 2) $\textsc{ssc} = \SI[per-mode = symbol]{4,93}{\kilogram\per\kilo\watt\per\hour}$ ;
					\tab 3) See \cref{fig_cycle_prelevement_vapeur} \pagerefp{fig_cycle_prelevement_vapeur};
					\tab 4) See \cref{fig_ts_lv_prelevement_vapeur} \pagerefp{fig_ts_lv_prelevement_vapeur};
					\tab 5) $h_\text{bleed} = \SI{2673,9}{\kilo\joule\per\kilogram}$, $h_\text{pre-mix} = \SI{341,1}{\kilo\joule\per\kilogram}$, $h_\text{post-mix} = \SI{670,4}{\kilo\joule\per\kilogram}$: thus the proportion needed to saturate the water after mixing is $z = \SI{14,1}{\percent}$ ;
					\tab 6) $w_\text{net~2} = \SI{-674,87}{\kilo\joule\per\kilogram}$ (\SI{-9,2}{\percent} : a tragedy !);
					\tab 7) $q_\text{boiler} = \SI{2344,4}{\kilo\joule\per\kilogram}$, ainsi $\eta_\text{thermal~2} = \SI{28,786}{\percent}$ (\SI{+1,49}{pt}: is it really desirable in this application?).
\end{description}

