%[grandeur!etat@d’état/de chemin]
%[fonction d’état ou de chemin]
\index{etat@état, grandeur et fonction d’}%
\index{chemin, grandeur et fonction de}%
\index{transfert, grandeur et fonction de}%
\index{parcours, grandeur et fonction de}%

\textbf{Définition}

On peut classer les grandeurs utilisées en thermodynamique selon les catégories suivantes :

\begin{itemize}
	\item Une grandeur est dite \vocab[grandeur!etat@d’état/de chemin]{grandeur d’état} si sa valeur ne dépend que de l’état actuel du système. La température $T$ est un exemple de grandeur d’état.
	\item Une grandeur est dite \vocab[grandeur!etat@d’état/de chemin]{grandeur de chemin} si sa valeur dépend du chemin parcouru. Le transfert de chaleur $Q$ et le travail $W$ sont les deux seules grandeurs de chemin utilisées dans ce livre.
\end{itemize}

En reprenant l’exemple proposé par Rogers \&~Mayhew~\cite{rogersetal1992}, on peut illustrer la distinction ainsi : un/e cycliste se déplace de A à B. Son altitude au départ $z_\A$ et à l’arrivée $z_\B$ sont des grandeurs d’état ($z$ étant alors une \vocab[fonction d’état ou de chemin]{fonction d’état}), et on peut quantifier $\Delta z \equiv z_\B - z_\A$ sans rien savoir du trajet. En revanche, le travail $W_\fromatob$ dépensé pour aller de A à B, lui, dépend du processus : il sera par exemple plus grand si la route est plus longue ou s’il y a du vent. La quantification de $W$ (une \vocab[fonction d’état ou de chemin]{fonction de chemin}) nécessite de connaître tous les états intermédiaires entre A et~B.

Les grandeurs sont parfois nommées \vocab[grandeur!etat@d’état/de chemin]{variables}\ ; les grandeurs d’état sont parfois nommées \vocab[propriété physique|seealso{grandeur}]{propriétés physiques}. Les grandeurs et fonctions de chemin sont parfois dites \vocab[grandeur!etat@d’état/de chemin]{de transfert} ou \vocab[grandeur!etat@d’état/de chemin]{de parcours}.

\textbf{Notation}
%[différentielle exacte ou inexacte]
\index{exacte, différentielle}%
\index{inexacte, différentielle}%
\index{$\Delta$ (symbole)}%
\index{delta (symboles)}%
\index{$\diffi$ (symbole)}%
\index{d (symbole)}%

Les variations infinitésimales des grandeurs d’état sont notées avec le symbole~$\tdiff$ : ce sont des \vocab[différentielle exacte ou inexacte]{différentielles exactes} et elles peuvent s’intégrer en connaissant seulement leurs valeurs initiale et finale. Par exemple pour la température~$T$ :
\begin{IEEEeqnarray}{rCcCl}
	\int_\A^\B \diff T 		&=& \Delta T  &=&  T_\B - T_\A
\end{IEEEeqnarray}

Les variations infinitésimales des grandeurs de transfert sont notées avec le symbole~$\tdiffi$ : ce sont des \vocab[différentielle exacte ou inexacte]{différentielles inexactes} et leur intégrale ne peut se quantifier qu’avec la connaissance de tous les états rencontrés pendant le parcours. Par exemple pour le travail~$W$, on ne peut pas écrire «~$W_\B - W_\A$~», ni «~$\Delta W$~», mais seulement :
\begin{IEEEeqnarray}{rCl}
	\int_\A^\B \diffi W		&=& W_\fromatob
\end{IEEEeqnarray}

Cette notation peut prêter à confusion, car c’est~$\tdiff$ et non pas~$\tdiffi$ qui devient~$\Delta$ en s’intégrant. Elle peut aussi sembler n’être qu’une façon compliquée de contourner l’utilisation des dérivées partielles. Clifford Truesdell~\cite{truesdell1980} remarque malicieusement qu’à cause de cette écriture, l’équation~\ref{def_entropie} \pagerefp{def_entropie} voudrait nous faire croire que certains types de différentielles sont plus grands que d’autres… Même si cette notation est très largement utilisée dans la littérature française (raison pour laquelle elle est désormais adoptée dans ce livre), il faut reconnaître que l’on peut tout à fait couvrir l’ensemble de la thermodynamique de l’ingénieur~\mbox{\cite{rogersetal1992,eastopetal1993}} ou de la thermodynamique physique et son histoire~\cite{truesdell1980} sans jamais devoir utiliser le symbole~$\tdiffi$ dans le sens ci-dessus.


