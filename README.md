Thermodynamique de l’ingénieur
==============================

Un livre de thermodynamique destiné à des étudiants en école d’ingénieurs.

Téléchargeable sur https://thermodynamique.fr/

Troisième édition, 2021, ISBN 9781794848207 par Olivier Cleynen / Thermodynamique.fr . Framabook (le projet éditorial de l’association Framasoft) a publié les deux permières éditions de ce livre en 2015 et 2018.


## Auteurs

Auteur principal : <a href="https://ariadacapo.net/">Olivier Cleynen</a>. Ses plus vifs remerciements vont aux personnes suivantes pour leurs contributions :

* Philippe Depondt a écrit quatre sections historiques (1.6, 3.6, 4.5 et 8.6) ;
* Nicolas Horny a effectué une relecture critique des aspects techniques et scientifiques de l’ouvrage ;
* Christophe Masutti et Mireille Bernex ont particulièrement contribué à la finalisation de la première édition de l’ouvrage ;
* De nombreuses personnes, en corrigeant des erreurs ou proposant des améliorations, ont réduit l’entropie de ce document, parmi lequelles Antoine L., Hamassala David Dicko, Kévin R., Florianne B., Julien D., Anthony Jouny, Thomas N., Amazigh.L.H, Victor D., Daniel C.-N., Pierrick Degardin, Arthur A., Ulrick M., Solène J., Florian Paupert, Gatien Bovyn, Mehdi Z., Jean-Bernard Marcon, Luc Benoit, Christophe Masutti, Thibault Mattera, Mireille Bernex, Nicolas Horny, Arnaud Gallant, Olivier Kim Hak, Gwen Messé, Laurent Prost, Christophe Valerio, Bruno Turgeon, Saou Ibrahim, Jean Claude Cailton, Philippe Godard, Robert Tomczak, Dimitrios Anagnostou, Sallah Azzdine, Selim Fikri, et Frédéric Novel.
* De nombreuses autres personnes, en publiant leurs photos et figures sous licence libre, ont contribué à illustrer le livre, sans y être toutefois associées.


## Licence

Le manuel est placé sous licence <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr">Creative Commons BY-SA</a> ; les conditions de réutilisation sont détaillées en annexe A8.

Attention, beaucoup de figures publiées dans ce dépôt (photos, schémas) sont placées sous licence compatible mais pas identique. Leur licence et auteur(s) sont indiqués à chaque fois dans le corps du manuel.
